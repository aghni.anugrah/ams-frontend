import React, {Component} from "react";
import "./Filter.css"
import Form from "react-bootstrap/Form";

class FilterRegional extends Component {
    renderYearData() {
        return this.props.listTahun.map((year, index) => {
            return (
                <option key={index} value={year.year}>{year.year}</option>
            );
        })
    }

    render() {
        return (
            <div className="card" className="shadow-lg mb-5 bg-white rounded">
                <div className="card-body">
                    <Form.Group controlId="formGridState">
                        <Form.Label>Tahun</Form.Label>
                        <select className="form-control" defaultValue="" onChange={this.props.handleTahun}>
                            <option value="" disabled>Choose</option>
                            {this.renderYearData()}
                        </select>
                    </Form.Group>
                </div>
                <button onClick={this.props.handleApply} type="submit" className="btn btn-block btn-warning" disabled={this.props.buttonState}>Apply</button>
            </div>
        );
    }
}

export default FilterRegional;