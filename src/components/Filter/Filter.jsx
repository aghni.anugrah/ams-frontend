import React, {Component} from "react";
import "./Filter.css"

class Filter extends Component {
  renderForm = () => {
    return this.props.data.opt.map((sort, index) => {
      return (
        <div key={index} className="form-check">
          <input className="form-check-input" type="radio" name={sort[0]} id={index} value="option1"
                 checked={this.arraysEqual(this.props.data.selectedFilter, sort)} onChange={() => this.props.handleChange(sort)}/>
          <label className="form-check-label" htmlFor={index}>
            {sort[0]}
          </label>
        </div>
      )
    })
  };

  arraysEqual = (a, b) => {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length !== b.length) return false;

    for (let i = 0; i < a.length; ++i) {
      if (a[i] !== b[i]) return false;
    }
    return true;
  };

  render() {
    return (
      <div className="card">
        <div className="card-body">
          <form>
            <div className="form-group">
              <input type="search" className="form-control" placeholder="Search" value={this.props.data.search}
                     onChange={this.props.handleSearch}/>
            </div>
            <fieldset className="form-group">
              <div className="row">
                <legend className="col-form-label col-4 pt-0">Order:</legend>
                <div className="col">
                  <div className="form-check form-check-inline">
                    <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1"
                           checked={!this.props.data.dsc} onChange={() => this.props.handleSort(false)}/>
                    <label className="form-check-label" htmlFor="inlineRadio1">Asc</label>
                  </div>
                  <div className="form-check form-check-inline">
                    <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2"
                           checked={this.props.data.dsc} onChange={() => this.props.handleSort(true)}/>
                    <label className="form-check-label" htmlFor="inlineRadio2">Desc</label>
                  </div>
                </div>
              </div>
            </fieldset>
            <fieldset className="form-group">
              <div className="row">
                <legend className="col-form-label col-4 pt-0">Sort By:</legend>
                <div className="col">
                  {this.renderForm()}
                </div>
              </div>
            </fieldset>
          </form>
        </div>
        <button onClick={this.props.handleApply} type="submit" className="btn btn-warning">Apply</button>
      </div>
    );
  }
}

export default Filter;