import React, {Component} from "react";

class OptionButton extends Component {
  render() {
    return (
      <div className="text-right">
        <button id='button-option' onClick={this.props.handleChange} type="button" className="btn btn-secondary disabled">Options</button>
      </div>
    );
  }
}

export default OptionButton;