import React, { Component } from 'react';
import Modal from 'react-bootstrap/Modal'
import {faQuestionCircle, faTimesCircle, faCircle} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Button} from "react-bootstrap";

class ModalUpdatePenugasan extends Component{

    renderIcon(){
        return this.props.type !== undefined ?
            (this.props.type === "confirm" ?
                    (
                        <FontAwesomeIcon
                            icon={faQuestionCircle}
                            mask={faCircle}
                            size="5x"
                            transform="shrink-3"
                            style={{ color: '#FF9802' }}
                            className="text-center"
                        />
                    )
                    :
                    (
                        <FontAwesomeIcon
                            icon={faTimesCircle}
                            mask={faCircle}
                            size="5x"
                            transform="shrink-3"
                            style={{ color: '#D83615' }}
                            className="text-center"
                        />
                    )
            )
            :
            null
    }

    render() {
        return(
            <React.Fragment>
                <Modal
                    show={this.props.show}
                    onHide={this.props.onHide}
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Body>
                        <h3 className="mb-4"><strong>{this.props.title}</strong></h3>
                        <div className="text-center m-2">
                            {this.renderIcon()}
                        </div>
                        <div className="mx-4 mb-1">
                            {this.props.children}
                        </div>
                        <div className="text-center mb-2">
                            {/*{this.renderSubmitButton()}*/}

                                <Button onClick={this.props.onHide} style={{ backgroundColor: '#FFFFFF', borderColor:'#FECF28', color:'#000000', margin:"10px"}}>BATAL</Button>
                                <Button onClick={this.props.action} style={{ backgroundColor: '#FECF28', borderColor:'#FECF28', color:'#000000'}}>SIMPAN</Button>

                        </div>
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        );
    }
}
export default ModalUpdatePenugasan;
