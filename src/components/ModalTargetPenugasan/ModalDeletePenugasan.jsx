import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAlignCenter, faCircle, faQuestionCircle} from "@fortawesome/free-solid-svg-icons";


class ModalDeletePenugasan extends Component {
    render() {
        return(
            <React.Fragment>
                <Modal
                    {...this.props}
                    size="2x"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    border-radius = "10px"
                >

                    <Modal.Body>
                        <div className="text-center">
                        <FontAwesomeIcon icon={faQuestionCircle}
                                         mask={faCircle}
                                         align={faAlignCenter}
                                         size="5x"
                                         transform="shrink-3"
                                         style={{ color: '#FF9802'}}/>
                        {this.props.children}
                            <Button onClick={this.props.onHide} style={{ backgroundColor: '#FFFFFF', borderColor:'#FF9802', color:'#FF9802', margin:"10px"}}>BATAL</Button>
                            <Button onClick={this.props.action} style={{ backgroundColor: '#FF9802', borderColor:'#FF9802', color:'#FFFFFF'}}>YA, HAPUS</Button>
                        </div>
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        );
    }
}

export default ModalDeletePenugasan;