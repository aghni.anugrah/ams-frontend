import React, { Component } from 'react';

import Modal from 'react-bootstrap/Modal'
import Button from "react-bootstrap/Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAlignCenter, faCircle, faQuestionCircle} from "@fortawesome/free-solid-svg-icons";

class ModalConfirmationSave extends Component{
    renderSecondaryButton(){
        if (this.props.secondaryButton !== undefined){
            return (<button onClick={this.props.secondaryAction}>{this.props.secondaryButton}</button>)
        }
    }

    render() {
        return(
            <React.Fragment>
                <Modal
                    {...this.props}
                    size="md"
                    // aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Body>
                        <div className="text-center">
                            <FontAwesomeIcon icon={faQuestionCircle}
                                             mask={faCircle}
                                             align={faAlignCenter}
                                             size="5x"
                                             transform="shrink-3"
                                             style={{ color: '#FF9802'}}/>
                        {this.props.children}


                        <Button onClick={this.props.secondaryAction} style={{ backgroundColor: '#FFFFFF', borderColor:'#FF9802', color:'#FF9802', margin:"10px"}}>TIDAK,BATAL</Button>
                        <Button onClick={this.props.primaryAction} style={{ backgroundColor: '#FF9802', borderColor:'#FF9802', color:'#FFFFFF'}}>YA,SIMPAN</Button>
                        </div>
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        );
    }
}
export default ModalConfirmationSave;