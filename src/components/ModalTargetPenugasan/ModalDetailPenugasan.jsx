import React, { Component } from 'react';

import Modal from 'react-bootstrap/Modal'
import Button from "react-bootstrap/Button";

class ModalDetailPenugasan extends Component{
    // shouldComponentUpdate(nextProps, nextState){
    //     return(
    //         nextProps.show !== this.props.show ||
    //         nextProps.children !== this.props.children
    //     );
    // }

    render() {
        return(
            <React.Fragment>
                <Modal
                    {...this.props}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">
                            {this.props.title}
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {this.props.children}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="warning" onClick={this.props.onHide}>OK</Button>
                    </Modal.Footer>
                </Modal>
            </React.Fragment>
        );
    }
    //     return(
    //         <React.Fragment>
    //             <Modal
    //                 {...this.props}
    //                 size="lg"
    //                 aria-labelledby="contained-modal-title-vcenter"
    //                 centered
    //             >
    //                 <div className="text-center">
    //                     <Modal.Title id="contained-modal-title--center" >
    //                         {this.props.title}
    //                     </Modal.Title>
    //                 </div>
    //
    //                 <Modal.Body>
    //                     {this.props.children}
    //
    //                 <div className="text-center">
    //                     <Button variant="warning" onClick={this.props.onHide}>OK</Button>
    //             </div>
    //                 </Modal.Body>
    //             </Modal>
    //         </React.Fragment>
    //     );
    // }
}
export default ModalDetailPenugasan;
