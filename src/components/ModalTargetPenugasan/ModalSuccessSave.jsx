import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAlignCenter, faCheckCircle, faCircle} from "@fortawesome/free-solid-svg-icons";


class ModalSuccessSave extends Component {
    render() {
        return(
            <React.Fragment>
                <Modal
                    {...this.props}
                    size="2x"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    border-radius = "10px"
                >

                    <Modal.Body>
                        <div className="text-center">
                            <FontAwesomeIcon icon={faCheckCircle}
                                             mask={faCircle}
                                             align={faAlignCenter}
                                             size="5x"
                                             transform="shrink-3"
                                             // style={{ color: '#FF9802'}}
                                />
                            {this.props.children}
                            <Button onClick={this.props.onHide} style={{ backgroundColor: '#FECF28', borderColor:'#FECF28', color:'#000000'}}>OK</Button>
                        </div>
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        );
    }
}

export default ModalSuccessSave;