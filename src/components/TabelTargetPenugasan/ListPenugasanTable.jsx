import React, {Component} from 'react';
import {faEdit, faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

class ListPenugasanTable extends Component {

renderTableData() {
    if (this.props.data.length === 0) {
        return (
            <tr>
                <td>Tidak ada penugasan untuk ditampilkan</td>
                <td/>
            </tr>
        )
    } else {
        return this.props.data.map((targetpenugasan, index) => {
                const detailTarget = targetpenugasan.target_penugasan
                return (
                    <tr key={index}>
                        <td onClick={() => this.props.detail(targetpenugasan)}>{targetpenugasan.salesperson_name}</td>
                        <td>{detailTarget.kuota_visit}</td>
                        <td id="editButton">
                            <button className="btn" onClick={() => this.props.update(targetpenugasan)}>
                                <FontAwesomeIcon
                                    icon={faEdit}/></button>
                        </td>
                        <td id="deleteButton">
                            <button className="btn" onClick={() => this.props.delete(detailTarget)}>
                                <FontAwesomeIcon icon={faTrash}/></button>
                        </td>
                    </tr>
                )
            }
        )
    }
}

    render() {
        return (
            <React.Fragment>
                <tbody>
                {this.renderTableData()}
                </tbody>
            </React.Fragment>
        )
    }

}
export default ListPenugasanTable