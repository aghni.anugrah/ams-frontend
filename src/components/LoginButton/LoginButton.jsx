import React, {Component} from "react";
import "./LoginButton.css"

class LoginButton extends Component {
  render() {
    return (
      <a id="btn-login" className="btn btn-warning mx-auto btn-block"
         href="https://accounts-staging.warungpintar.co/oauth/login?client_id=5&redirect_uri=http://localhost:3000&response_type=code&state=xyz"
         role="button">Login with SSO</a>
    );
  }
}

export default LoginButton;