import React, { Component, Fragment } from "react";
import Pagination from "react-bootstrap/Pagination";
import classes from './CustomPagination.css'

const LEFT_PAGE = "LEFT";
const RIGHT_PAGE = "RIGHT";

const range = (from, to, step = 1) => {
    let i = from;
    const range = [];

    while (i <= to) {
        range.push(i);
        i += step;
    }

    return range;
};

class CustomPagination extends Component {
    componentDidMount() {
        this.gotoPage(1);
    }

    gotoPage = page => {
        const currentPage = Math.max(0, Math.min(page, Math.ceil(this.props.totalRecords / this.props.pageLimit)));

        const paginationData = {
            currentPage,
            totalPages: Math.ceil(this.props.totalRecords / this.props.pageLimit),
            pageLimit: this.props.pageLimit,
            totalRecords: this.props.totalRecords
        };

        this.props.onPageChanged(paginationData);
    };

    handleClick = (page, evt) => {
        evt.preventDefault();
        this.gotoPage(page);
    };

    handleMoveLeft = evt => {
        evt.preventDefault();
        this.gotoPage(this.props.currentPage - 3);
    };

    handleMoveRight = evt => {
        evt.preventDefault();
        this.gotoPage(this.props.currentPage + 3);
    };

    handleMovePrev = evt => {
        evt.preventDefault();
        this.gotoPage(this.props.currentPage - 1);
    };

    handleMoveNext = evt => {
        evt.preventDefault();
        this.gotoPage(this.props.currentPage + 1);
    };

    fetchPageNumbers = () => {
        const totalPages = Math.ceil(this.props.totalRecords / this.props.pageLimit);
        const currentPage = this.props.currentPage;

        const totalNumbers = 4;
        const totalBlocks = totalNumbers + 2;

        if (totalPages > totalBlocks) {
            const leftBound = currentPage - 1;
            const rightBound = currentPage + 1;
            const beforeLastPage = totalPages - 1;

            const startPage = leftBound > 2 ? leftBound : 2;
            const endPage = rightBound < beforeLastPage ? rightBound : beforeLastPage;

            let pages = range(startPage, endPage);

            const pagesCount = pages.length;
            const singleSpillOffset = totalNumbers - pagesCount - 1;

            const leftSpill = startPage > 2;
            const rightSpill = endPage < beforeLastPage;

            const leftSpillPage = LEFT_PAGE;
            const rightSpillPage = RIGHT_PAGE;

            if (leftSpill && !rightSpill) {
                const extraPages = range(startPage - singleSpillOffset, startPage - 1);
                pages = [leftSpillPage, ...extraPages, ...pages];
            } else if (!leftSpill && rightSpill) {
                const extraPages = range(endPage + 1, endPage + singleSpillOffset);
                pages = [...pages, ...extraPages, rightSpillPage];
            } else if (leftSpill && rightSpill) {
                pages = [leftSpillPage, ...pages, rightSpillPage];
            }

            return [1, ...pages, totalPages];
        }

        return range(1, totalPages);
    };

    render() {
        if (!this.props.totalRecords) return null;

        if (Math.ceil(this.props.totalRecords / this.props.pageLimit) === 1) return null;

        return (
            <Fragment>
                <Pagination size="sm">
                    {this.props.currentPage !== 1 &&
                    <Pagination.Prev
                        onClick={this.handleMovePrev}
                        className={classes.item}
                    />
                    }
                    {this.fetchPageNumbers().map((page, index) => {
                        if (page === LEFT_PAGE) {
                            return (
                                <Pagination.Ellipsis
                                    key={index}
                                    onClick={this.handleMoveLeft}
                                    className={classes.item}
                                />
                            );
                        } if (page === RIGHT_PAGE) {
                            return (
                                <Pagination.Ellipsis
                                    key={index}
                                    onClick={this.handleMoveRight}
                                    className={classes.item}
                                />
                            );
                        }
                        return (
                            <Pagination.Item
                                key={index}
                                active={this.props.currentPage === page}
                                onClick={e => this.handleClick(page, e)}
                                className={classes.item}
                            >
                                {page}
                            </Pagination.Item>
                        );
                    })}
                    {this.props.currentPage !== Math.ceil(this.props.totalRecords / this.props.pageLimit) &&
                    <Pagination.Next
                        onClick={this.handleMoveNext}
                        className={classes.item}
                    />
                    }
                </Pagination>
            </Fragment>
        );
    }
}

export default CustomPagination;
