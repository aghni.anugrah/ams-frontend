import React, { Component } from 'react'
import {PieChart, Pie, Cell, Legend, ResponsiveContainer} from 'recharts';

const OUTSIDE_COLORS = ['#97C444', '#37AB6A', '#CC1D57', '#A1377F','#007296', '#009AA3'];
const INSIDE_COLORS = ['#FECF28', '#D83615', '#1A4A73'];

class CustomTwoLevelPieChart extends Component {
    render() {
        return (
            <React.Fragment>
                <h5 className="text-center font-weight-bold">{this.props.title}</h5>
                <p className="text-center font-italic">{this.props.subtitle}</p>
                <ResponsiveContainer width="100%" height="85%">
                    <PieChart label="true">
                        <Pie data={this.props.insideLayerData} dataKey="value" outerRadius={75} fill="#8884d8"
                             nameKey="name" legendType="circle"
                        >
                            {this.props.insideLayerData.map((entry, index) => <Cell key={`cell-${index}`} fill={INSIDE_COLORS[index % INSIDE_COLORS.length]} />)}
                        </Pie>
                        <Pie
                            label
                            data={this.props.outsideLayerData}
                            innerRadius={75}
                            outerRadius={100}
                            fill="#82ca9d"
                            dataKey="value"
                            legendType="circle"
                        >
                            {this.props.outsideLayerData.map((entry, index) => <Cell key={`cell-${index}`} fill={OUTSIDE_COLORS[index % OUTSIDE_COLORS.length]} />)}
                        </Pie>
                        <Legend />
                    </PieChart>
                </ResponsiveContainer>
            </React.Fragment>
        )
    }
}

export default CustomTwoLevelPieChart