import React, { Component } from 'react'
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';
import Label from "recharts/lib/component/Label";

class CustomBarChart extends Component {
    render() {
        console.log(this.props.data.length*50);
        return (
            <React.Fragment>
                <h5 className="text-center font-weight-bold">{this.props.title}</h5>
                <p className="text-center font-italic">{this.props.subtitle}</p>
                <ResponsiveContainer width="100%" height="80%">
                    <BarChart
                        layout={this.props.layout}
                        width={500}
                        height={this.props.data.length*1000}
                        data={this.props.data}
                        margin={{left: 30 }}
                    >
                        <CartesianGrid stroke="#f5f5f5"/>
                        <XAxis dataKey={this.props.dataKeyX} type={this.props.dataTypeX} >
                            <Label value={this.props.labelX} position="bottom"/>
                        </XAxis>
                        <YAxis dataKey={this.props.dataKeyY} type={this.props.dataTypeY} >
                            <Label value={this.props.labelY} position="left" angle={-90} dx={-20} dy={-100}/>
                        </YAxis>
                        <Tooltip />
                        <Legend verticalAlign="bottom" align="center" wrapperStyle={{position:'bottom'}}/>
                        <Bar dataKey={this.props.dataKeyStackedVisit} stackId="a" fill="#FECF28" />
                        <Bar dataKey={this.props.dataKeyStackedActivation} stackId="a" fill="#D83615" />
                        <Bar dataKey={this.props.dataKeyStackedNoo} stackId="a" fill="#1A4A73" />
                    </BarChart>
                </ResponsiveContainer>
            </React.Fragment>
        )
    }
}

export default CustomBarChart