import React, { Component } from 'react'
import CustomTwoLevelPieChart from "../CustomRecharts/CustomPieChart/CustomTwoLevelPieChart";
import CustomBarChart from "../CustomRecharts/CustomBarChart/CustomBarChart";
import PenugasanCompletionCard from "../Cards/PenugasanCompletionCard";

class LeadSalesReport extends Component {
    renderBestSalespersonCompletionCard(data) {
        if (data !== undefined) {
            return data.map((metrics, index) => {
                    return (
                        <div className="col-sm-4" key={index}>
                            <PenugasanCompletionCard data={metrics} type={index}/>
                        </div>
                    )
                }
            )
        }
    }

    render() {
        return (
            <React.Fragment>
                <div className="justify-content-center">
                    <br/>
                    <div style={{height:'600px'}}>
                        <CustomTwoLevelPieChart insideLayerData={this.props.overviewPie.insideLayer} outsideLayerData={this.props.overviewPie.outsideLayer}
                                                title="Kinerja Kolektif Tim" subtitle={this.props.periode}
                        />
                    </div>
                    <br/><br/>
                    <div style={{height:`${this.props.overviewBar.length*100}px`}}>
                        <CustomBarChart layout="vertical" data={this.props.overviewBar} dataTypeX="number" dataKeyY="name" dataTypeY="category"
                                        dataKeyStackedVisit="Visit" dataKeyStackedActivation="Activation" dataKeyStackedNoo="NOO"
                                        title="Kinerja Per Salesperson" subtitle={this.props.periode} labelX="Jumlah Capaian" labelY="Nama Salesperson"
                        />
                    </div>
                    {this.props.overviewBestSales !== null &&
                        <div>
                            <br/>
                            <h5 className="text-center font-weight-bolder">Anggota Tim Terbaik</h5>
                            <p className="text-center font-italic">{this.props.periode}</p>
                            <p className="text-center font-weight-bold">{this.props.bestSalesName}</p>
                            <div className="row">
                                {this.renderBestSalespersonCompletionCard(this.props.overviewBestSales)}
                            </div>
                        </div>
                    }
                </div>
            </React.Fragment>
        )
    }
}

export default LeadSalesReport