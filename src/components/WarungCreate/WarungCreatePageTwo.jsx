import React, {Component} from "react";
import "react-bootstrap"

class WarungCreatePageTwo extends Component {
  render() {
    return (
      <div>
        <form>
          <div className="form-group">
            <label htmlFor="namaWarung">Nama Warung</label>
            <input type="text" className="form-control" onChange={this.props.handleChange}
                   value={this.props.state.warung_name} name="warung_name" id="namaWarung"/>
          </div>
          <div className="form-group">
            <label htmlFor="alamat">Alamat</label>
            <input type="text" className="form-control" onChange={this.props.handleChange}
                   value={this.props.state.address} name="address" id="alamat"/>
          </div>
          <div className="form-group">
            <label htmlFor="rtrw">RT/RW</label>
            <input type="text" className="form-control" onChange={this.props.handleChange}
                   value={this.props.state.rt_rw} name="rt_rw" id="rtrw"/>
          </div>
          <div className="form-group">
            <label htmlFor="patokan">Patokan Lokasi Warung/Usaha</label>
            <textarea className="form-control" onChange={this.props.handleChange} value={this.props.state.point_land}
                      name="point_land" id="patokan" rows="3"/>
          </div>
        </form>
        <div className="text-center row mb-2">
          <div className="col">
            <button onClick={() => this.props.handlePage(1)} type="button" className="btn btn-warning">kembali
            </button>
          </div>
          <div className="col">
            <button onClick={this.props.handleConfirm} type="button" className="btn btn-warning">simpan</button>
          </div>
        </div>
      </div>
    );
  }
}

export default WarungCreatePageTwo;