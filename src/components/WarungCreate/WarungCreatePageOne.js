import React, {Component} from "react";
import "react-bootstrap"

class WarungCreatePageOne extends Component {
  render() {
    let wilayah = this.props.state.wilayah.map((wilayah, index) =>
      <option key={index} value={wilayah}>{wilayah}</option>
    );
    return (
      <div>
        <form>
          <div className="form-group">
            <label htmlFor="refferalcode">Refferal Code</label>
            <input type="text" className="form-control" id="refferalcode" value={this.props.state.sales_code}
                   name="sales_code" readOnly/>
          </div>
          <div className="form-group">
            <label htmlFor="namaLengkap">Nama Lengkap</label>
            <input type="text" onChange={this.props.handleChange} value={this.props.state.mitra_name}
                   className="form-control" name="mitra_name" id="namaLengkap"/>
          </div>
          <div className="form-group">
            <label htmlFor="noKTP">No. KTP</label>
            <input type="number" onChange={this.props.handleChange} value={this.props.state.ktp_id}
                   className="form-control" name="ktp_id" id="noKTP"/>
          </div>
          <div className="form-group">
            <label htmlFor="tanggalLahir">Tanggal Lahir</label>
            <input type="date" className="form-control" onChange={this.props.handleChange}
                   value={this.props.state.birth} name="birth" id="tanggalLahir"/>
          </div>
          <div className="form-group">
            <label htmlFor="noHP">No. HP Aktif</label>
            <div className="input-group">
              <div className="input-group-prepend">
                <span className="input-group-text" id="calling-code">+62</span>
              </div>
              <input type="number" className="form-control" onChange={this.props.handleChange}
                     value={this.props.state.phone_number} name="phone_number" aria-describedby="calling-code"/>
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="noWA">No. WA</label>
            <select onChange={this.props.handleChange} value={this.props.state.wa_different} className="form-control"
                    name="wa_different" id="noWA">
              <option value={false}>Sama dengan No. HP</option>
              <option value={true}>Berbeda dengan No. HP</option>
            </select>
            <div className="input-group mt-1" hidden={!this.props.state.wa_different}>
              <div className="input-group-prepend">
                <span className="input-group-text" id="calling-code">+62</span>
              </div>
              <input type="number" className="form-control" onChange={this.props.handleChange}
                     value={this.props.state.whatsapp_phone_number} name="whatsapp_phone_number"
                     aria-describedby="calling-code" placeholder="Isi di sini"/>
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="jenisUsaha">Jenis Usaha</label>
            <select className="form-control" onChange={this.props.handleChange} value={this.props.state.business_type}
                    name="business_type" id="jenisUsaha">
              <option value="lainnya">Lainnya</option>
              <option value="gerobak">Warung Gerobak</option>
              <option value="nasi">Warung Nasi</option>
              <option value="kelontong">Warung Kelontong</option>
              <option value="kopi">Warung Kopi</option>
              <option value="tidak">Tidak Memiliki Warung</option>
            </select>
            <input type="text" className="form-control mt-1" onChange={this.props.handleChange}
                   value={this.props.state.business_type} hidden={!this.props.state.business_type_different}
                   placeholder="Isi di sini" name="business_type" id="jenisUsahaText"/>
          </div>
          <div className="form-group">
            <label htmlFor="kelurahan">Kelurahan</label>
            <input type="text" className="form-control" onChange={this.props.handleChange}
                   value={this.props.state.village_name} name="village_name" id="kelurahan"/>
          </div>
          <div className="form-group">
            <label htmlFor="kota">Kota</label>
            <select className="form-control" onChange={this.props.handleChange} value={this.props.state.kota}
                    name="city_name" id="kota">
              {wilayah}
            </select>
          </div>
          <fieldset className="form-group">
            <div className="row">
              <legend className="col-form-label col-sm-2 pt-0">Jalan Menuju Warung Cukup Dilalui 1 Mobil dan 1 Motor?
              </legend>
              <div className="col-sm-10">
                <div className="form-check">
                  <input className="form-check-input" type="radio" name="road_access_enough" id="yaJalan" value={true}
                         onChange={this.props.handleChange} checked={this.props.state.road_access_enough}/>
                  <label className="form-check-label" htmlFor="yaJalan">
                    Ya
                  </label>
                </div>
                <div className="form-check">
                  <input className="form-check-input" type="radio" name="road_access_enough" id="tidakJalan"
                         value={false} onChange={this.props.handleChange}
                         checked={!this.props.state.road_access_enough}/>
                  <label className="form-check-label" htmlFor="tidakJalan">
                    Tidak
                  </label>
                </div>
              </div>
            </div>
          </fieldset>
          <fieldset className="form-group">
            <div className="row">
              <legend className="col-form-label col-sm-2 pt-0">Menggunakan Smartphone Android?</legend>
              <div className="col-sm-10">
                <div className="form-check">
                  <input className="form-check-input" type="radio" name="smartphone_access" id="yaSmartphone"
                         value={true} onChange={this.props.handleChange} checked={this.props.state.smartphone_access}/>
                  <label className="form-check-label" htmlFor="yaSmartphone">
                    Ya
                  </label>
                </div>
                <div className="form-check">
                  <input className="form-check-input" type="radio" name="smartphone_access" id="tidakSmartphone"
                         value={false} onChange={this.props.handleChange}
                         checked={!this.props.state.smartphone_access}/>
                  <label className="form-check-label" htmlFor="tidakSmartphone">
                    Tidak
                  </label>
                </div>
              </div>
            </div>
          </fieldset>
        </form>
        <div className="row">
          <button onClick={() => this.props.handlePage(2)} type="button"
                  className="btn btn-warning mx-auto mb-2">Lanjut
          </button>
        </div>
      </div>
    );
  }
}

export default WarungCreatePageOne;