import React from "react";
import {Modal} from "react-bootstrap"
import "../StatusTableWarung/Logo.css"

function WarungModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      dialogClassName = "modal-body-bg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Mitra
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4>{props.warung.warung_name}</h4>
        <p>
          Village Name: {props.warung.village_name}
        </p>
      </Modal.Body>
      {/*<Modal.Footer>*/}
      {/*  <Button onClick={props.onHide}>Close</Button>*/}
      {/*</Modal.Footer>*/}
    </Modal>
  );
}

export default WarungModal;