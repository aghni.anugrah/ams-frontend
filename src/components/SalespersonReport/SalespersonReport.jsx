import React, { Component, useState, useEffect } from 'react'
import CustomTwoLevelPieChartSales from "../CustomRechartsSales/CustomPieChartSales/CustomTwoLevelPieChartSales";
import CustomBarChartSales from "../CustomRechartsSales/CustomBarChartSales/CustomBarChartSales";
import logo from "./ilustration-periode.png";
import "./SalespersonReport.css"
import CustomTwoLevelPieChartSalesDesktop from "../CustomRechartsSales/CustomPieChartSales/CustomTwoLevelPieChartSalesDesktop";
import CustomBarChartSalesDesktop from "../CustomRechartsSales/CustomBarChartSales/CustomBarChartSalesDesktop";

const RenderPieChart = ({insideLayerData, outsideLayerData}) => {
    const [isDesktop, setDesktop] = useState(window.innerWidth > 575);
    const updateMedia = () => {
        setDesktop(window.innerWidth > 575);
    };

    useEffect(() => {
        window.addEventListener("resize", updateMedia);
        return () => window.removeEventListener("resize", updateMedia);
    });

    return (
        isDesktop ? (
            <React.Fragment>
                <div id="piechart-desktop">
                    <CustomTwoLevelPieChartSalesDesktop insideLayerData={insideLayerData} outsideLayerData={outsideLayerData} />
                </div>
            </React.Fragment>
        ) : (
            <React.Fragment>
                <div id="piechart">
                    <CustomTwoLevelPieChartSales insideLayerData={insideLayerData} outsideLayerData={outsideLayerData} />
                </div>
            </React.Fragment>
        )
    )
};

const RenderBarChart = ({data}) => {
    const [isDesktop, setDesktop] = useState(window.innerWidth > 575);
    const updateMedia = () => {
        setDesktop(window.innerWidth > 575);
    };

    useEffect(() => {
        window.addEventListener("resize", updateMedia);
        return () => window.removeEventListener("resize", updateMedia);
    })

    return (
        isDesktop ? (
            <React.Fragment>
                <div id="barchart-desktop">
                    <CustomBarChartSalesDesktop data={data}/>
                </div>
            </React.Fragment>
        ) : (
            <React.Fragment>
                <div id="barchart">
                    <CustomBarChartSales data={data}/>
                </div>
            </React.Fragment>
        )
    )
};


class SalespersonReport extends Component {

    onPieEnter = (data, index) => {
        this.setState({
            activeIndex: index,
        });
    };

    parseData = (data) => {
        return data.map((item) => ({
            date: new Date(Date.parse(item.start_date)).toLocaleDateString('id-ID').toString().split("/")[0] +"/"
                + new Date(Date.parse(item.start_date)).toLocaleDateString('id-ID').toString().split("/")[1]+ '-'
                + new Date(Date.parse(item.end_date)).toLocaleDateString('id-ID').toString().split("/")[0] +"/"
                +  new Date(Date.parse(item.end_date)).toLocaleDateString('id-ID').toString().split("/")[1],
            Visit: item.Visit,
            Activation: item.Activation,
            NOO: item.NOO
        }));
    }


    render() {
        return (
            <React.Fragment>
                <div className="text-center">
                    {this.props.individualPie.insideLayer[0].value !== 0 ?
                        <div>
                            <h2 id="judul-report-2">Kinerja Keseluruhan</h2>
                            <h2 id="judul-report-periode">{this.props.periode}</h2>
                            <RenderPieChart insideLayerData={this.props.individualPie.insideLayer} outsideLayerData={this.props.individualPie.outsideLayer}/>

                        </div>
                        :
                        <div>
                            <p id="nodata">Tidak ada data</p>
                            <img src={logo} style={{height:"auto", width:"auto"}} alt="logo" className="mx-auto mt-2 center"/>
                            <p id="sub-nodata">Data penugasan untuk rentang periode yang diberikan tidak ditemukan</p>
                            <p id="sub-nodata-2">Tolong masukan kembali rentang periode yang baru</p>
                         </div>
                    }
                    {this.props.currentIndividualBar.length !== 0 ?
                        <div>
                            <h2 id="judul-report-2">Kinerja Per Penugasan</h2>
                            <h2 id="judul-report-periode">{this.props.periode}</h2>
                                <RenderBarChart data={this.parseData(this.props.currentIndividualBar)}/>
                        </div> : null}
                </div>
            </React.Fragment>
            )
        }
    }
export default SalespersonReport