import React, {Component} from "react";
import "./Status.css"
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faBoxOpen, faCheck, faClock, faHandshake, faShippingFast, faTimes} from '@fortawesome/free-solid-svg-icons'


class Status extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="status-ver pt-2">
          <div className="row row-cols-2">
            <div className="col-11">
              <div className="row">
                <div className="verification col">
                  <h4>Status Verifikasi:</h4>
                  <p><span><FontAwesomeIcon icon={faCheck}/></span> Berhasil</p>
                  <p><span><FontAwesomeIcon icon={faTimes}/></span> Gagal</p>
                  <p><span><FontAwesomeIcon icon={faClock}/></span> Proses</p>
                </div>
                <div className="col">
                  <h4>Status First Order:</h4>
                  <p><span><FontAwesomeIcon icon={faHandshake}/></span> Diterima</p>
                  <p><span><FontAwesomeIcon icon={faBoxOpen}/></span> Gagal</p>
                  <p><span><FontAwesomeIcon icon={faShippingFast}/></span> Diantar</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Status;