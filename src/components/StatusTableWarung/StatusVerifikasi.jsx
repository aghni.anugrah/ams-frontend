import React, {Component} from "react";
import "./Status.css"
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faClock, faTimes} from '@fortawesome/free-solid-svg-icons'


class StatusVerifikasi extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="row">
                    <div className="col">
                        <p>Status Verifikasi:</p>
                        <p><span><FontAwesomeIcon icon={faTimes}/></span> Ditolak </p>
                        <p><span><FontAwesomeIcon icon={faClock}/></span> Belum disetujui</p>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default StatusVerifikasi;