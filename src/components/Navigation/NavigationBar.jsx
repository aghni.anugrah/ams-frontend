import React, { Component } from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Dropdown from "react-bootstrap/Dropdown";
import NavItem from "react-bootstrap/NavItem";
import NavLink from "react-bootstrap/NavLink";
import "./NavigationBar.css"
import logo from "./warpin-logo.png"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons/faBars";

class NavigationBar extends Component {
    checkUndefined() {
        if (this.props.roleUser === undefined) {
            return (
                    <Navbar className="color-nav" expand="lg" fixed="top" style={{marginLeft: "-15px", marginRight: "-15px"}}>
                        <Navbar.Brand href="/" style={{color: '#FECF28'}}>
                            <img
                                id="logoWarpin"
                                src={logo}
                                className="d-inline-block align-top"
                                alt="Warung Pintar"/>
                        </Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav">
                            <FontAwesomeIcon icon={faBars}/>
                        </Navbar.Toggle>
                        {this.props.roleUser === "lead_sales" && this.props.loggedIn &&
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="ml-auto">
                                <Nav.Link href="/" style={{color: '#FECF28'}}>Dashboard</Nav.Link>
                                <Nav.Link href="/salesperson-control" style={{color: '#FECF28'}}>Tim</Nav.Link>
                                <Dropdown as={NavItem} alignRight>
                                    {this.props.namaUser !== undefined ?
                                        <Dropdown.Toggle as={NavLink}
                                                         style={{color: '#FECF28'}}>Halo {this.props.namaUser.split(" ")[0]} !</Dropdown.Toggle>
                                        :
                                        <Dropdown.Toggle as={NavLink} style={{color: '#FECF28'}}>Halo!</Dropdown.Toggle>
                                    }
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={this.props.onLogout}>Logout</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Nav>
                        </Navbar.Collapse>
                        }
                        {this.props.roleUser === "salesperson" && this.props.loggedIn &&
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="ml-auto">
                                <Nav.Link href="/" style={{color: '#FECF28'}}>Dashboard</Nav.Link>
                                <Nav.Link href="/target" style={{color: '#FECF28'}}>Penugasan</Nav.Link>
                                <Nav.Link href="/table" style={{color: '#FECF28'}}>Mitra</Nav.Link>
                                <Dropdown as={NavItem} alignRight>
                                    {this.props.namaUser !== undefined ?
                                        <Dropdown.Toggle as={NavLink} style={{color: '#FECF28'}}>Halo {this.props.namaUser.split(" ")[0]} !</Dropdown.Toggle>
                                        :
                                        <Dropdown.Toggle as={NavLink} style={{color: '#FECF28'}}>Halo!</Dropdown.Toggle>
                                    }
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={this.props.onLogout}>Logout</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Nav>
                        </Navbar.Collapse>
                        }
                        {this.props.roleUser === "regional_lead" && this.props.loggedIn &&
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="ml-auto">
                                <Nav.Link href="/" style={{color: '#FECF28'}}>Dashboard</Nav.Link>
                                <Dropdown as={NavItem} alignRight>
                                    {this.props.namaUser !== undefined ?
                                        <Dropdown.Toggle as={NavLink}
                                                         style={{color: '#FECF28'}}>Halo {this.props.namaUser.split(" ")[0]} !</Dropdown.Toggle>
                                        :
                                        <Dropdown.Toggle as={NavLink} style={{color: '#FECF28'}}>Halo!</Dropdown.Toggle>
                                    }
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={this.props.onLogout}>Logout</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Nav>
                        </Navbar.Collapse>
                        }
                        {this.props.roleUser === "validator" && this.props.loggedIn &&
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="ml-auto">
                                <Nav.Link href="/" style={{color: '#FECF28'}}>Dashboard</Nav.Link>
                                <Nav.Link href="/validasi/view-all" style={{color: '#FECF28'}}>Mitra</Nav.Link>
                                <Dropdown as={NavItem} alignRight>
                                    {this.props.namaUser !== undefined ?
                                        <Dropdown.Toggle as={NavLink}
                                                         style={{color: '#FECF28'}}>Halo {this.props.namaUser.split(" ")[0]} !</Dropdown.Toggle>
                                        :
                                        <Dropdown.Toggle as={NavLink} style={{color: '#FECF28'}}>Halo!</Dropdown.Toggle>
                                    }
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={this.props.onLogout}>Logout</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Nav>
                        </Navbar.Collapse>
                        }
                        {!this.props.loggedIn &&
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="ml-auto">
                                <Nav.Link href="https://accounts-staging.warungpintar.co/oauth/login?client_id=5&redirect_uri=http://localhost:3000&response_type=code&state=xyz" style={{color: '#FECF28'}}>Login</Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                        }
                    </Navbar>
            )
        } else {
            return (
                    <Navbar className="color-nav" expand="lg" fixed="top" style={{marginLeft: "-15px", marginRight: "-15px"}}>
                        <Navbar.Brand href="/" style={{color: '#FECF28'}}>
                            <img
                                id="logoWarpin"
                                src={logo}
                                className="d-inline-block align-top"
                                alt="Warung Pintar"/>
                        </Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav">
                            <FontAwesomeIcon icon={faBars}/>
                        </Navbar.Toggle>
                        {this.props.roleUser.toLowerCase() === "lead_sales" && this.props.loggedIn &&
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="ml-auto">
                                <Nav.Link href="/" style={{color: '#FECF28'}}>Dashboard</Nav.Link>
                                <Nav.Link href="/target-penugasan/view-all" style={{color: '#FECF28'}}>Penugasan</Nav.Link>
                                <Nav.Link href="/salesperson-control" style={{color: '#FECF28'}}>Tim</Nav.Link>
                                <Dropdown as={NavItem} alignRight>
                                    {this.props.namaUser !== undefined ?
                                        <Dropdown.Toggle as={NavLink}
                                                         style={{color: '#FECF28'}}>Halo {this.props.namaUser.split(" ")[0]} !</Dropdown.Toggle>
                                        :
                                        <Dropdown.Toggle as={NavLink} style={{color: '#FECF28'}}>Halo!</Dropdown.Toggle>
                                    }
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={this.props.onLogout}>Logout</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Nav>
                        </Navbar.Collapse>
                        }
                        {this.props.roleUser.toLowerCase() === "salesperson" && this.props.loggedIn &&
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="ml-auto">
                                <Nav.Link href="/" style={{color: '#FECF28'}}>Dashboard</Nav.Link>
                                <Nav.Link href="/target" style={{color: '#FECF28'}}>Penugasan</Nav.Link>
                                <Dropdown as={NavItem}>
                                    <Dropdown.Toggle as={NavLink} style={{color: '#FECF28'}}>Mitra</Dropdown.Toggle>
                                    <Dropdown.Menu>
                                        <Dropdown.Item href="/table">Registrasi</Dropdown.Item>
                                        <Dropdown.Item href="/warung-tidak-registrasi/view-all">Tidak Registrasi</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                                <Dropdown as={NavItem} alignRight>
                                    {this.props.namaUser !== undefined ?
                                        <Dropdown.Toggle as={NavLink} style={{color: '#FECF28'}}>Halo {this.props.namaUser.split(" ")[0]} !</Dropdown.Toggle>
                                        :
                                        <Dropdown.Toggle as={NavLink} style={{color: '#FECF28'}}>Halo!</Dropdown.Toggle>
                                    }
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={this.props.onLogout}>Logout</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Nav>
                        </Navbar.Collapse>
                        }
                        {this.props.roleUser.toLowerCase() === "regional_lead" && this.props.loggedIn &&
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="ml-auto">
                                <Nav.Link href="/" style={{color: '#FECF28'}}>Dashboard</Nav.Link>
                                <Dropdown as={NavItem} alignRight>
                                    {this.props.namaUser !== undefined ?
                                        <Dropdown.Toggle as={NavLink} style={{color: '#FECF28'}}>Halo {this.props.namaUser.split(" ")[0]} !</Dropdown.Toggle>
                                        :
                                        <Dropdown.Toggle as={NavLink} style={{color: '#FECF28'}}>Halo!</Dropdown.Toggle>
                                    }
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={this.props.onLogout}>Logout</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Nav>
                        </Navbar.Collapse>
                        }
                        {this.props.roleUser.toLowerCase() === "validator" && this.props.loggedIn &&
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="ml-auto">
                                <Nav.Link href="/validasi/view-all" style={{color: '#FECF28'}}>Mitra</Nav.Link>
                                <Dropdown as={NavItem} alignRight>
                                    {this.props.namaUser !== undefined ?
                                        <Dropdown.Toggle as={NavLink} style={{color: '#FECF28'}}>Halo {this.props.namaUser.split(" ")[0]} !</Dropdown.Toggle>
                                        :
                                        <Dropdown.Toggle as={NavLink} style={{color: '#FECF28'}}>Halo!</Dropdown.Toggle>
                                    }
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={this.props.onLogout}>Logout</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Nav>
                        </Navbar.Collapse>
                        }
                        {!this.props.loggedIn &&
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="ml-auto">
                                <Nav.Link href="https://accounts-staging.warungpintar.co/oauth/login?client_id=5&redirect_uri=http://localhost:3000&response_type=code&state=xyz" style={{color: '#FECF28'}}>Login</Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                        }
                    </Navbar>
            )
        }
    }

    render() {
        return(
            <React.Fragment>
                {this.checkUndefined()}
            </React.Fragment>
        )
    }
}

export default NavigationBar;