import React, {Component} from "react";
import Card from 'react-bootstrap/Card'
import Accordion from "react-bootstrap/Accordion";
import './RegionalStatistics.module.css'
import classes from './RegionalStatistics.module.css'

class RegionalMonthStatistics extends Component {
    renderStatistics() {
        if (this.props.data.length === 0) {
            return (
                <tr>
                    <td>Tidak ada statistik yang dapat ditampilkan</td>
                    <td/>
                    <td/>
                </tr>
            )
        } else {
            return this.props.data.map((bulan, index) => {
                return (
                    <tr key={index}>
                        <td>{bulan.Bulan}</td>
                        <td>{bulan.TotalAkuisisi}</td>
                        <td>{bulan.Akumulatif}</td>
                    </tr>
                );
            })
        }
    }

    render() {
        return (
            <Accordion defaultActiveKey="1">
                <Card className="shadow-lg mb-5 bg-white rounded">
                    <Accordion.Toggle as={Card.Header} eventKey="1">
                        <h3 className="font-weight-bold" style={{color: '#3d405c'}}>Statistik Akuisisi Setiap Bulan</h3>
                    </Accordion.Toggle>
                    <Accordion.Collapse eventKey="1" style={{width: '100%'}}>
                        <Card.Body  id={classes.table}>
                            <table className="table table-hover">
                                <thead className="Header">
                                <tr>
                                    <th scope="col" className="font-weight-bold">Bulan</th>
                                    <th scope="col" className="font-weight-bold">Total Akuisisi</th>
                                    <th scope="col" className="font-weight-bold">Akuisisi Akumulatif</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.renderStatistics()}
                                </tbody>
                            </table>
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        )
    }
}

export default RegionalMonthStatistics