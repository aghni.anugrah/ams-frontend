import React, { Component } from 'react'
import {CartesianGrid, XAxis, YAxis, Tooltip, Bar, ResponsiveContainer, Line, ComposedChart} from 'recharts';

const CustomTooltip = ({ active, payload, label }) => {
    if (active) {
        return (
            <div className="custom-tooltip">
                <p className="label">{`${label} : ${payload[0].value}`}</p>
            </div>
        );
    }
    return null;
};

class RegionalSalesReport extends Component{
    render() {
        return(
            <React.Fragment>
                <ResponsiveContainer width="100%" height="100%">
                    <ComposedChart
                        data={this.props.data}
                        margin={{
                            top: 20, right: 30, left: 20, bottom: 5,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis tick={{fontSize: 14}} dataKey="Bulan" type="category"/>
                        <YAxis tick={{fontSize: 14}} domain={['auto', 'auto']} type="number"/>
                        <Bar dataKey="Akumulatif" stackId="a" barSize={10} fill="#FECF28" />
                        <Line type="monotone" dataKey="Akumulatif" stroke="#ff7300" />
                        <Tooltip content={<CustomTooltip/>} />
                    </ComposedChart>
                </ResponsiveContainer>
            </React.Fragment>
        )
    }
}

export default RegionalSalesReport