import React, { Component } from 'react'
import {PieChart, Pie, Cell, Label, ResponsiveContainer} from 'recharts';
const COLORS = ['#FECF28', '#FFEACC'];

function CustomLabel({viewBox, value1}){
    const {cx, cy} = viewBox;
    return (
        <text x={cx} y={cy} fill="#3d405c" className="recharts-text recharts-label" textAnchor="middle" dominantBaseline="central">
            <tspan fontSize="16">{value1}</tspan>
        </text>
    )
}


class RegionalCityOverview extends Component {
    state = {
        data: this.props.data
    }
    calculatePercentage() {
        let percentage;
        percentage = (this.props.data.beda_target[0].value / (this.props.data.beda_target[0].value + this.props.data.beda_target[1].value) * 100).toFixed(2)
        let percentageString;
        percentageString = percentage + " %"
        return percentageString;
    }
    render() {
        return (
            <React.Fragment>
                <ResponsiveContainer width="100%" height="55%">
                    <PieChart onMouseEnter={this.onPieEnter}>
                        <Pie
                            data={this.props.data.beda_target}
                            innerRadius={50}
                            outerRadius={65}
                            fill="#8884d8"
                            paddingAngle={5}
                            dataKey="value"
                        >
                            {
                                this.props.data.beda_target.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
                            }
                            <Label content={<CustomLabel value1={this.calculatePercentage()}/>} position="center" />
                        </Pie>
                    </PieChart>
                </ResponsiveContainer>
            </React.Fragment>
        );
    }
}

export default RegionalCityOverview;