import React, { Component } from 'react'
import Table from 'react-bootstrap/Table'
import classes from "./TableWarungNoreg.module.css";

class TableWarungNoreg extends Component {
    renderTableHeader() {
        return (
            <tr className={classes.Header}>
                <th>Nama Warung</th>
                <th>Tanggal Kunjungan</th>
            </tr>
        )
    }
    renderTableData() {
        return this.props.data.map((warung, index) => {
            return (
                <tr key={index} onClick={() => this.props.detail(warung)}>
                    <td>{warung.nama_warung}</td>
                    <td>{new Date(Date.parse(warung.tanggal_kunjungan)).toLocaleString('id-ID', {
                        year: 'numeric',
                        month: 'numeric',
                        day: 'numeric',
                        hour:'2-digit',
                        minute: '2-digit'
                    })}</td>
                </tr>
            )
        }
        )
    }
    render() {
        return (
            <div className="mt-2">
                <Table hover responsive style={{wordWrap:'break-word'}}>
                    <thead>
                        {this.renderTableHeader()}
                    </thead>
                    <tbody>
                        {this.renderTableData()}
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default TableWarungNoreg