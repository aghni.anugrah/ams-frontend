import React, {Component} from "react";
import "./Table.css";
import "../StatusTableWarung/Logo.css";
import {faCheck, faClock, faQuestion, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Table from "react-bootstrap/Table";

class VerifikasiTable extends Component {
    renderTableHeader() {
        return (
            <tr>
                <th>Lokasi</th>
                <th>Nama</th>
                <th>Status</th>
            </tr>
        )
    }
    renderTableData() {
        return this.props.data.map((mitra, index) => {
            let status;

            if (mitra.status === "registration_nil") {
                status = <FontAwesomeIcon icon={faClock} />
            } else if (mitra.status === "rejected") {
                status = <FontAwesomeIcon icon={faTimes} />
            } else {
                status = <FontAwesomeIcon icon={faQuestion} />
            }
                return (
                    <tr key={index} onClick={() => this.props.detail(mitra)}>
                        <td>{mitra.village_name}</td>
                        <td>{mitra.warung_name}</td>
                        <td>{status}</td>
                    </tr>
                )
            }
        )

    }
    render() {
        return (
            <div>
                <Table bordered hover responsive>
                    <thead>
                    {this.renderTableHeader()}
                    </thead>
                    <tbody>
                    {this.renderTableData()}
                    </tbody>
                </Table>
            </div>
        )
    }
}



export default VerifikasiTable;