import React, {Component} from "react";
import Table from 'react-bootstrap/Table'
import './Table.css'


class TargetPenugasanTable extends Component {
    renderTableHeader() {
        return (
            <tr>
                <th>No.</th>
                <th>Wilayah</th>
                <th>Periode</th>
            </tr>
        )
    }
    renderTableData() {
        var counter = 1
        return this.props.data.map((targetPenugasanInactive, index) => {
                return (
                    <tr key={index} onClick={() => this.props.detail(targetPenugasanInactive.target_penugasan)}>
                        <td>{counter++}</td>
                        <td>{targetPenugasanInactive.nama_wilayah}</td>
                        <td> {new Date(Date.parse(targetPenugasanInactive.target_penugasan.tanggal_mulai)).toLocaleDateString('id-ID')} to {new Date(Date.parse(targetPenugasanInactive.target_penugasan.tanggal_selesai)).toLocaleDateString('id-ID')}</td>
                    </tr>
                )
            }
        )

    }
    render() {
        return (
            <div>
                <Table bordered hover responsive>
                    <thead>
                    {this.renderTableHeader()}
                    </thead>
                    <tbody>
                    {this.renderTableData()}
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default TargetPenugasanTable;