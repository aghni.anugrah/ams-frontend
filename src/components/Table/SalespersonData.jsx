import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import './SalespersonTable.css'

class SalespersonData extends Component {
    renderSalespersonData() {
        if (this.props.data.length === 0) {
            return (
                <tr>
                    <td>Tidak ada anggota tim untuk ditampilkan</td>
                    <td/>
                </tr>
            )
        } else {
            return this.props.data.map((salesperson, index) => {
                return (
                    <tr key={index}>
                        <td onClick={() => this.props.detail(salesperson)}>{salesperson.nama}</td>
                        <td id="deleteButton"><button className="btn" onClick={() => this.props.confirmation(salesperson)}><FontAwesomeIcon icon={faTrash}/></button></td>
                    </tr>
                );
            })
        }
    }

    render() {
        return (
            <React.Fragment>
                        <tbody>
                            {this.renderSalespersonData()}
                        </tbody>
            </React.Fragment>
        )
    }
}

export default SalespersonData;