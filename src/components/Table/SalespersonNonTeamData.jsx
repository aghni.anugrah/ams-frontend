import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserPlus } from "@fortawesome/free-solid-svg-icons";
import './SalespersonTable.css'
//import Button from "react-bootstrap/Button";

class SalespersonNonTeamData extends Component {
    renderSalespersonData() {
        if (this.props.data.length === 0) {
            return (
                <tr>
                    <td>Tidak ada Salesperson yang dapat ditambahkan</td>
                </tr>
            )
        } else {
            return this.props.data.map((salesperson, index) => {
                return (
                    <tr key={index}>
                        <td onClick={() => this.props.detail(salesperson)}>{salesperson.nama}</td>
                        <td id="addUserButton"><button className="btn" onClick={() => this.props.confirmation(salesperson)}><FontAwesomeIcon icon={faUserPlus}/></button></td>
                    </tr>
                );
            })
        }
    }

    render() {
        return (
            <React.Fragment>
                <tbody>
                {this.renderSalespersonData()}
                </tbody>
            </React.Fragment>
        )
    }
}

export default SalespersonNonTeamData;