import React, { Component } from 'react'
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';

class CustomBarChartSales extends Component {
    render() {
        return (
            <React.Fragment>
                <BarChart
                    layout="vertical"
                    width={280}
                    height={300}
                    data={this.props.data}
                    margin={{
                        top: 20, right: 10, bottom: 20, left: 40,
                    }}
                >
                    <CartesianGrid stroke="#f5f5f5"/>

                    <XAxis type="number" label={{ value: "Kuota", position: "outsideBottom", dy: 20 }}> </XAxis>
                    <YAxis dataKey="date" type="category" label={{ value: "Periode", position: "outsideTop", dx: 30, dy: -105}}>
                    </YAxis>
                    <Tooltip/>
                    <Legend verticalAlign="bottom" align="center" wrapperStyle={{paddingTop: "20px"}}/>
                    <Bar dataKey="Visit" stackId="a" fill="#FECF28" dy="-10" />
                    <Bar dataKey="Activation" stackId="a" fill="#D83615"  />
                    <Bar dataKey="NOO" stackId="a" fill="#1A4A73"  />
                </BarChart>
            </React.Fragment>
        )
    }
}

export default CustomBarChartSales
