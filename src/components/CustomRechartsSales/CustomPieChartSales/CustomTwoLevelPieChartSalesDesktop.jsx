import React, { Component } from 'react'
import {PieChart, Pie, Cell, Legend} from 'recharts';

const RADIAN = Math.PI / 180;
const renderCustomizedInnerLabel = ({cx, cy, midAngle, innerRadius, outerRadius, nameKey, index,}) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
        // <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
        //     {nameKey}
        // </text>
        <text x={x} y={y} fill="white" textAnchor="middle" dominantBaseline="central">
            {`${nameKey}`}
        </text>
    );
};

const OUTSIDE_COLORS = ['#97C444', '#37AB6A', '#CC1D57', '#A1377F','#007296', '#009AA3'];
const INSIDE_COLORS = ['#FECF28', '#D83615', '#1A4A73'];

class CustomTwoLevelPieChartSalesDesktop extends Component {
    render() {
        return (
            <React.Fragment>
                <h5 className="text-center font-weight-bold">{this.props.title}</h5>
                <p className="text-center">{this.props.subtitle}</p>
                <PieChart label="true" width={925} height={300}>
                    <Pie data={this.props.insideLayerData} dataKey="value" cx={475} cy={115} outerRadius={60} fill="#8884d8"
                         nameKey="name" legendType="circle"
                    >
                        {this.props.insideLayerData.map((entry, index) => <Cell key={`cell-${index}`} fill={INSIDE_COLORS[index % INSIDE_COLORS.length]} />)}
                    </Pie>
                    <Pie
                        label
                        data={this.props.outsideLayerData}
                        cx={475}
                        cy={115}
                        innerRadius={60}
                        outerRadius={85}
                        fill="#82ca9d"
                        dataKey="value"
                        legendType="circle"
                    >
                        {this.props.outsideLayerData.map((entry, index) => <Cell key={`cell-${index}`} fill={OUTSIDE_COLORS[index % OUTSIDE_COLORS.length]} />)}
                    </Pie>
                    <Legend />
                </PieChart>
            </React.Fragment>
        )
    }
}

export default CustomTwoLevelPieChartSalesDesktop
