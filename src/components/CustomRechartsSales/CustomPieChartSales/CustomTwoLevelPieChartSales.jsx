import React, { Component } from 'react'
import {PieChart, Pie, Sector, Cell, Legend, ResponsiveContainer} from 'recharts';

const RADIAN = Math.PI / 180;
const renderCustomizedInnerLabel = ({cx, cy, midAngle, innerRadius, outerRadius, nameKey, index,}) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
        <text x={x} y={y} fill="white" textAnchor="middle" dominantBaseline="central">
            {`${nameKey}`}
        </text>
    );
};

const OUTSIDE_COLORS = ['#97C444', '#37AB6A', '#CC1D57', '#A1377F','#007296', '#009AA3'];
const INSIDE_COLORS = ['#FECF28', '#D83615', '#1A4A73'];

class CustomTwoLevelPieChartSales extends Component {
    render() {
        return (
            <React.Fragment>
                <h5 className="text-center font-weight-bold">{this.props.title}</h5>
                <p className="text-center">{this.props.subtitle}</p>
                <PieChart label="true" width={300} height={440}>
                    <Pie data={this.props.insideLayerData} dataKey="value" cx={145} cy={130} outerRadius={70} fill="#8884d8"
                         nameKey="name" legendType="circle"
                    >
                        {this.props.insideLayerData.map((entry, index) => <Cell key={`cell-${index}`} fill={INSIDE_COLORS[index % INSIDE_COLORS.length]} />)}
                    </Pie>
                    <Pie
                        label
                        data={this.props.outsideLayerData}
                        cx={145}
                        cy={130}
                        innerRadius={70}
                        outerRadius={95}
                        fill="#82ca9d"
                        dataKey="value"
                        legendType="circle"
                    >
                        {this.props.outsideLayerData.map((entry, index) => <Cell key={`cell-${index}`} fill={OUTSIDE_COLORS[index % OUTSIDE_COLORS.length]} />)}
                    </Pie>
                    <Legend />
                </PieChart>
            </React.Fragment>
        )
    }
}

export default CustomTwoLevelPieChartSales
