import React, {Component} from "react";
import {ProgressBar} from "react-bootstrap";

class ProgressBarSales extends React.Component {

    render() {
        const visit = this.props.visit;
        const aktivasi = this.props.aktivasi;
        const NOO = this.props.NOO;
        const kuota_visit = this.props.kuota_visit;
        const kuota_activation= this.props.kuota_activation;
        const kuota_noo = this.props.kuota_noo;
        const visit_value = visit/kuota_visit * 100;
        const aktivasi_value = aktivasi/kuota_activation * 100;
        const noo_value = NOO/kuota_noo * 100;
        return (
            <div>

                <p id="visit"> Kuota Visit</p>
                <ProgressBar variant="warning" now={visit_value} label={`${visit}`} />

                <p id="aktivasi"> Kuota Aktivasi</p>
                <ProgressBar variant="warning" now={aktivasi_value} label={`${aktivasi}`} />

                <p id="noo"> Kuota NOO</p>
                <ProgressBar variant="warning" now={noo_value} label={`${NOO}`} />
        </div>

        )
    }
}

export default ProgressBarSales;


