import React, {Component} from "react";

class LogoutButton extends Component {
  render() {
    return (
      <div>
        <button onClick={this.props.onLogout} type="button" className="btn btn-danger my-3">Logout</button>
      </div>
    );
  }
}

export default LogoutButton;