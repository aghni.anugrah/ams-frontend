import React, {Component} from 'react';
import Modal from 'react-bootstrap/Modal'
import {faQuestionCircle, faTimesCircle, faCircle} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheckCircle} from "@fortawesome/free-solid-svg-icons/faCheckCircle";

class CustomModal extends Component {
  renderSecondaryButton() {
    if (this.props.type === undefined) {
      if (this.props.secondaryButton !== undefined) {
        return (<button className="mr-2 btn btn-outline-light" style={{borderColor: '#FECF28', color: '#FECF28'}}
                        onClick={this.props.secondaryAction}>{this.props.secondaryButton.toUpperCase()}</button>)
      }
    } else {
      return this.props.type === "confirm" ?
        (
          this.props.secondaryAction !== undefined ?
            <button className="mr-2 btn btn-outline-light" style={{borderColor: '#FF9802', color: '#FF9802'}}
                    onClick={this.props.secondaryAction}>{this.props.secondaryButton.toUpperCase()}</button>
            :
            null
        )
        :
        (this.props.secondaryAction !== undefined ?
          <button className="mr-2 btn btn-outline-light" style={{borderColor: '#D83615', color: '#D83615'}}
                  onClick={this.props.secondaryAction}>{this.props.secondaryButton.toUpperCase()}</button> :
          null)
    }
  }

  renderPrimaryButton() {
    if (this.props.type === undefined) {
      return (<button className="btn btn-warning ml-2"
                      style={{backgroundColor: '#FECF28', borderColor: '#FECF28', color: '#000000'}}
                      onClick={this.props.primaryAction}>{this.props.primaryButton.toUpperCase()}</button>)
    } else {
      if (this.props.type === "success") {
        return (
          <button className="btn btn-warning ml-2"
                  style={{backgroundColor: '#8BB31E', borderColor: '#8BB31E', color: '#FFFFFF'}}
                  onClick={this.props.primaryAction}>{this.props.primaryButton.toUpperCase()}</button>
        )
      }
      return this.props.type === "confirm" ?
        (
          <button className="btn btn-warning ml-2"
                  style={{backgroundColor: '#FF9802', borderColor: '#FF9802', color: '#FFFFFF'}}
                  onClick={this.props.primaryAction}>{this.props.primaryButton.toUpperCase()}</button>
        )
        :
        (
          <button className="btn btn-warning ml-2"
                  style={{backgroundColor: '#D83615', borderColor: '#D83615', color: '#FFFFFF'}}
                  onClick={this.props.primaryAction}>{this.props.primaryButton.toUpperCase()}</button>
        )
    }
  }

  renderIcon() {
    if (this.props.type !== undefined && this.props.type === "success") {
      return (
        <FontAwesomeIcon
          icon={faCheckCircle}
          mask={faCircle}
          size="5x"
          transform="shrink-3"
          style={{color: '#8BB31E'}}
          className="text-center"
        />
      )
    }

    return this.props.type !== undefined ?
      (this.props.type === "confirm" ?
          (
            <FontAwesomeIcon
              icon={faQuestionCircle}
              mask={faCircle}
              size="5x"
              transform="shrink-3"
              style={{color: '#FF9802'}}
              className="text-center"
            />
          )
          :
          (
            <FontAwesomeIcon
              icon={faTimesCircle}
              mask={faCircle}
              size="5x"
              transform="shrink-3"
              style={{color: '#D83615'}}
              className="text-center"
            />
          )
      )
      :
      null
  }

  getSizeFromProps(){
        return this.props.size !== undefined ? this.props.size : "md"
    }

    render() {
        return(
            <React.Fragment>
                <Modal
                    show={this.props.show}
                    onHide={this.props.onHide}
                    size={this.getSizeFromProps()}
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Body>
            <h3 className="mb-4 text-center"><strong>{this.props.title}</strong></h3>
            <div className="text-center m-2">
              {this.renderIcon()}
            </div>
            <div className="mx-4 mb-1">
              {this.props.children}
            </div>
            <div className="text-center mb-2">
              {this.renderSecondaryButton()}
              {this.renderPrimaryButton()}
            </div>
          </Modal.Body>
        </Modal>
      </React.Fragment>
    );
  }
}

export default CustomModal;