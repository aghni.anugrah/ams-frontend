import React, {Component} from "react";
import Card from 'react-bootstrap/Card'
import './Card.css'


class TargetPenugasanCard extends Component {

    renderCardData() {
        if (this.props.data.Wilayah !== undefined) {
            return (
                <div>
                    <Card.Text> Wilayah : {this.props.data.Wilayah.nama_wilayah}</Card.Text>
                    <Card.Text> Periode Awal : {new Date(Date.parse(this.props.data.Wilayah.target_penugasan.tanggal_mulai)).toLocaleDateString('id-ID')} </Card.Text>
                    <Card.Text> Periode Akhir : {new Date(Date.parse(this.props.data.Wilayah.target_penugasan.tanggal_selesai)).toLocaleDateString('id-ID')} </Card.Text>
                </div>
            );
        }
    }

    render() {
        return (
            <div className="cardWilayah">
                <Card.Body>
                    {this.renderCardData()}
                </Card.Body>
            </div>
        )
    }
}



export default TargetPenugasanCard;

// <CardDeck>
//     <Card>
//         <Card.Img variant="top" src="holder.js/100px160" />
//         <Card.Body>
//             <Card.Title>Card title</Card.Title>
//             <Card.Text>
//                 This is a wider card with supporting text below as a natural lead-in to
//                 additional content. This content is a little bit longer.
//             </Card.Text>
//         </Card.Body>
//         <Card.Footer>
//             <small className="text-muted">Last updated 3 mins ago</small>
//         </Card.Footer>
//     </Card>
//     <Card>
//         <Card.Img variant="top" src="holder.js/100px160" />
//         <Card.Body>
//             <Card.Title>Card title</Card.Title>
//             <Card.Text>
//                 This card has supporting text below as a natural lead-in to additional
//                 content.{' '}
//             </Card.Text>
//         </Card.Body>
//         <Card.Footer>
//             <small className="text-muted">Last updated 3 mins ago</small>
//         </Card.Footer>
//     </Card>
//     <Card>
//         <Card.Img variant="top" src="holder.js/100px160" />
//         <Card.Body>
//             <Card.Title>Card title</Card.Title>
//             <Card.Text>
//                 This is a wider card with supporting text below as a natural lead-in to
//                 additional content. This card has even longer content than the first to
//                 show that equal height action.
//             </Card.Text>
//         </Card.Body>
//         <Card.Footer>
//             <small className="text-muted">Last updated 3 mins ago</small>
//         </Card.Footer>
//     </Card>
// </CardDeck>