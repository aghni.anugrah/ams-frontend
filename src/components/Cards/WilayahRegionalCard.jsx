import React, {Component} from "react";
import Card from 'react-bootstrap/Card'
import './Card.css'

class WilayahRegionalCard extends Component {
    render() {
        return (
            <div className='cardWilayahRegional'>
                <Card.Body>
                    <Card.Text style={{fontSize:'14px', fontWeight:'1000'}}>Wilayah: {this.props.data}</Card.Text>
                    <Card.Text style={{fontSize:'14px', fontWeight:'1000'}}>Tahun: {this.props.tahun}</Card.Text>
                </Card.Body>
            </div>
        )
    }
}

export default WilayahRegionalCard