import React, {Component} from "react";
import Card from 'react-bootstrap/Card'
import './Card.css'


class KuotaVisitCard extends Component {

    renderCardData=() => {
        if (this.props.data.Wilayah !== undefined) {
            return (
                <div className='cardVisit'>
                    <Card.Text style={{fontSize:'14px', fontWeight:'300'}}>VISIT</Card.Text>
                    <Card.Text>{this.props.data.Wilayah.target_penugasan.real_visit} / {this.props.data.Wilayah.target_penugasan.kuota_visit}</Card.Text>
                </div>

            );
        }
    }
    render() {
        return (
            <div className='cardVisit'>
                <Card.Body>
                    {this.renderCardData()}
                </Card.Body>
            </div>
        )
    }
}



export default KuotaVisitCard;

