import React, {Component} from "react";
import Card from 'react-bootstrap/Card'


class KuotaAktivasiCard extends Component {

    renderCardData() {
        if (this.props.data.Wilayah !== undefined) {
            return (
                <div className='cardAktivasi'>
                    <Card.Text style={{fontSize:'14px', fontWeight:'300'}}>AKTIVASI</Card.Text>
                    <Card.Text>{this.props.data.Wilayah.target_penugasan.real_activation} / {this.props.data.Wilayah.target_penugasan.kuota_activation}</Card.Text>
                </div>
            );
        }
    }
    render() {
        return (
            <div className='cardAktivasi'>
                <Card.Body>
                    {this.renderCardData()}
                </Card.Body>
            </div>
        )
    }
}



export default KuotaAktivasiCard;

