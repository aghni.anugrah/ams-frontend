import React, {Component} from "react";
import Card from 'react-bootstrap/Card'
import './Card.css'


class PenugasanTimActiveCard extends Component {

    renderCardData() {
        if (this.props.data !== undefined) {
            return (
                <div>
                    <Card.Text> Wilayah: {this.props.data.wilayah_name}</Card.Text>
                    <Card.Text> Tanggal mulai: {new Date(Date.parse(this.props.data.start_date)).toLocaleDateString('id-ID')} </Card.Text>
                    <Card.Text> Tanggal selesai: {new Date(Date.parse(this.props.data.end_date)).toLocaleDateString('id-ID')} </Card.Text>
                    <Card.Text> Jumlah anggota tim: {this.props.data.team_member_amount}</Card.Text>
                </div>
            );
        }
    }

    render() {
        return (
            <div className="cardWilayah">
                <Card.Body>
                    {this.renderCardData()}
                </Card.Body>
            </div>
        )
    }
}



export default PenugasanTimActiveCard;