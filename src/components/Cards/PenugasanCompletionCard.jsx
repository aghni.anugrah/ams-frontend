import React, {Component} from "react";
import Card from 'react-bootstrap/Card'
import './Card.css'
import CustomDonutChart from "../CustomRecharts/CustomPieChart/CustomDonutChart";

const RenderBestSalesCompletionCard = ({data, type}) => {
    return (
        <React.Fragment>
            {type === 0 && <Card.Body className="shadow-lg mb-5 bg-white rounded" id="completionVisitCardContainer">
                <p id="completionStats" className="text-center font-weight-bold">Visit</p>
                <CustomDonutChart data={data.donutData}/>
                <p id="completionStats">{data.actualData[0].name}</p>
                <p id="completionStatsAngka">{data.actualData[0].value}</p>
                <p id="completionStats">{data.actualData[1].name}</p>
                <p id="completionStatsAngka">{data.actualData[1].value}</p>
                <p id="completionStats">Target Tercapai Per Penugasan:</p>
                <p id="completionStatsAngka">{data.actualData[2].value}/{data.actualData[3].value}</p>
            </Card.Body>}
            {type === 1 && <Card.Body className="shadow-lg mb-5 bg-white rounded" id="completionActivationCardContainer">
                <p id="completionStats" className="text-center font-weight-bold">Activation</p>
                <CustomDonutChart data={data.donutData}/>
                <p id="completionStats">{data.actualData[0].name}</p>
                <p id="completionStatsAngka">{data.actualData[0].value}</p>
                <p id="completionStats">{data.actualData[1].name}</p>
                <p id="completionStatsAngka">{data.actualData[1].value}</p>
                <p id="completionStats">Target Tercapai Per Penugasan:</p>
                <p id="completionStatsAngka">{data.actualData[2].value}/{data.actualData[3].value}</p>
            </Card.Body>}
            {type === 2 && <Card.Body className="shadow-lg mb-5 bg-white rounded" id="completionNOOCardContainer">
                <p id="completionStats" className="text-center font-weight-bold">NOO</p>
                <CustomDonutChart data={data.donutData}/>
                <p id="completionStats">{data.actualData[0].name}</p>
                <p id="completionStatsAngka">{data.actualData[0].value}</p>
                <p id="completionStats">{data.actualData[1].name}</p>
                <p id="completionStatsAngka">{data.actualData[1].value}</p>
                <p id="completionStats">Target Tercapai Per Penugasan:</p>
                <p id="completionStatsAngka">{data.actualData[2].value}/{data.actualData[3].value}</p>
            </Card.Body>}
        </React.Fragment>
    );
};

class PenugasanCompletionCard extends Component {
    render() {
        return (
            <div className='cardCompletion'>
                <RenderBestSalesCompletionCard data={this.props.data[0]} type={this.props.type}/>
            </div>
        )
    }
}



export default PenugasanCompletionCard;

