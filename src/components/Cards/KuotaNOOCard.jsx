import React, {Component} from "react";
import Card from 'react-bootstrap/Card'


class KuotaNOOCard extends Component {

    renderCardData() {
        if (this.props.data.Wilayah !== undefined) {
            return (
                <div className='cardNOO'>
                    <Card.Text style={{fontSize:'14px', fontWeight:'300'}}>NOO</Card.Text>
                    <Card.Text>{this.props.data.Wilayah.target_penugasan.real_noo} / {this.props.data.Wilayah.target_penugasan.kuota_noo}</Card.Text>
                </div>
            );
        }
    }
    render() {
        return (
            <div className='cardNOO'>
                <Card.Body>
                    {this.renderCardData()}
                </Card.Body>
            </div>
        )
    }
}



export default KuotaNOOCard;

