import React, {Component, useEffect, useState} from "react";
import Card from 'react-bootstrap/Card'
import './Card.css'
import RegionalCityOverview from "../RegionalSalesReport/RegionalCityOverview";


const RenderCityOverview = ({data, month, year}) => {
    const URL = "/dashboard/regional/city/"
    const param = data.id_kota
    const yearTambahan = "/"+ year

    return (
            <React.Fragment>
                <a href={URL + param + yearTambahan} style={{color: '#3d405c'}}>
                    <Card.Body className="shadow-lg mb-5 bg-white rounded" id="cardContainer">
                        <h3 className="font-weight-bold" id="cityText">{data.kota}</h3>
                        <RegionalCityOverview data={data}/>
                        <p id="stats">Target Sampai Bulan {month}</p>
                        <p id="statsAngka">{data.target_akuisisi}</p>
                        <p id="stats">Pencapaian Sampai Bulan {month}</p>
                        <p id="statsAngka">{data.total_akuisisi}</p>
                    </Card.Body>
                </a>
            </React.Fragment>
    );
}

class StatistikKotaCard extends Component {
    render() {
        return (
            <div className='cardCity'>
                <RenderCityOverview data={this.props.data} month={this.props.month} year={this.props.year}/>
            </div>
        )
    }
}



export default StatistikKotaCard;

