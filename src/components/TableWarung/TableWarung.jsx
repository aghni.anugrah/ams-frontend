import React, {Component} from "react";
import "./TableWarung.css";
import "../StatusTableWarung/Logo.css";
import {faCheck, faClock, faQuestion, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

class TableWarung extends Component {
  renderTable = () => {
    return this.props.warung.map((warung, index) => {
      let status;
      let date = new Date(warung.updated_at).toLocaleString("id-ID");

      if (warung.state === "registration_done" || warung.state === "registration_accepted" ||
        warung.state === "validation_pending" || warung.state === "validation accepted") {
        status = <FontAwesomeIcon icon={faClock} />
      } else if (warung.state === "ready") {
        status = <FontAwesomeIcon icon={faCheck} />
      } else if (warung.state === "rejected") {
        status = <FontAwesomeIcon icon={faTimes} />
      } else {
        status = <FontAwesomeIcon icon={faQuestion} />
      }

      return (
        <tr key={index} onClick={() => this.props.modal(warung)}>
          <td>{warung.village_name}</td>
          <td>{warung.warung_name}</td>
          <td>{status}</td>
          <td>{date}</td>
        </tr>
      )
    })
  };

  render() {
    if (this.props.warung.length > 0 ) {
      return (
        <div className="card" id="table-warung">
          <div className="table-responsive">
            <table className="table">
              <thead>
              <tr>
                <th scope="col">Lokasi</th>
                <th scope="col">Nama</th>
                <th scope="col">Status</th>
                <th scope="col">Last Update</th>
              </tr>
              </thead>
              <tbody>
              {this.renderTable()}
              </tbody>
            </table>
          </div>
        </div>
      );
    } else {
      return (
        <h2>Data not available</h2>
      )
    }
  }
}

export default TableWarung;