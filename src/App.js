import React, {Component} from 'react';
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import Home from "./containers/Home/Home";
import Login from "./containers/Login/Login";
import TargetPenugasanCreate from "./containers/TargetPenugasan/TargetPenugasanCreate";
import TargetPenugasanViewAll from "./containers/TargetPenugasan/TargetPenugasanViewAll";
import WarungRegistrasi from "./containers/WarungRegistered/WarungRegistrasi";
import WarungNoregViewAll from "./containers/WarungNoregViewAll/WarungNoregViewAll";
import WarungNoregCreate from "./containers/WarungNoregCreate/WarungNoregCreate";
import SalespersonControl from "./containers/SalespersonControl/SalespersonControl";
import SalespersonAddition from "./containers/SalespersonControl/SalespersonAddition/SalespersonAddition";
import "./App.css"
import TargetPenugasan from "./containers/MelihatTargetPenugasan/TargetPenugasan";
import VerifikasiViewAll from "./containers/VerifikasiMitra/VerifikasiViewAll";
import "./App.css"
import NavigationBar from "./components/Navigation/NavigationBar";
import VerifikasiCreate from "./containers/VerifikasiMitra/VerifikasiCreate";
import HomeLeadSales from "./containers/HomeLeadSales/HomeLeadSales";
import HomeRegional from "./containers/HomeRegional/HomeRegional";
import HomeValidator from "./containers/HomeValidator/HomeValidator";
import CityRegional from "./containers/HomeRegional/CityRegional";
import WarungCreate from "./containers/WarungCreate/WarungCreate";
import WarungUpdate from "./containers/WarungUpdate/WarungUpdate";
import NotFound from "./containers/NotFound/NotFound";

class App extends Component {
  state = {
    loggedIn: false,
    namaUser: "",
    roleUser: "",
  };

  constructor(props) {
    super(props);
    const queryString = require("query-string");
    const parsed = queryString.parse(window.location.search);
    if (parsed.access_token !== undefined && !(parsed.access_token === "")) {
      localStorage.setItem("token", parsed.access_token);
    }
    if (localStorage.getItem("token") === undefined || localStorage.getItem("token") === null) {
      this.state = {loggedIn: false}
    } else {
      this.state = {loggedIn: true}
    }

    if ((!this.state.namaUser || !this.state.roleUser) && this.state.loggedIn) {
      this.fetchRoleandName();
    }
  }

  handleLogout = () => {
    this.setState({
      loggedIn: false,
      namaUser: "",
      roleUser: "",
    });
    localStorage.clear();
  };

  fetchRoleandName = () => {
    const token = "Bearer " + localStorage.getItem("token");
    const URL = "http://localhost:8080";
    fetch(URL + "/navbar-role", {
      method: "GET",
      headers: {
        "Authorization": token,
      }
    }).then(response => {
      return response.json();
    }).then((data) => {
      let nameUser = data.nama;
      let userRole = data.role;
      this.setState({
        namaUser: nameUser,
        roleUser: userRole,
      });
    }).catch()
  };

  render() {
    return (
      <div className="App">
        <NavigationBar namaUser={this.state.namaUser} roleUser={this.state.roleUser} loggedIn={this.state.loggedIn}
                       onLogout={this.handleLogout}/>
        <Router>
          {!this.state.loggedIn ? <Redirect to="/login"/> : null}
          <Switch>
            <Route path="/login" exact render={() =>
              <Login
                loggedIn={this.state.loggedIn}
              />}
            />
            <Route path="/" exact
                   render={() =>
                     (
                       this.state.roleUser === "salesperson" &&
                       <Home
                         loggedIn={this.state.loggedIn}
                         onLogout={this.handleLogout}
                         namaUser={this.state.namaUser}
                       />
                     )
                     ||
                     (
                       this.state.roleUser === "lead_sales" &&
                       <HomeLeadSales
                         loggedIn={this.state.loggedIn}
                         onLogout={this.handleLogout}
                         namaUser={this.state.namaUser}
                       />)
                     ||
                     (
                       this.state.roleUser === "regional_lead" &&
                       <HomeRegional
                         loggedIn={this.state.loggedIn}
                         onLogout={this.handleLogout}
                         namaUser={this.state.namaUser}
                       />
                     )
                     ||
                     (
                         this.state.roleUser === "validator" &&
                         <HomeValidator
                             loggedIn={this.state.loggedIn}
                             onLogout={this.handleLogout}
                             namaUser={this.state.namaUser}
                         />
                     )
                     ||
                     (
                       !this.state.loggedIn &&
                       <Login
                         loggedIn={this.state.loggedIn}
                       />
                     )
                   }
            />
            <Route path="/warung-tidak-registrasi/view-all" exact
                   render={() =>
                     <WarungNoregViewAll
                       loggedIn={this.state.loggedIn}
                     />}
            />
            <Route path="/warung-tidak-registrasi/create" exact
                   render={() =>
                     <WarungNoregCreate
                       loggedIn={this.state.loggedIn}
                     />}
            />
            <Route path="/table" exact component={WarungRegistrasi}/>
            <Route path="/salesperson-control/" exact
                   render={() =>
                     <SalespersonControl
                       loggedIn={this.state.loggedIn}
                     />}
            />
            <Route path="/salesperson-add/" exact
                   render={() =>
                     <SalespersonAddition
                       loggedIn={this.state.loggedIn}
                     />}
            />
            <Route path="/target" exact
                   render={() =>
                     <TargetPenugasan
                       loggedIn={this.state.loggedIn}
                     />
                   }
            />
            <Route path="/target-penugasan/create" exact
                   render={() =>
                     <TargetPenugasanCreate
                       loggedIn={this.state.loggedIn}
                     />
                   }
            />
            <Route path="/target-penugasan/view-all" exact
                   render={() =>
                     <TargetPenugasanViewAll
                       loggedIn={this.state.loggedIn}
                     />
                   }
            />
          <Route path="/table" render={() => <WarungRegistrasi/> } />
          <Route path="/validasi/view-all" exact
                 render={() =>
                     <VerifikasiViewAll
                         loggedIn={this.state.loggedIn}
                     />
                 }
          />
          <Route path="/validasi/create/:id" exact
                 render={({match}) =>
                     <VerifikasiCreate
                         loggedIn={this.state.loggedIn}
                         id={match.params.id}
                     />}
          />
            <Route path="/dashboard/regional/city/:id/:year" render={({match}) => (
              <CityRegional
                loggedIn={this.state.loggedIn}
                id={match.params.id}
                year={match.params.year}
                onLogout={this.handleLogout}
                namaUser={this.state.namaUser}
              />
            )}/>
            <Route path="/warung/create" exact component={WarungCreate}/>
            <Route path="/warung/update/:id" exact component={WarungUpdate}/>
            <Route component={NotFound}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;