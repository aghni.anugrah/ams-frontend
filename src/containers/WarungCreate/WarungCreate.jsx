import React, {Component} from "react";
import {Redirect} from 'react-router-dom'
import WarungCreatePageOne from "../../components/WarungCreate/WarungCreatePageOne";
import WarungCreatePageTwo from "../../components/WarungCreate/WarungCreatePageTwo";
import Card from "react-bootstrap/Card";
import CustomModal from "../../components/Modal/CustomModal";

class WarungCreate extends Component {
  state = {
    sales_code: "",
    mitra_name: "",
    warung_name: "",
    ktp_id: "",
    phone_number: "",
    whatsapp_phone_number: "",
    business_type: "gerobak",
    village_name: "",
    road_access_enough: true,
    smartphone_access: true,
    address: "",
    city_name: "",
    rt_rw: "",
    point_land: "",
    birth: "",
    birth_date: "",
    page: 1,
    success: false,
    showConfirmationSubmitModal: false,
    showBadFormModal: false,
    redirect: false,
    wa_different: false,
    business_type_different: false,
    wilayah: [],
    error: ""
  };

  controller = new AbortController();

  fetchRefCode = () => {
    const URL = "http://localhost:8080";
    const token = "Bearer " + localStorage.getItem("token");
    fetch(URL + "/user/ref-code", {
      method: "GET",
      signal: this.controller.signal,
      headers: {
        'Authorization': token
      }
    }).then(res => {
      if (res.status !== 200) {
        throw new Error()
      }
      res.json().then(result => {
        this.setState({sales_code: result.referral_code});
      });
    }).catch(() => {
      this.setState({error: "refcode"})
    })
  };

  fetchWilayah = () => {
    const URL = "http://localhost:8080";
    const token = "Bearer " + localStorage.getItem("token");
    fetch(URL + "/wilayah", {
      method: "GET",
      signal: this.controller.signal,
      headers: {
        'Authorization': token
      }
    }).then(res => {
      if (res.status !== 200) {
        throw new Error()
      }
      res.json().then(result => {
          this.setState({
            wilayah: result.wilayah,
            city_name: result.wilayah[0]
          });
        }
      )
    }).catch(() => {
      this.setState({error: "wilayah"})
    });
  };

  handleChange = (e) => {
    if (e.target.type === "radio" || e.target.name === "wa_different") {
      this.setState({[e.target.name]: e.target.value === 'true'});
    } else {
      this.setState({[e.target.name]: e.target.value});
    }
    if (e.target.name === "business_type") {
      if (e.target.value === "gerobak" || e.target.value === "nasi" || e.target.value === "kelontong" || e.target.value === "kopi" || e.target.value === "tidak") {
        this.setState({business_type_different: false})
      } else if (e.target.value === "lainnya") {
        this.setState({business_type_different: true, business_type: ""})
      }
    }
    if (e.target.name === "birth") {
      this.setState({birth_date: e.target.value + "T00:00:00+07:00"});
    }
  };

  pageOneCompleted() {
    return this.state.mitra_name !== "" && this.state.ktp_id !== "" && this.state.birth !== "" &&
      this.state.business_type !== "" && this.state.village_name !== "" && this.state.city_name !== "" &&
      this.state.road_access_enough === true && this.state.smartphone_access === true;
  };

  pageTwoCompleted() {
    return this.state.warung_name !== "" && this.state.address !== "" && this.state.rt_rw !== "" && this.state.point_land !== ""
  };

  handlePage = (page) => {
    if (!this.pageOneCompleted()) {
      this.handleBadForm(true)
    } else {
      this.setState({page})
    }
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const token = "Bearer " + localStorage.getItem("token");
    fetch("http://localhost:8080/warung", {
      method: "POST",
      signal: this.controller.signal,
      headers: {
        "Authorization": token
      },
      body: JSON.stringify(this.state),
    }).then(res => {
      if (res.status === 200) {
        this.setState({success: true})
      }
    })
  };

  handleConfirmSubmit = () => {
    if (!this.pageTwoCompleted() || !this.pageOneCompleted()) {
      this.handleBadForm(true)
    } else {
      this.setState({showConfirmationSubmitModal: true})
    }
  };

  closeConfirmationFormModal = () => {
    this.setState({showConfirmationSubmitModal: false})
  };

  handleBadForm = (showBadFormModal) => {
    this.setState({showBadFormModal})
  };

  componentDidMount() {
    this.fetchWilayah();
    this.fetchRefCode();
    if (this.state.success) {
      window.onbeforeunload = undefined
    } else {
      window.onbeforeunload = () => true
    }
  }

  componentWillUnmount() {
    window.onbeforeunload = null;
    this.controller.abort()
  }

  render() {
    if (this.state.redirect) {
      return (
        <Redirect to="/table"/>
      )
    }
    if (this.state.error) {
      let err;
      if (this.state.error === "wilayah") {
        err = <h1>Error Fetching Area</h1>
      } else if (this.state.error === "refcode") {
        err = <h1>Error Fetching Referral Code</h1>
      }
      return <div>{err}<h2>Please Contact Administrator</h2></div>
    }

    return (
      <div className="container">
        <Card className="shadow-lg mb-5 mt-3 bg-white rounded">
          <CustomModal
            show={this.state.showConfirmationSubmitModal}
            title="Simpan Data?"
            onHide={this.closeConfirmationFormModal}
            primaryButton="Ya, simpan"
            primaryAction={this.handleSubmit}
            secondaryButton="Tidak, kembali"
            secondaryAction={this.closeConfirmationFormModal}
            type="confirm"
          />
          <CustomModal
            show={this.state.showBadFormModal}
            title="Penyimpanan Data Gagal"
            onHide={() => this.handleBadForm(false)}
            primaryButton="Perbaiki"
            primaryAction={() => this.handleBadForm(false)}
            type="fail"
          >
            <p className="text-center">Lengkapi data</p>
          </CustomModal>
          <CustomModal
            show={this.state.success}
            title="Data Telah Dikirim Untuk Diverifikasi"
            onHide={() => this.setState({redirect: true})}
            primaryButton="OK"
            primaryAction={() => this.setState({redirect: true})}
            type="success"
          />
          <div className="text-center m-1">
            <h1>Registrasi Calon Mitra</h1>
          </div>
          <div className="row">
            <div className="col-11 mx-auto mt-1">
              {this.state.page === 1 ?
                <WarungCreatePageOne handlePage={this.handlePage} handleChange={this.handleChange}
                                     state={this.state}/> :
                <WarungCreatePageTwo handleSubmit={this.handleSubmit} handlePage={this.handlePage}
                                     handleChange={this.handleChange} handleConfirm={this.handleConfirmSubmit}
                                     state={this.state}/>}
            </div>
          </div>
        </Card>
      </div>
    );
  }
}

export default WarungCreate;