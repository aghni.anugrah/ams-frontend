import React, {Component, useEffect, useState} from "react";
import {Redirect} from 'react-router-dom'
import Card from "react-bootstrap/Card";
import classes from './HomeRegional.module.css'
import Button from "react-bootstrap/Button";
import WilayahRegionalCard from "../../components/Cards/WilayahRegionalCard";
import RegionalSalesReport from "../../components/RegionalSalesReport/RegionalSalesReport";
import RegionalSalesReportMobile from "../../components/RegionalSalesReport/RegionalSalesReportMobile";
import RegionalMonthStatistics from "../../components/RegionalSalesReport/RegionalMonthStatistics";
import StatistikKotaCard from "../../components/Cards/StatistikKotaCard";
import FilterRegional from "../../components/Filter/FilterRegional";

const RenderChartResponsive = ({data}) => {
    const [isDesktop, setDesktop] = useState(window.innerWidth > 575);
    const updateMedia = () => {
        setDesktop(window.innerWidth > 575);
    };

    useEffect(() => {
        window.addEventListener("resize", updateMedia);
        return () => window.removeEventListener("resize", updateMedia);
    });

    return (
            isDesktop ? (
                <React.Fragment>
                    <Card className="shadow-lg mb-5 bg-white rounded" id={classes.chartContainer}>
                        <h3 className="font-weight-bold" id={classes.chartTitle}>Jumlah Akuisisi</h3>
                        <RegionalSalesReport data={data}/>
                    </Card>
                </React.Fragment>
            ) : (
                <React.Fragment>
                    <Card className="shadow-lg mb-5 bg-white rounded" id={classes.chartContainer}>
                        <h3 className="font-weight-bold" id={classes.chartTitle}>Jumlah Akuisisi</h3>
                        <RegionalSalesReportMobile data={data}/>
                    </Card>
                </React.Fragment>
            )
    );
}

class HomeRegional extends Component {
    state = {
        listBulan: [],
        listCity: [],
        years: [],
        isLoadingGraph: true,
        isLoadingCity: true,
        isLoadingYear: true,
        isFilter: false,
        region: "",
        tahun: new Date().getFullYear(),
        tahunCard: new Date().getFullYear(),
        buttonDisable: true,
    };

    componentDidMount() {
        this.fetchData();
        this.fetchCityData();
        this.fetchYear()
    }

    componentWillUnmount() {
        this.controller.abort()
    }

    fetchData = () => {
        const URL = "http://localhost:8080";
        const token = "Bearer " + localStorage.getItem("token");
        let year = "?year=" + this.state.tahun;
        fetch(URL + "/dashboard/regional" + year, {
            method: "GET",
            signal: this.controller.signal,
            headers: {
                "Authorization": token,
            }
        }).then(response => {
            return response.json();
        }).then((data) => {
            const fetchedData = [];
            let wilayah;
            for (let key in data.AcquisitionPerMonth) {
                fetchedData.push({
                    ...data.AcquisitionPerMonth[key]
                });
            }
            wilayah = data.Wilayah;
            this.setState({
                listBulan: fetchedData,
                region: wilayah,
                isLoadingGraph: false,
            });
        }).catch(err => {
            throw new Error(err)
        });
    }

    controller = new AbortController();

    fetchYear = () => {
        const URL = "http://localhost:8080/dashboard/regional/existing-year";
        const token = "Bearer " + localStorage.getItem("token");
        fetch(URL, {
            method: "GET",
            signal: this.controller.signal,
            headers: {
                "Authorization": token,
            }
        }).then(response => {
            return response.json();
        }).then((data) => {
            const fetchedData = [];
            for (let key in data.Years) {
                fetchedData.push({
                    ...data.Years[key]
                });
            }
            this.setState({
                years: fetchedData,
                isLoadingYear: false
            });
        }).catch(err => {
            throw new Error(err)
        });
    }

    fetchCityData = () => {
        const URL = "http://localhost:8080";
        const token = "Bearer " + localStorage.getItem("token");
        let year = "?year=" + this.state.tahun;
        fetch(URL + "/dashboard/regional/all-city" + year, {
            method: "GET",
            signal: this.controller.signal,
            headers: {
                "Authorization": token,
            }
        }).then(response => {
            return response.json();
        }).then((data) => {
            const fetchedData = [];
            for (let key in data.AcquisitionPerCity) {
                fetchedData.push({
                    ...data.AcquisitionPerCity[key]
                });
            }
            this.setState({
                listCity: fetchedData,
                isLoadingCity: false
            });
        }).catch(err => {
            throw new Error(err)
        });
    }

    showFilter = () => {
        if (this.state.isFilter) {
            this.setState({
                isFilter: false
            })
        } else {
            this.setState({
                isFilter: true
            })
        }
    };

    handleTahunCard = () => {
        this.setState({
            tahunCard: this.state.tahun
        })
    }

    handleTahun = (e) => {
        this.setState({
            tahun: e.target.value,
            buttonDisable: false
        })
    };

    handleApply = () => {
        this.fetchData();
        this.fetchCityData();
        this.handleTahunCard();
    };

    renderCityOverview(month, year) {
        return this.state.listCity.map((city, index) => {
            return (
                <div className="col-sm-4" id={classes.betweenCityMargin} key={index}>
                    <StatistikKotaCard data={city} month={month} year={year}/>
                </div>
            );
        })
    }

    render() {
        if (!this.props.loggedIn) {
            return <Redirect to="/login"/>
        } else {
            if (this.state.isLoadingGraph || this.state.isLoadingCity || this.state.isLoadingYear) {
                return (
                    <br/>
                )
            } else {
                if (this.state.listBulan.length === 0) {
                    return (
                        <React.Fragment>
                            <h2 className="font-weight-bold" id={classes.welcome}>Halo, {this.props.namaUser.split(" ")[0]}</h2>
                            <Card className="shadow-lg mb-5 bg-white rounded">
                                <div className="row" id={classes.margin}>
                                    <div className="col-sm-10" id={classes.wilayahCard}>
                                        <WilayahRegionalCard data={this.state.region} tahun={this.state.tahunCard}/>
                                    </div>
                                    <div className="col-sm-2" id={classes.button}>
                                        <Button className="btn-block" onClick={this.showFilter} id={classes.options} variant="light">Options</Button>
                                    </div>
                                </div>
                                {this.state.isFilter &&
                                <div className="col-sm-auto" id={classes.filter}>
                                    <FilterRegional handleTahun={this.handleTahun} buttonState={this.state.buttonDisable} handleApply={this.handleApply} tahun={this.state.tahun} listTahun={this.state.years}/>
                                </div>
                                }
                                <h2 className="font-weight-bold">Tidak ada data untuk tahun {this.state.tahunCard}</h2>
                            </Card>
                        </React.Fragment>
                    )
                } else {
                    return (
                        <React.Fragment>
                            <h2 className="font-weight-bold" id={classes.welcome}>Halo, {this.props.namaUser.split(" ")[0]}</h2>
                            <Card className="shadow-lg mb-5 bg-white rounded">
                                <div className="row" id={classes.margin}>
                                    <div className="col-sm-10" id={classes.wilayahCard}>
                                        <WilayahRegionalCard data={this.state.region} tahun={this.state.tahunCard}/>
                                    </div>
                                    <div className="col-sm-2" id={classes.button}>
                                        <Button className="btn-block" onClick={this.showFilter} id={classes.options} variant="light">Options</Button>
                                    </div>
                                </div>
                                {this.state.isFilter &&
                                <div className="col-sm-auto" id={classes.filter}>
                                    <FilterRegional data={this.props} handleTahun={this.handleTahun} buttonState={this.state.buttonDisable} handleApply={this.handleApply} tahun={this.state.tahun} listTahun={this.state.years}/>
                                </div>
                                }
                                <div className="row" id={classes.cardMargin}>
                                    <RenderChartResponsive data={this.state.listBulan}/>
                                </div>
                                <div className="col-sm-auto" id={classes.accordion}>
                                    <RegionalMonthStatistics data={this.state.listBulan}/>
                                </div>
                                <div className="row" id={classes.cityMargin}>
                                    {this.renderCityOverview(this.state.listBulan[this.state.listBulan.length - 1].Bulan, this.state.tahunCard)}
                                </div>
                            </Card>
                        </React.Fragment>
                    );
                }
            }
        }
    }
}

export default HomeRegional;