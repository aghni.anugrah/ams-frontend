import React, {Component, useEffect, useState} from "react";
import {Redirect} from 'react-router-dom'
import Card from "react-bootstrap/Card";
import classes from './HomeRegional.module.css'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft} from "@fortawesome/free-solid-svg-icons";
import Button from "react-bootstrap/Button";
import WilayahRegionalCard from "../../components/Cards/WilayahRegionalCard";
import RegionalSalesReport from "../../components/RegionalSalesReport/RegionalSalesReport";
import RegionalSalesReportMobile from "../../components/RegionalSalesReport/RegionalSalesReportMobile";
import RegionalMonthStatistics from "../../components/RegionalSalesReport/RegionalMonthStatistics";
import FilterRegional from "../../components/Filter/FilterRegional";

const MyFunction = ({data}) => {
    const [isDesktop, setDesktop] = useState(window.innerWidth > 575);
    const updateMedia = () => {
        setDesktop(window.innerWidth > 575);
    };

    useEffect(() => {
        window.addEventListener("resize", updateMedia);
        return () => window.removeEventListener("resize", updateMedia);
    });

    return (
        isDesktop ? (
            <React.Fragment>
                <Card className="shadow-lg mb-5 bg-white rounded" id={classes.chartContainer}>
                    <h3 className="font-weight-bold" id={classes.chartTitle}>Jumlah Akuisisi</h3>
                    <RegionalSalesReport data={data}/>
                </Card>
            </React.Fragment>
        ) : (
            <React.Fragment>
                <Card className="shadow-lg mb-5 bg-white rounded" id={classes.chartContainer}>
                    <h3 className="font-weight-bold" id={classes.chartTitle}>Jumlah Akuisisi</h3>
                    <RegionalSalesReportMobile data={data}/>
                </Card>
            </React.Fragment>
        )
    );
}

class CityRegional extends Component {
    state = {
        listBulan: [],
        years: [],
        isLoadingGraph: true,
        region: "",
        tahun: this.props.year,
        tahunCard: this.props.year,
        buttonDisable: true,
    };

    componentDidMount() {
        this.fetchData();
    }

    componentWillUnmount() {
        this.controller.abort()
    }

    fetchData = () => {
        const URL = "http://localhost:8080";
        const URLPlus = "?year=" + this.state.tahun;
        const param = this.props.id
        const token = "Bearer " + localStorage.getItem("token");
        fetch(URL + "/dashboard/regional/city/" + param + URLPlus, {
            method: "GET",
            headers: {
                "Authorization": token,
            }
        }).then(response => {
            return response.json();
        }).then((data) => {
            const fetchedData = [];
            const fetchedDataYears = [];
            let wilayah;
            for (let key in data.AcquisitionPerMonth) {
                fetchedData.push({
                    ...data.AcquisitionPerMonth[key]
                });
            }
            for (let key in data.AvailableYears) {
                fetchedDataYears.push({
                    ...data.AvailableYears[key]
                });
            }
            wilayah = data.Wilayah;
            this.setState({
                listBulan: fetchedData,
                years: fetchedDataYears,
                region: wilayah,
                isLoadingGraph: false,
            });
        }).catch(err => {
            throw new Error(err)
        });
    }

    controller = new AbortController();

    showFilter = () => {
        if (this.state.isFilter) {
            this.setState({
                isFilter: false
            })
        } else {
            this.setState({
                isFilter: true
            })
        }
    };

    handleTahunCard = () => {
        this.setState({
            tahunCard: this.state.tahun
        })
    }

    handleTahun = (e) => {
        this.setState({
            tahun: e.target.value,
            buttonDisable: false
        })
    };

    handleApply = () => {
        this.fetchData();
        this.handleTahunCard();
    };

    render() {
        if (!this.props.loggedIn) {
            return <Redirect to="/login"/>
        } else {
            if (this.state.isLoadingGraph) {
                return (
                    <br/>
                )
            } else {
                if (this.state.listBulan.length === 0) {
                    return (
                        <React.Fragment>
                            <h2 className="font-weight-bold" id={classes.welcomeCity}>Halo, {this.props.namaUser.split(" ")[0]}</h2>
                            <div id={classes.backDashboard}>
                                <FontAwesomeIcon icon={faChevronLeft} style={{color: "#007afe"}}/>
                                <a href="/" className="font-weight-bold" id={classes.backDashboard}>Kembali ke Dashboard</a>
                            </div>
                            <Card className="shadow-lg mb-5 bg-white rounded">
                                <div className="row" id={classes.margin}>
                                    <div className="col-sm-10" id={classes.wilayahCard}>
                                        <WilayahRegionalCard data={this.state.region} tahun={this.state.tahunCard}/>
                                    </div>
                                    <div className="col-sm-2" id={classes.button}>
                                        <Button className="btn-block" onClick={this.showFilter} id={classes.options} variant="light">Options</Button>
                                    </div>
                                    {this.state.isFilter &&
                                    <div className="col-sm-auto" id={classes.filter}>
                                        <FilterRegional handleTahun={this.handleTahun} buttonState={this.state.buttonDisable} handleApply={this.handleApply} tahun={this.state.tahun} listTahun={this.state.years}/>
                                    </div>
                                    }
                                </div>
                                <h2 className="font-weight-bold">Tidak ada data untuk tahun {this.state.tahunCard}</h2>
                            </Card>
                        </React.Fragment>
                    )
                } else {
                    return (
                        <React.Fragment>
                            <h2 className="font-weight-bold" id={classes.welcomeCity}>Halo, {this.props.namaUser.split(" ")[0]}</h2>
                            <div id={classes.backDashboard}>
                                <FontAwesomeIcon icon={faChevronLeft} style={{color: "#007afe"}}/>
                                <a href="/" className="font-weight-bold" id={classes.backDashboard}>Kembali ke Dashboard</a>
                            </div>
                            <Card className="shadow-lg mb-5 bg-white rounded">
                                <div className="row" id={classes.margin}>
                                    <div className="col-sm-10" id={classes.wilayahCard}>
                                        <WilayahRegionalCard data={this.state.region} tahun={this.state.tahunCard}/>
                                    </div>
                                    <div className="col-sm-2" id={classes.button}>
                                        <Button className="btn-block" onClick={this.showFilter} id={classes.options} variant="light">Options</Button>
                                    </div>
                                </div>
                                {this.state.isFilter &&
                                <div className="col-sm-auto" id={classes.filter}>
                                    <FilterRegional data={this.props} handleTahun={this.handleTahun} buttonState={this.state.buttonDisable} handleApply={this.handleApply} tahun={this.state.tahun} listTahun={this.state.years}/>
                                </div>
                                }
                                <div className="row" id={classes.cardMargin}>
                                    <MyFunction data={this.state.listBulan}/>
                                </div>
                                <div className="col-sm-auto" id={classes.accordion}>
                                    <RegionalMonthStatistics data={this.state.listBulan}/>
                                </div>
                            </Card>
                        </React.Fragment>
                    );
                }
            }
        }
    }
}

export default CityRegional;