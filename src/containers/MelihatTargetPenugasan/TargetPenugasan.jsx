import React, {Component} from "react";
import {Redirect} from 'react-router-dom'
import Card from 'react-bootstrap/Card'
import TargetPenugasanTable from "../../components/Table/TargetPenugasanTable";
import CustomModal from "../../components/Modal/CustomModal";
import TargetPenugasanCard from "../../components/Cards/TargetPenugasanCard";
import KuotaVisitCard from "../../components/Cards/KuotaVisitCard";
import KuotaAktivasiCard from "../../components/Cards/KuotaAktivasiCard";
import KuotaNOOCard from "../../components/Cards/KuotaNOOCard";
import CustomPagination from "../../components/Pagination/CustomPagination";

const ITEM_PER_PAGE = 5;

class TargetPenugasan extends Component {
    state = {
        targetPenugasanInactive: [],
        targetPenugasanActive: {},
        showDetailModal: false,
        id_wilayah_penugasan: "",
        tanggal_mulai: "",
        tanggal_selesai: "",
        kuota_visit: "",
        kuota_activation: "",
        kuota_noo: "",
        real_visit: "",
        real_activation: "",
        real_noo: "",
        isLoading: true,
        currentTugas: [],
        currentPage: 1
    }

    controller = new AbortController();

    getTargetPenugasanInactive=() => {
        const token = "Bearer " + localStorage.getItem("token");
        fetch("http://localhost:8080/target-penugasan/inactive", {
            signal: this.controller.signal,
            method: "GET",
            headers: {
                "Authorization": token,
            }
        }).then(response => {
            return response.json();
        }).then((data) => {
            const fetchedData = [];
            for (let key in data.wilayah_target_penugasan){
                fetchedData.push({
                    ...data.wilayah_target_penugasan[key]
                });
            }
            this.setState({
                targetPenugasanInactive: fetchedData,
                currentTugas: fetchedData.slice(0, ITEM_PER_PAGE),
                isLoading: false
            });
        })
    }

    getTargetPenugasanActive=() => {
        const token = "Bearer " + localStorage.getItem("token");
        fetch("http://localhost:8080/target-penugasan/active", {
            signal: this.controller.signal,
            method: "GET",
            headers: {
                "Authorization" : token,
            }
        }).then(response => {
            return response.json();
        }).then((data) => {
            this.setState ({
                targetPenugasanActive: data
            });
        });

    }

    componentDidMount() {
        this.getTargetPenugasanInactive();
        this.getTargetPenugasanActive();
    }

    componentWillUnmount() {
        this.controller.abort()
    }

    closeDetailModal = () => {
        this.setState({
            showDetailModal: false,
            kuota_visit: "",
            kuota_activation: "",
            kuota_noo: "",
            real_visit: "",
            real_activation: "",
            real_noo: ""
        });
    }

    showDetailHandler = (penugasan) => {
        this.setState({
            showDetailModal: true,
            kuota_visit: penugasan.kuota_visit,
            kuota_activation: penugasan.kuota_activation,
            kuota_noo: penugasan.kuota_noo,
            real_visit: penugasan.real_visit,
            real_activation: penugasan.real_activation,
            real_noo: penugasan.real_noo
        })
    }

    onPageChanged = data => {
        const { currentPage, pageLimit } = data;

        const offset = (currentPage - 1) * pageLimit;
        const currentTugas = this.state.targetPenugasanInactive.slice(offset, offset + pageLimit);

        this.setState({ currentTugas, currentPage });
    };



    renderDetail(){
        return(
                <div>
                    <p >Kuota Visit : {this.state.real_visit} / {this.state.kuota_visit}</p>
                    <p >Kuota Aktivasi : {this.state.real_activation} / {this.state.kuota_activation}</p>
                    <p >Kuota NOO : {this.state.real_noo} / {this.state.kuota_noo}</p>
                </div>
            )
    }

    renderPenugasanActive() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-4'>
                        <KuotaVisitCard data={this.state.targetPenugasanActive} />
                    </div>
                    <div className='col-md-4'>
                        <KuotaAktivasiCard data={this.state.targetPenugasanActive} />
                    </div>
                    <div className='col-md-4'>
                        <KuotaNOOCard data={this.state.targetPenugasanActive} />
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-2'></div>
                    <div className='col-md-8'>
                        <TargetPenugasanCard data={this.state.targetPenugasanActive} />
                    </div>
                    <div className='col-md-2'></div>
                </div>
            </div>
        )
    }


    render() {
        if (!this.props.loggedIn) {
            return <Redirect to="/login"/>
        }

        if (this.state.isLoading){
            return (
                <div style={{
                    position: 'absolute', left: '50%', top: '50%',
                    transform: 'translate(-50%, -50%)'
                }}>
                    <h1 className="font-weight-bolder">Loading ...</h1>
                </div>
            )
        }

        return (
            <React.Fragment>
                <br/>
                <h1 className="text-center mb-2 font-weight-bold" style={{fontSize:'36px'}}>
                    Daftar Penugasan
                </h1>
                <br/>
                <Card className="shadow-lg mb-5 bg-white rounded">
                    <div className='card-header' id='header'></div>
                    <br/>
                    <h1 className="text-md-center mb-2 font-weight-bold" style={{fontSize:'24px'}}>
                        Kuota yang Berhasil Anda Capai di Minggu Ini :
                    </h1>
                    <div className='text-align-center'>
                        {this.state.targetPenugasanActive.status === "success" ?
                            this.renderPenugasanActive()
                            :
                            <p>Belum ada penugasan</p>
                        }
                    </div>
                    <CustomModal
                        show={this.state.showDetailModal}
                        onHide={this.closeDetailModal}
                        title="Detail Penugasan"
                        primaryButton="OK"
                        primaryAction={this.closeDetailModal}
                    >
                        {this.renderDetail()}
                    </CustomModal>
                    <br/>
                    <h1 className="text-md-center mb-2 font-weight-bold" style={{fontSize:'20px'}}>
                        Riwayat Penugasan
                    </h1>
                    <div className="container pt-3">
                        {this.state.targetPenugasanInactive.length > 0 ?
                            <TargetPenugasanTable data={this.state.currentTugas} detail={this.showDetailHandler}/>
                            :
                            <p className="text-center m-3">Belum ada riwayat penugasan</p>
                        }
                    </div>

                    <div className="d-flex flex-row py-4 fa-pull-right">
                        <CustomPagination
                            currentPage={this.state.currentPage}
                            totalRecords={this.state.targetPenugasanInactive.length}
                            pageLimit={ITEM_PER_PAGE}
                            onPageChanged={this.onPageChanged}
                        />
                    </div>


                </Card>
            </React.Fragment>
        );
    }
}

export default TargetPenugasan;