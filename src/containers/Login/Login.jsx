import React, {Component} from "react";
import LoginButton from "../../components/LoginButton/LoginButton";
import logo from "./illustration.png";
import "./Login.css";

class Login extends Component {

  render() {
    return (
      <React.Fragment>
          <div className="container-fluid">
              <div className="row">
                  <h2 className="mx-auto font-weight-bold justify-content-sm-center">ACQUISITION MANAGEMENT SYSTEM</h2>
              </div>
              <div className="row" style={{paddingBottom: "40px"}}>
                  <div className="col">
                      <img src={logo} style={{height:"auto", width:"auto"}} alt="logo" className="mx-auto mt-2 center"/>
                  </div>
              </div>
          </div>
        <LoginButton/>
      </React.Fragment>
    );
  }
}

export default Login;