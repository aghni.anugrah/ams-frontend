import React, {Component} from "react";
import {Redirect} from "react-router-dom";
import Form from "react-bootstrap/Form";
import DateTimePicker from 'react-datetime-picker'
import CustomModal from "../../components/Modal/CustomModal";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Button from "react-bootstrap/Button";
import Tooltip from "react-bootstrap/Tooltip";
import InputGroup from "react-bootstrap/InputGroup";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";
import {faInfoCircle, faCircle} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

class WarungNoregCreate extends Component {
    state = {
        nama_warung: "",
        datepicker: new Date(),
        tanggal_kunjungan: new Date(),
        lokasi_warung: "",
        alasan_penolakkan: "",
        successCreate: false,
        showConfirmationSubmitModal: false,
        showBadFormModal: false,
        customAlasan: ""
    };

    controller = new AbortController();

    componentDidMount() {
        this.state.successCreate ?
            window.onbeforeunload = undefined
            :
            window.onbeforeunload = () => true
    }

    componentWillUnmount() {
        window.onbeforeunload = null;
        this.controller.abort()
    }

    handleChange = e => {
        this.setState({[e.target.name]: e.target.value});
    };

    handleDateChange = tanggal => {
        tanggal = new Date(tanggal.setHours(tanggal.getHours() + 7));
        this.setState({
            datepicker: new Date(tanggal.setHours(tanggal.getHours() - 7)),
            tanggal_kunjungan: new Date(tanggal.setHours(tanggal.getHours() + 7)).toISOString().split('.')[0]+"+07:00"
        });
    };

    formIsComplete(){
        return !(this.state.nama_warung === "" || this.state.lokasi_warung === "" || this.state.alasan_penolakkan === "")
    }

    showConfirmationFormModal = (e) => {
        e.preventDefault();
        this.setState({
            showConfirmationSubmitModal: true
        });
    };

    closeConfirmationFormModal = () => {
        this.setState({
            showConfirmationSubmitModal: false
        });
    };

    showBadFormatFormModal = () => {
        this.setState({
            showBadFormModal: true
        });
    };

    closeBadFormatFormModal = () => {
        this.setState({
            showBadFormModal: false
        });
    };

    handleSubmit = (e) => {
        console.log(this.state)
        e.preventDefault();
        const token = "Bearer " + localStorage.getItem("token");
        fetch("http://localhost:8080/warung-tidak-daftar/create", {
            method: "POST",
            signal: this.controller.signal,
            headers: {
                "Authorization" : token
            },
            body: JSON.stringify(this.state),
        }).then(res => {
            if (res.status === 200) {
                this.setState({successCreate: true});
            } else if (res.status === 400 || res.status === 500) {
                this.closeConfirmationFormModal();
                this.showBadFormatFormModal();
            }
        })
    };

    handleAlasan = (e) => {
        if(e.target.value !== "0") {
            this.setState({
                alasan_penolakkan: e.target.value,
                customAlasan:""
            })
        } else {
            this.setState({
                customAlasan: e.target.value,
                alasan_penolakkan: ""
            });
        }
    };

    renderSubmitButton(){
        return (
            this.formIsComplete() ?
                <Button onClick={this.showConfirmationFormModal} style={{ backgroundColor: '#FECF28', borderColor:'#FECF28', color:'#000000' }} type="submit" className="mt-4 mb-2 mh-auto btn btn-primary text-center">
                    Simpan Data
                </Button>
                :
                <OverlayTrigger
                    placement="right"
                    delay={{ show: 250, hide: 400 }}
                    overlay={<Tooltip id="tooltip-disabled">Data belum lengkap!</Tooltip>}
                >
                    <span className="text-center">
                        <button disabled style={{ pointerEvents: 'none', backgroundColor: '#FECF28', borderColor:'#FECF28', color:'#000000' }} className="mt-4 mb-2 mh-auto btn btn-primary text-center">
                            Simpan Data
                        </button>
                    </span>
                </OverlayTrigger>
        )
    }

    renderFormSummary() {
        return (
            <div>
                <p style={{wordWrap:'break-word'}}>Nama Warung: {this.state.nama_warung}</p>
                <p style={{wordWrap:'break-word'}}>Alamat Warung: {this.state.lokasi_warung}</p>
                <p>Tanggal Kunjungan: {
                    this.state.datepicker
                        .toLocaleString('id-ID',
                            {
                                weekday: 'long',
                                year: 'numeric',
                                month: 'long',
                                day: 'numeric',
                                hour:'2-digit',
                                minute: '2-digit'})
                }</p>
                <p style={{wordWrap:'break-word'}}>Alasan Penolakan: {this.state.alasan_penolakkan}</p>
                <p className="text-center">Yakin ingin menyimpan data?</p>
            </div>
        )
    }

    renderBadFormModal() {
        return (
            <div>
                <p className="text-center">Ada data yang tidak sesuai format!</p>
                <p className="text-center">Pastikan sudah memenuhi:</p>
                <ul>
                    <li>Nama warung mengandung kata "<strong>Warung</strong>", "<strong>Gerai</strong>", "<strong>Kedai</strong>", "<strong>Lapak</strong>" atau "<strong>Toko</strong>"</li>
                    <li>Alamat warung memiliki nama <strong>kecamatan</strong> dan nama <strong>kota</strong></li>
                    <li>Pengisian data <strong>maksimal 12 jam</strong> setelah waktu kunjungan yang sebenarnya</li>
                </ul>
            </div>
        )
    }

    renderTooltipForm(message){
        return (
            <OverlayTrigger
                placement="right"
                delay={{ show: 250, hide: 400 }}
                overlay={
                    <Tooltip id="tooltip-disabled">
                        {message}
                    </Tooltip>
                }
            >
                <span style={{paddingLeft:5}}>
                    <FontAwesomeIcon
                        icon={faInfoCircle}
                        mask={faCircle}
                        size="lg"
                        transform="shrink-3"
                        style={{ color: '#FF9802' }}
                    />
                </span>
            </OverlayTrigger>
        )
    }

    render() {
        if (!this.props.loggedIn) {
            return <Redirect to="/login"/>
        }
        if (this.state.successCreate) {
            return <Redirect to={{
                pathname: "/warung-tidak-registrasi/view-all",
                state: {
                    successCreate: true,
                    name: this.state.nama_warung
                }
            }}/>
        }
        return (
            <React.Fragment>
                <Container fluid style={{marginTop:'-3rem',paddingBottom:'8em'}}>
                    <h2 className="text-center font-weight-bold">
                        Tambah Calon Mitra Tidak Registrasi
                    </h2>
                    <Card className="shadow-lg mb-5 bg-white rounded">
                        <Card.Body>
                            <CustomModal
                                show={this.state.showConfirmationSubmitModal}
                                title="Ringkasan Data"
                                onHide={this.closeConfirmationFormModal}
                                primaryButton="Ya, simpan"
                                primaryAction={this.handleSubmit}
                                secondaryButton="Tidak, kembali"
                                secondaryAction={this.closeConfirmationFormModal}
                                type="confirm"
                            >
                                {this.renderFormSummary()}
                            </CustomModal>
                            <CustomModal
                                show={this.state.showBadFormModal}
                                title="Penyimpanan Data Gagal"
                                onHide={this.closeBadFormatFormModal}
                                primaryButton="Perbaiki"
                                primaryAction={this.closeBadFormatFormModal}
                                type="fail"
                            >
                                {this.renderBadFormModal()}
                            </CustomModal>
                            <Form onSubmit={this.handleSubmit}>
                                <Form.Group controlId="FGNamaWarung">
                                    <Form.Label>
                                        Nama Warung
                                        {this.renderTooltipForm("Nama warung harus mengandung kata \"Warung\", \"Gerai\", \"Kedai\", \"Lapak\" atau \"Toko\"")}
                                    </Form.Label>
                                    <Form.Control
                                        required
                                        type="text"
                                        onChange={this.handleChange}
                                        name="nama_warung"
                                        placeholder="Contoh: Warung 4 Sehat 5 Sentosa"
                                    />
                                </Form.Group>
                                <Form.Group controlId="FGLokasiWarung">
                                    <Form.Label>Alamat Warung</Form.Label>
                                    {this.renderTooltipForm("Alamat warung harus mencantumkan nama kecamatan dan nama kota")}
                                    <Form.Control
                                        required
                                        type="text"
                                        onChange={this.handleChange}
                                        name="lokasi_warung"
                                        placeholder="Contoh: Jalan Kenangan No. 1, Kecamatan Jatisari, Kota Jakarta Tenggara"
                                    />
                                </Form.Group>
                                <Form.Group controlId="FGTanggalKunjungan">
                                    <Form.Label>Tanggal Kunjungan</Form.Label>
                                    {this.renderTooltipForm("Pengisian data maksimal 12 jam setelah waktu kunjungan yang sebenarnya")}
                                    <InputGroup>
                                        <DateTimePicker
                                            calendarClassName="lg"
                                            name="datepicker"
                                            onChange={this.handleDateChange}
                                            value={this.state.datepicker}
                                            clearIcon={null}
                                            required={true}
                                            disableClock={true}
                                            minDate={new Date(new Date().setDate(new Date().getDate() - 1 ))}
                                            maxDate={new Date()}
                                            locale="id-ID"
                                        />
                                    </InputGroup>
                                </Form.Group>
                                <Form.Group controlId="FGAlasanPenolakan">
                                    <Form.Label>Alasan Penolakan</Form.Label>
                                    {this.renderTooltipForm("Cantumkan alasan penolakan warung secara singkat dan mudah dipahami")}
                                    <Form.Control required as="select" onChange={this.handleAlasan} name="customAlasan" defaultValue={''}>
                                        <option value="" disabled hidden>-- Pilih --</option>
                                        <option value="Belum mau bergabung">Belum mau bergabung</option>
                                        <option value="Usaha masih belum maju, belum perlu">Usaha masih belum maju, belum perlu</option>
                                        <option value="0">Lainnya</option>
                                    </Form.Control>
                                    {this.state.customAlasan === "0" &&
                                    <Form.Control
                                        required
                                        type="text"
                                        onChange={this.handleChange}
                                        name="alasan_penolakkan"
                                        placeholder="Alasan warung menolak bergabung menjadi mitra..."
                                    />
                                    }
                                </Form.Group>
                                <div className="form-row text-center">
                                    <div className="col-12">
                                        {this.renderSubmitButton()}
                                    </div>
                                </div>
                            </Form>
                        </Card.Body>
                    </Card>
                </Container>
            </React.Fragment>
        );
    }
}

export default WarungNoregCreate;

