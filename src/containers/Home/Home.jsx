import React, { Component } from "react";
import { Redirect } from 'react-router-dom'
import DatePicker from 'react-date-picker';
import Button from "react-bootstrap/Button";
import './Home.css';
import SalespersonReport from "../../components/SalespersonReport/SalespersonReport";
import ProgressBarSales from "../../components/ProgressBar/ProgressBarSales";
import logo from "./ilustration-penugasan.png";

class Home extends Component {
  state = {
    activePenugasan: null,
    individualPie: null,
    isLoading: true,
    tanggalAwal: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
    tanggalAkhir: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0),
    currentIndividualPie: null,
    currentIndividualBar: null,
    periode: "",
  };

  controller = new AbortController();

  componentWillUnmount() {
    this.controller.abort()
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {

    let startDate = this.state.tanggalAwal;
    if (startDate !== new Date(new Date().getFullYear(), new Date().getMonth(), 1)) {
      startDate = "?tanggal-awal=" + startDate.toISOString().split('T')[0]
    }

    let endDate = this.state.tanggalAkhir;
    if (endDate !== new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)) {
      endDate = "&tanggal-akhir=" + endDate.toISOString().split('T')[0]
    }

    const URL = "http://localhost:8080";
    const token = "Bearer " + localStorage.getItem("token");
    fetch(URL + "/dashboard/salesperson" + startDate + endDate, {
      method: "GET",
      signal: this.controller.signal,
      headers: {
        "Authorization": token,
      }
    }).then(response => {
      return response.json();
    }).then((data) => { 
      if (typeof data.ActivePenugasan !== 'undefined') {
        this.setState({
          activePenugasan: data.ActivePenugasan,
        });
      }
      if (typeof data.Individual !== 'undefined') {
        this.setState({
          individualPie: this.currentIndividualPie(data.Individual),
          currentIndividualBar: data.Individual.Report,
          periode: this.formatPeriodeToString(this.state.tanggalAwal, this.state.tanggalAkhir),
        });
      }
      this.setState({
        isLoading: false
      })
    })
  }

  currentIndividualPie = (data) => {
    return ({
      outsideLayer: [
        {name: 'Visit Berhasil', value: data.total_targeted_visit - data.total_achieved_visit > 0 ? data.total_achieved_visit : data.total_targeted_visit},
        {name: 'Visit Tidak berhasil', value: data.total_targeted_visit - data.total_achieved_visit > 0 ? data.total_targeted_visit - data.total_achieved_visit : 0},
        {name: 'Aktivasi Berhasil', value: data.total_targeted_activation - data.total_achieved_activation > 0 ? data.total_achieved_activation : data.total_targeted_activation},
        {name: 'Aktivasi Tidak Berhasil', value: data.total_targeted_activation - data.total_achieved_activation > 0 ? data.total_targeted_activation - data.total_achieved_activation : 0},
        {name: 'NOO Berhasil', value: data.total_targeted_noo - data.total_achieved_noo > 0 ? data.total_achieved_noo : data.total_targeted_noo},
        {name: 'NOO Tidak Berhasil', value: data.total_targeted_noo - data.total_achieved_noo > 0 ? data.total_targeted_noo - data.total_achieved_noo : 0}
      ],
      insideLayer: [
        { name: 'Target Visit', value: data.total_targeted_visit },
        { name: 'Target Aktivasi', value: data.total_targeted_activation },
        { name: 'Target NOO', value: data.total_targeted_noo }
      ]
    }
    );
  };

  formatPeriodeToString(startDate, endDate) {
    var tanggalMulai = new Date(Date.parse(startDate))
      .toLocaleString('id-ID',
        {
          year: 'numeric',
          month: 'long',
          day: 'numeric',
        });
    var tanggalSelesai = new Date(Date.parse(endDate))
      .toLocaleString('id-ID',
        {
          year: 'numeric',
          month: 'long',
          day: 'numeric',
        });
    return (tanggalMulai + " - " + tanggalSelesai)
  }

  handleApply = () => {
    this.fetchData();
  };

  dateIsChanged() {
    return !(
      this.state.tanggalAwal === new Date(new Date().getFullYear(), new Date().getMonth(), 1)
      && this.state.tanggalAkhir === new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)
    )
  }

  renderSubmitButton() {
    return (
      this.dateIsChanged() ?
        <Button onClick={this.handleApply}
          style={{ backgroundColor: '#FECF28', borderColor: '#FECF28', color: '#000000' }} type="submit"
          className="mt-4 mb-2 mh-auto btn btn-primary text-center">
          Apply
            </Button>
        :
        <span className="text-center">
          <button disabled style={{
            pointerEvents: 'none',
            backgroundColor: '#FECF28',
            borderColor: '#FECF28',
            color: '#000000'
          }} className="mt-4 mb-2 mh-auto btn btn-primary text-center">
            Simpan Data
                    </button>
        </span>
    )
  }

  handleStartDateChange = tanggal => {
    this.setState({
      tanggalAwal: tanggal
    });
  };
  handleEndDateChange = tanggal => {
    this.setState({
      tanggalAkhir: tanggal
    });
  };

  render() {
    if (!this.props.loggedIn) {
      return <Redirect to="/login" />
    }
    const queryString = require("query-string");
    const parsed = queryString.parse(window.location.search);
    if (Object.keys(parsed).length > 0) {
      return <Redirect to="/" />
    }
    if (this.state.isLoading) {
      return (
        <div style={{
          position: 'absolute', left: '50%', top: '50%',
          transform: 'translate(-50%, -50%)'
        }}>
          <h1 className="font-weight-bolder">Loading ...</h1>
        </div>
      )
    } else {
      if (this.state.activePenugasan === null) {
        return (
            <React.Fragment>
          <div id="report-sales" className="container-fluid">
            <div className="text-center">
              <p id="report-data">Sales {this.props.namaUser.split(" ")[0]}, kamu belum memiliki penugasan</p>
              <img src={logo} style={{height: "auto", width: "auto"}} alt="logo" className="mx-auto mt-2 center"/>
              <p id="report-data-detail">Tidak Ada Penugasan</p>
            </div>
          </div>
                <div className="card-report" id="chartContainerSales">
                    <div className="text-center">
                        <h2 id="judul-report">Rangkuman Kinerja</h2>
                        <h3 id="judul-report-periode">Periode</h3>
                        <div className="row justify-content-md-center">
                            <div className="col col-lg-2">
                                <DatePicker
                                    name="tanggalAwal"
                                    onChange={this.handleStartDateChange}
                                    value={this.state.tanggalAwal}
                                    style={{marginLeft: '40px'}}/>
                            </div>

                            <div className="col-md-auto">
                                <p>sampai</p>
                            </div>

                            <div className="col col-lg-2">
                                <DatePicker
                                    name="tanggalAkhir"
                                    onChange={this.handleEndDateChange}
                                    value={this.state.tanggalAkhir}/>
                            </div>
                        </div>
                        {this.renderSubmitButton()}
                    </div>
                    <SalespersonReport
                        individualPie={this.state.individualPie}
                        currentIndividualBar={this.state.currentIndividualBar}
                        periode={this.state.periode}
                    />
                </div>
        </React.Fragment>
        )
      } else {
        if (this.state.activePenugasan.start_date !== null) {
          return (
              <React.Fragment>
                <div id="report-sales" className="container-fluid">
                  <h3 id="profile"> Halo, Sales {this.props.namaUser.split(" ")[0]}!</h3>
                  <p id="subprofile"> Home</p>
                  <div id="card-progress" className="card">
                    <div className="text-center">
                      <h2 id="judul-progress">Progress Kinerja</h2>
                      <h2 id="judul-report-periode">
                        {new Date(Date.parse(this.state.activePenugasan.start_date)).toLocaleString('id-ID',
                            {year: 'numeric', month: 'long', day: 'numeric'})} -
                        {new Date(Date.parse(this.state.activePenugasan.end_date)).toLocaleString('id-ID',
                            {year: 'numeric', month: 'long', day: 'numeric'})}
                      </h2>
                    </div>

                    <ProgressBarSales
                        kuota_visit={this.state.activePenugasan.kuota_visit}
                        kuota_activation={this.state.activePenugasan.kuota_activation}
                        kuota_noo={this.state.activePenugasan.kuota_noo}
                        visit={this.state.activePenugasan.real_visit}
                        aktivasi={this.state.activePenugasan.real_activation}
                        NOO={this.state.activePenugasan.real_noo}
                    />
                  </div>

                  <div className="card-report" id="chartContainerSales">
                    <div className="text-center">
                      <h2 id="judul-report">Rangkuman Kinerja</h2>
                      <h3 id="judul-report-periode">Periode</h3>
                      <div className="row justify-content-md-center">
                        <div className="col col-lg-2">
                          <DatePicker
                              name="tanggalAwal"
                              onChange={this.handleStartDateChange}
                              value={this.state.tanggalAwal}
                              style={{marginLeft: '40px'}}/>
                        </div>

                        <div className="col-md-auto">
                          <p>sampai</p>
                        </div>

                        <div className="col col-lg-2">
                          <DatePicker
                              name="tanggalAkhir"
                              onChange={this.handleEndDateChange}
                              value={this.state.tanggalAkhir}/>
                        </div>
                      </div>
                      {this.renderSubmitButton()}
                    </div>

                    <SalespersonReport
                        individualPie={this.state.individualPie}
                        currentIndividualBar={this.state.currentIndividualBar}
                        periode={this.state.periode}
                    />
                  </div>
                </div>
              </React.Fragment>
          );
        } else {
          return (
              <React.Fragment>
                <div id="report-sales" className="container-fluid">
                  <h3 id="profile"> Halo, Sales {this.props.namaUser.split(" ")[0]}!</h3>
                  <p id="subprofile"> Kamu tidak memiliki penugasan aktif</p>
                  <div className="card-report" id="chartContainerSales">
                    <div className="text-center">
                      <h2 id="judul-report">Rangkuman Kinerja</h2>
                      <h3 id="judul-report-periode">Periode</h3>
                      <div className="row justify-content-md-center">
                        <div className="col col-lg-2">
                          <DatePicker
                              name="tanggalAwal"
                              onChange={this.handleStartDateChange}
                              value={this.state.tanggalAwal}
                              style={{marginLeft: '40px'}}/>
                        </div>

                        <div className="col-md-auto">
                          <p>sampai</p>
                        </div>

                        <div className="col col-lg-2">
                          <DatePicker
                              name="tanggalAkhir"
                              onChange={this.handleEndDateChange}
                              value={this.state.tanggalAkhir}/>
                        </div>
                      </div>
                      {this.renderSubmitButton()}
                    </div>

                    <SalespersonReport
                        individualPie={this.state.individualPie}
                        currentIndividualBar={this.state.currentIndividualBar}
                        periode={this.state.periode}
                    />
                  </div>
                </div>
              </React.Fragment>
          )
        }
      }
    }
  }
}

export default Home;
