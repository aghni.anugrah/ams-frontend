import React, { Component } from "react";
import { Redirect } from 'react-router-dom'
//import Loader from 'react-loader-spinner'
import SalespersonData from "../../components/Table/SalespersonData";
import CustomPagination from "../../components/Pagination/CustomPagination";
import CustomModal from "../../components/Modal/CustomModal";
import Alert from "react-bootstrap/Alert";
import { faUserPlus } from "@fortawesome/free-solid-svg-icons";
import "./SalespersonControl.module.css"
import classes from "./SalespersonControl.module.css"
import Card from "react-bootstrap/Card";
// import NavigationBar from "../../components/Navigation/NavigationBar";
import OptionButton from "../../components/Filter/OptionButton";
import Filter from "../../components/Filter/Filter";
import Button from "react-bootstrap/Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const ITEM_PER_PAGE = 5;

class SalespersonControl extends Component {
    state = {
        listSalesperson : [],
        Wilayah: "",
        showDetailModal: false,
        showConfirmationModal: false,
        successDelete: false,
        isLoading: true,
        showSuccessAlert: false,
        id: "",
        nama: "",
        email: "",
        referral_code: "",
        currentSalesperson: [],
        currentPage: 1,
        showFilter: false,
        search: "",
        dsc: false,
        opt: [
            ["Nama Salesperson", "nama-salesperson"]
        ],
        selectedFilter: ["Nama Salesperson", "nama-salesperson"],
        nameNotificationDelete: ""
    };

    componentDidMount() {
        this.fetchData();
    }

    handleSearch = (e) => {
        this.setState({search: e.target.value})
    };

    handleSelectOrder = (orderDsc) => {
        this.setState({dsc: orderDsc})
    };

    handleSelectFilter = (selected) => {
        this.setState({selectedFilter: selected});
    };

    handleApply = () => {
        this.fetchData();
        this.setState({currentPage:1})
    };

    handleFilter = () => {
        this.setState({showFilter: !this.state.showFilter})
    };

    fetchData = () => {
        let search = this.state.search;
        if (search !== "") {
            search = "&search=" + search
        }

        let sort;
        if (this.state.selectedFilter.length > 0) {
            sort = "&sort-by=" + this.state.selectedFilter[1]
        }

        let dsc = "?dsc=" + this.state.dsc;

        const URL = "http://localhost:8080";
        const token = "Bearer " + localStorage.getItem("token");
        fetch(URL + "/user/salespersons" + dsc + sort + search, {
            method: "GET",
            headers: {
                "Authorization" : token,
            }
        }).then(response => {
            return response.json();
        }).then((data) => {
            const fetchedData = [];
            let wilayah;
            for (let key in data.Anggota) {
                fetchedData.push({
                    ...data.Anggota[key]
                });
            }
            wilayah = data.Wilayah;
            this.setState({
                listSalesperson: fetchedData,
                currentSalesperson: fetchedData.slice(0, ITEM_PER_PAGE),
                Wilayah: wilayah,
                isLoading: false
            });
        }).catch(err => {
            throw new Error(err)
        });
    }

    closeDetailModal = () => {
        this.setState({
            showDetailModal: false,
        })
    };

    closeConfirmationModal = () => {
        if (this.state.showConfirmationModal) {
            this.setState({
                showConfirmationModal: false
            })
        }
    };

    showConfirmationHandler = (salesperson) => {
        if (!this.state.showConfirmationModal) {
            this.setState({
                id: salesperson.id,
                nama: salesperson.nama,
                showConfirmationModal: true,
            })
        }
    };

    showDetailHandler = (salesperson) => {
        this.setState({
            showDetailModal: true,
            nama: salesperson.nama,
            email: salesperson.email,
            referral_code: salesperson.referral_code
        })
    };

    deleteToEndpoint = (e) => {
        e.preventDefault();
        const idSalesperson = this.state.id;
        let i;
        for (i = 0; i < this.state.listSalesperson.length; i++){
            if (idSalesperson === this.state.listSalesperson[i].id) {
                this.setState({
                    nameNotificationDelete: this.state.listSalesperson[i].nama
                })
            }
        }
        const token = "Bearer " + localStorage.getItem("token");
        fetch("http://localhost:8080/user/delete-lead/" + idSalesperson, {
            method: "PUT",
            headers: {
                "Authorization" : token,
            }
        }).then(response => {
            if (response.status === 200) {
                    this.componentDidMount();
                    this.setState({
                    successDelete: true,
                    showSuccessAlert: true,
                    showConfirmationModal: false
                })
            }
        })
    };

    onPageChanged = data => {
        const { currentPage, pageLimit } = data;

        const offset = (currentPage - 1) * pageLimit;
        const currentSalesperson = this.state.listSalesperson.slice(offset, offset + pageLimit);

        this.setState({ currentSalesperson, currentPage });
    };

    renderDetail() {
        return this.state.nama === "" ?
            (
                <div>
                    <p>No data to be shown</p>
                </div>
            )
            :
            (
                <div>
                    <p>Nama Salesperson: {this.state.nama}</p>
                    <p>Email Salesperson: {this.state.email}</p>
                    <p>Referral: {this.state.referral_code}</p>
                </div>
            )
    }

    renderConfirmation() {
        return (
            <div>
                <p>Apakah anda yakin ingin mengeluarkan {this.state.nama} dari tim?</p>
            </div>
        )
    }

    renderWilayah() {
        return (
            <h3>Wilayah: {this.state.Wilayah}</h3>
        )
    }

    render() {
        if (!this.props.loggedIn) {
            return <Redirect to="/login"/>
        }
        if (this.state.isLoading){
            return (
                // <div style={{
                //     position: 'absolute', left: '50%', top: '50%',
                //     transform: 'translate(-50%, -50%)'
                // }}>
                //     <Loader
                //         type="Oval"
                //         color="#FECF28"
                //         height={300}
                //         width={300}
                //         timeout={5000}
                //     />
                // </div>
                <br/>
            )
        } else {
            return (
                <React.Fragment>
                        <h2 className="font-weight-bold">Anggota Tim</h2>
                        <Card className="shadow-lg mb-5 bg-white rounded">
                            {this.state.showSuccessAlert &&
                                <Alert
                                    className={classes.alert}
                                    variant="success"
                                    onClose={() =>
                                        this.setState({
                                            showSuccessAlert: false
                                        })
                                    }
                                    dismissible>
                                    <p>{this.state.nameNotificationDelete.split(" ")[0]} berhasil dikeluarkan</p>
                                </Alert>
                            }
                            {this.state.showDetailModal &&
                            <CustomModal
                                show={this.state.showDetailModal}
                                title="Detail Salesperson"
                                onHide={this.closeDetailModal}
                                primaryButton="Oke"
                                primaryAction={this.closeDetailModal}>
                                {this.renderDetail()}
                            </CustomModal>
                            }
                            {this.state.showConfirmationModal &&
                            <CustomModal
                                show={this.state.showConfirmationModal}
                                title="Konfirmasi Penghapusan"
                                onHide={this.closeConfirmationModal}
                                primaryButton="Ya, hapus"
                                primaryAction={this.deleteToEndpoint}
                                secondaryButton="Batal"
                                secondaryAction={this.closeConfirmationModal}
                                type="confirm"
                            >
                                {this.renderConfirmation()}
                            </CustomModal>
                            }
                            {!this.state.showSuccessAlert &&
                            <div className="card-header" id="header">
                                <div className="col-md-12 d-flex align-items-center">
                                    {this.renderWilayah()}
                                    <Button href="/salesperson-add" variant="link" className="btn ml-auto"><FontAwesomeIcon icon={ faUserPlus }/></Button>
                                </div>
                            </div>
                            }
                            {this.state.showSuccessAlert &&
                            <div/>
                            }
                            <div className={classes.filter}>
                                <OptionButton handleChange={this.handleFilter}/>
                            </div>
                            {this.state.showFilter ?
                                <React.Fragment>
                                    <Filter data={this.state} handleChange={this.handleSelectFilter} handleSort={this.handleSelectOrder}
                                            handleSearch={this.handleSearch} handleApply={this.handleApply}/>
                                </React.Fragment>
                                :
                                <React.Fragment/>
                            }
                            <Card.Body style={{ paddingTop: '5px' }}>
                                <table className="table table-hover">
                                    <thead className="Header">
                                    <tr>
                                        <th scope="col" className="font-weight-lighter">Nama Sales</th>
                                        <th scope="col"/>
                                    </tr>
                                    </thead>
                                        <SalespersonData data={this.state.currentSalesperson} detail={this.showDetailHandler} confirmation={this.showConfirmationHandler}/>
                                </table>
                                <div className="d-flex flex-row fa-pull-right">
                                    <CustomPagination
                                        currentPage={this.state.currentPage}
                                        totalRecords={this.state.listSalesperson.length}
                                        pageLimit={ITEM_PER_PAGE}
                                        onPageChanged={this.onPageChanged}
                                    />
                                </div>
                            </Card.Body>
                        </Card>
                </React.Fragment>
            );
        }
    }
}

export default SalespersonControl