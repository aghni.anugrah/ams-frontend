import React, { Component } from "react";
import { Redirect } from 'react-router-dom'
//import Loader from 'react-loader-spinner'
import SalespersonNonTeamData from "../../../components/Table/SalespersonNonTeamData";
import CustomModal from "../../../components/Modal/CustomModal";
import Alert from "react-bootstrap/Alert";
import "../SalespersonControl.module.css"
import classes from "../SalespersonControl.module.css"
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import OptionButton from "../../../components/Filter/OptionButton";
import Filter from "../../../components/Filter/Filter";

class SalespersonAddition extends Component {
    state = {
        listSalesperson: [],
        Wilayah: "",
        showDetailModal: false,
        showConfirmationModal: false,
        successAdd: false,
        isLoading: true,
        showSuccessAlert: false,
        id: "",
        nama: "",
        email: "",
        referral_code: "",
        showFilter: false,
        search: "",
        dsc: false,
        opt: [
            ["Nama Salesperson", "nama-salesperson"]
        ],
        selectedFilter: ["Nama Salesperson", "nama-salesperson"],
        banyakAnggota: "",
        listTim: [],
    };

    handleSearch = (e) => {
        this.setState({search: e.target.value})
    };

    handleSelectOrder = (orderDsc) => {
        this.setState({dsc: orderDsc})
    };

    handleSelectFilter = (selected) => {
        this.setState({selectedFilter: selected});
    };

    handleApply = () => {
        this.fetchData()
    };

    handleFilter = () => {
        this.setState({showFilter: !this.state.showFilter})
    };

    componentDidMount() {
        this.fetchData();
        this.fetchTeamData();
    }

    fetchTeamData = () => {
        const URL = "http://localhost:8080";
        const token = "Bearer " + localStorage.getItem("token");
        fetch(URL + "/user/salespersons", {
            method: "GET",
            headers: {
                "Authorization" : token,
            }
        }).then(response => {
            return response.json();
        }).then((data) => {
            const fetchedData = [];
            for (let key in data.Anggota) {
                fetchedData.push({
                    ...data.Anggota[key]
                });
            }
            this.setState({
                listTim: fetchedData,
                banyakAnggota: this.state.listTim.length,
            });
        }).catch(err => {
            throw new Error(err)
        });
    }

    fetchData = () => {
        let search = this.state.search;
        if (search !== "") {
            search = "&search=" + search
        }

        let sort;
        if (this.state.selectedFilter.length > 0) {
            sort = "&sort-by=" + this.state.selectedFilter[1]
        }

        let dsc = "?dsc=" + this.state.dsc;

        const URL = "http://localhost:8080";
        const token = "Bearer " + localStorage.getItem("token");
        fetch(URL + "/user/salespersons-non-team" + dsc + sort + search, {
            method: "GET",
            headers: {
                "Authorization" : token,
            }
        }).then(response => {
            return response.json();
        }).then((data) => {
            let wilayah;
            const fetchedData = [];
            for (let key in data.Anggota) {
                fetchedData.push({
                    ...data.Anggota[key]
                });
            }
            wilayah = data.Wilayah;
            this.setState({
                listSalesperson: fetchedData,
                Wilayah: wilayah,
                isLoading: false
            });
        }).catch(err => {
            throw new Error(err)
        });
    }

    closeDetailModal = () => {
        this.setState({
            showDetailModal: false,
        })
    };

    closeConfirmationModal = () => {
        if (this.state.showConfirmationModal) {
            this.setState({
                showConfirmationModal: false
            })
        }
    };

    showConfirmationHandler = (salesperson) => {
        if (!this.state.showConfirmationModal) {
            this.setState({
                id: salesperson.id,
                nama: salesperson.nama,
                showConfirmationModal: true,
            })
        }
    };

    showDetailHandler = (salesperson) => {
        this.setState({
            showDetailModal: true,
            nama: salesperson.nama,
            email: salesperson.email,
            referral_code: salesperson.referral_code
        })
    };

    addToEndPoint = (e) => {
        e.preventDefault();
        const idSalesperson = this.state.id;
        const token = "Bearer " + localStorage.getItem("token");
        fetch("http://localhost:8080/user/update/" + idSalesperson, {
            method: "PUT",
            headers: {
                "Authorization" : token,
            }
        }).then(response => {
            if (response.status === 200) {
                this.componentDidMount();
                this.setState({
                    successAdd: true,
                    showSuccessAlert: true,
                    showConfirmationModal: false
                })
            }
        })
    };

    renderDetail() {
        return this.state.nama === "" ?
            (
                <div>
                    <p>No data to be shown</p>
                </div>
            )
            :
            (
                <div>
                    <p>Nama Salesperson: {this.state.nama}</p>
                    <p>Email Salesperson: {this.state.email}</p>
                    <p>Referral: {this.state.referral_code}</p>
                </div>
            )
    }

    renderWilayah() {
        return (
            <h3>Wilayah: {this.state.Wilayah}</h3>
        )
    }

    renderConfirmation() {
        return (
            <div>
                <p>Apakah anda yakin ingin menambahkan {this.state.nama} ke dalam tim?</p>
            </div>
        )
    }

    render() {
        if (!this.props.loggedIn) {
            return <Redirect to="/login"/>
        }
        if (this.state.isLoading){
            return (
                // <div style={{
                //     position: 'absolute', left: '50%', top: '50%',
                //     transform: 'translate(-50%, -50%)'
                // }}>
                //     <Loader
                //         type="Oval"
                //         color="#FECF28"
                //         height={300}
                //         width={300}
                //         timeout={5000}
                //     />
                // </div>
                <br/>
            )
        } else {
            return (
                <React.Fragment>
                        <h2 className="font-weight-bold">Tambah Anggota Tim</h2>
                        <Card className="shadow-lg mb-5 bg-white rounded">
                            {this.state.showSuccessAlert &&
                            <Alert
                                className={classes.alertAdd}
                                variant="success"
                                onClose={() =>
                                    this.setState({
                                        showSuccessAlert: false
                                    })
                                }
                                dismissible>
                                <p>Salesperson berhasil ditambahkan. Anggota tim: {this.state.banyakAnggota + 1}</p>
                            </Alert>
                            }
                            {this.state.showDetailModal &&
                            <CustomModal
                                show={this.state.showDetailModal}
                                title="Detail Salesperson"
                                onHide={this.closeDetailModal}
                                primaryButton="Oke"
                                primaryAction={this.closeDetailModal}>
                                {this.renderDetail()}
                            </CustomModal>
                            }
                            {this.state.showConfirmationModal &&
                            <CustomModal
                                show={this.state.showConfirmationModal}
                                title="Konfirmasi Penambahan Salesperson"
                                onHide={this.closeConfirmationModal}
                                primaryButton="Ya, tambahkan"
                                primaryAction={this.addToEndPoint}
                                secondaryButton="Batal"
                                secondaryAction={this.closeConfirmationModal}
                                type="confirm"
                            >
                                {this.renderConfirmation()}
                            </CustomModal>
                            }
                            {!this.state.showSuccessAlert &&
                            <div className="card-header" id="header">
                                <div className="col-md-12 d-flex align-items-center">
                                    {this.renderWilayah()}
                                </div>
                            </div>
                            }
                            {this.state.showSuccessAlert &&
                            <div/>
                            }
                            <div className={classes.filter}>
                                <OptionButton handleChange={this.handleFilter}/>
                            </div>
                            {this.state.showFilter ?
                                <React.Fragment>
                                    <Filter data={this.state} handleChange={this.handleSelectFilter} handleSort={this.handleSelectOrder}
                                            handleSearch={this.handleSearch} handleApply={this.handleApply}/>
                                </React.Fragment>
                                :
                                <React.Fragment/>
                            }
                            <Card.Body style={{ paddingTop: '5px' }}>
                                <table className="table table-hover">
                                    <thead className="Header">
                                    <tr>
                                        <th scope="col" className="font-weight-lighter">Nama Sales</th>
                                        <th scope="col"/>
                                    </tr>
                                    </thead>
                                    <SalespersonNonTeamData data={this.state.listSalesperson} detail={this.showDetailHandler} confirmation={this.showConfirmationHandler}/>
                                </table>
                            </Card.Body>
                        </Card>
                        <Button style={{marginBottom: "20px"}} variant="primary" href="/salesperson-control">Anggota Tim</Button>
                </React.Fragment>
            );
        }
    }
}
export default SalespersonAddition;