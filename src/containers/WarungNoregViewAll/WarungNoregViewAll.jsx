import React, {Component} from "react";
import {Redirect, withRouter} from "react-router-dom";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import TableWarungNoreg from '../../components/TableWarungNoreg/TableWarungNoreg'
import CustomModal from "../../components/Modal/CustomModal";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";
import Alert from "react-bootstrap/Alert";
import Expire from "../../components/Expire/Expire";
import {faPlusSquare} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import CustomPagination from "../../components/Pagination/CustomPagination";
import OptionButton from "../../components/Filter/OptionButton";
import Filter from "../../components/Filter/Filter";

const ITEM_PER_PAGE = 5;

class WarungNoregViewAll extends Component {
    state = {
        warungNoreg: [],
        hasActivePenugasan: false,
        isLoading: true,
        showSuccessAlert: this.props.location.state !== undefined,
        showDetailModal: false,
        namaWarung: "",
        tanggalKunjungan: "",
        alamat: "",
        alasanPenolakan: "",
        currentWarungs: [],
        currentPage: 1,
        showFilter: false,
        search: "",
        dsc: true,
        opt: [
            ["Tanggal Kunjungan", "tanggal"],
            ["Nama Warung", "nama-warung"]
        ],
        selectedFilter: ["Tanggal Kunjungan", "tanggal"]
    };

    controller = new AbortController();

    componentDidMount() {
        this.fetchData();
        window.history.replaceState(undefined, '')
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.state.namaWarung !== prevState.namaWarung || this.state.currentPage !== prevState.currentPage){
            this.setState({
                showSuccessAlert: false
            });
        }
        window.history.replaceState(undefined, '')
    }

    componentWillUnmount() {this.controller.abort()}

    fetchData(){

        let search = this.state.search;
        if (search !== "") {
            search = "&search=" + search
        }

        let sort;
        if (this.state.selectedFilter.length > 0) {
            sort = "&sort-by=" + this.state.selectedFilter[1]
        }

        let dsc = "?dsc=" + this.state.dsc;

        const URL = "http://localhost:8080";

        const token = "Bearer " + localStorage.getItem("token");
        fetch(URL + "/warung-tidak-daftar/view-all" + dsc + sort + search, {
            method: "GET",
            signal: this.controller.signal,
            headers: {
                "Authorization" : token,
            }
        }).then(response => {
            return response.json();
        }).then((data) => {
            const fetchedData = [];
            for (let key in data.ListWarungNoreg){
                fetchedData.push({
                    ...data.ListWarungNoreg[key]
                });
            }
            this.setState({
                hasActivePenugasan: data.active_penugasan,
                warungNoreg: fetchedData,
                currentWarungs: fetchedData.slice(0, ITEM_PER_PAGE),
                currentPage: 1,
                isLoading: false
            });
        }).catch(() => {
            this.setState({warungNoreg: []})
        });
    }

    handleSearch = (e) => {
        this.setState({search: e.target.value})
    };

    handleSelectOrder = (orderDsc) => {
        this.setState({dsc: orderDsc})
    };

    handleSelectFilter = (selected) => {
        this.setState({selectedFilter: selected});
    };

    handleApply = () => {
        this.fetchData();
    };

    handleFilter = () => {
        this.setState({showFilter: !this.state.showFilter})
    };

    closeDetailModal = () => {
        this.setState({
            showDetailModal: false,
            namaWarung: "",
            alamat: "",
            tanggalKunjungan: "",
            alasanPenolakan: ""
        });
    };

    showDetailHandler = (warung) => {
        this.setState({
            showDetailModal: true,
            namaWarung: warung.nama_warung,
            alamat: warung.lokasi_warung,
            tanggalKunjungan: new Date(Date.parse(warung.tanggal_kunjungan))
                .toLocaleString('id-ID',
                    {
                        weekday: 'long',
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric',
                        hour:'2-digit',
                        minute: '2-digit'
                    }),
            alasanPenolakan: warung.alasan_penolakkan
        })
    };

    closeAlert = () =>{
        this.setState({
            showSuccessAlert: false
        });
        // window.history.replaceState(undefined, '')
    };

    onPageChanged = data => {
        const { currentPage, pageLimit } = data;

        const offset = (currentPage - 1) * pageLimit;
        const currentWarungs = this.state.warungNoreg.slice(offset, offset + pageLimit);

        this.setState({ currentWarungs, currentPage });
    };

    renderDetail(){
        return (
                <div>
                    <p style={{wordWrap:'break-word'}}>Nama Warung: {this.state.namaWarung}</p>
                    <p style={{wordWrap:'break-word'}}>Alamat Warung: {this.state.alamat}</p>
                    <p >Tanggal Kunjungan: {this.state.tanggalKunjungan}</p>
                    <p style={{wordWrap:'break-word'}}>Alasan Penolakan: {this.state.alasanPenolakan}</p>
                </div>
            )
    }

    render() {
        console.log(this.state)
        if (!this.props.loggedIn) {
            return <Redirect to="/login"/>
        }
        if (this.state.isLoading){
            return (
                <div style={{
                    position: 'absolute', left: '50%', top: '50%',
                    transform: 'translate(-50%, -50%)'
                }}>
                    <h1 className="font-weight-bolder">Loading ...</h1>
                </div>
            )
        }
        else {
            return (
                <React.Fragment>
                    <Container fluid style={{marginTop:'-3rem',paddingBottom:'8em'}}>
                        <h2 className="text-center mb-4 font-weight-bold">
                            Calon Mitra Tidak Registrasi
                        </h2>
                        <Card className="shadow-lg mb-5 bg-white rounded">
                            <Card.Body>
                                <div className="container">
                                    <CustomModal
                                        show={this.state.showDetailModal}
                                        title="Detail Warung"
                                        onHide={this.closeDetailModal}
                                        primaryButton="Oke"
                                        primaryAction={this.closeDetailModal}
                                    >
                                        {this.renderDetail()}
                                    </CustomModal>
                                    {this.state.showSuccessAlert &&
                                        <Expire delay={4000}>
                                            <Alert
                                                variant="success"
                                                onClose={this.closeAlert}
                                                dismissible>
                                                <p>Data warung {this.props.location.state.name} berhasil disimpan!</p>
                                            </Alert>
                                        </Expire>
                                    }
                                    {
                                        !this.state.hasActivePenugasan ?
                                            <div>
                                                <h5 className="text-center">
                                                    Kamu tidak memiliki penugasan aktif. <br/>
                                                    Penambahan warung tidak mendaftar saat ini tidak akan menambah capaian visit kamu.
                                                </h5>
                                            </div>
                                            :
                                            null
                                    }
                                    <button className="btn btn-link fa-pull-left">
                                        <a href="/warung-tidak-registrasi/create">
                                            <FontAwesomeIcon
                                                id="create-icon"
                                                icon={faPlusSquare}
                                                style={{
                                                    color: '#FECF28',
                                                    marginTop:'1vw'
                                                }}
                                                size="2x"
                                            />
                                        </a>
                                    </button>
                                    <OptionButton className="fa-pull-right" handleChange={this.handleFilter}/>
                                    {this.state.showFilter ?
                                        <Filter data={this.state} handleChange={this.handleSelectFilter} handleSort={this.handleSelectOrder}
                                                    handleSearch={this.handleSearch} handleApply={this.handleApply}/>
                                        :
                                        null
                                    }
                                    {this.state.warungNoreg.length > 0 ?
                                        <TableWarungNoreg data={this.state.currentWarungs} detail={this.showDetailHandler}/>
                                        :
                                        <p className="text-center m-3">Tidak ada data untuk ditampilkan</p>
                                    }
                                    <div className="d-flex flex-row py-4 fa-pull-right">
                                        <CustomPagination
                                            currentPage={this.state.currentPage}
                                            totalRecords={this.state.warungNoreg.length}
                                            pageLimit={ITEM_PER_PAGE}
                                            onPageChanged={this.onPageChanged}
                                        />
                                    </div>
                                </div>
                            </Card.Body>
                        </Card>
                    </Container>
                </React.Fragment>
            );
        }
    }
}

export default withRouter(WarungNoregViewAll);