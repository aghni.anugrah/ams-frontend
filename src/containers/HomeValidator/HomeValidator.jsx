import React, {Component} from "react";
import {Redirect} from 'react-router-dom'

class Home extends Component {
    render() {
        if (!this.props.loggedIn) {
            return <Redirect to="/login"/>
        } else {
            return (
                <React.Fragment>
                    <div className="container-fluid">
                        <div className="row">
                            <h1>Halo, {this.props.namaUser}!</h1>
                        </div>
                        <div className="row">
                            <img src={require('./home-logo.png')} alt="" className="mx-auto mt-2 center"/>
                        </div>
                    </div>
                </React.Fragment>
            );
        }
    }
}

export default Home;