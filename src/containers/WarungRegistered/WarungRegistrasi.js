import React, {Component} from "react";
import TableWarung from "../../components/TableWarung/TableWarung";
import Status from "../../components/StatusTableWarung/Status";
import "./WarungRegistrasi.module.css"
import Filter from "../../components/Filter/Filter";
import OptionButton from "../../components/Filter/OptionButton";
import CustomPagination from "../../components/Pagination/CustomPagination";
import {Redirect} from "react-router-dom";
import CustomModal from "../../components/Modal/CustomModal";
import {faCheck, faClock, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const ITEM_PER_PAGE = 5;

class WarungRegistrasi extends Component {
  state = {
    warung: [],
    showModal: false,
    warungSelected: {},
    showFilter: false,
    search: "",
    dsc: true,
    opt: [
      ["Tanggal", "tanggal"],
      ["Lokasi", "lokasi"],
      ["Nama Mitra", "nama-mitra"],
      ["Status", "status"]
    ],
    selectedFilter: ["Tanggal", "tanggal"],
    currentWarung: [],
    currentPage: 1,
    showDetailModal: false,
    update: "",
    loading: true
  };

  controller = new AbortController();

  closeDetailModal = () => {
    this.setState({
      showModal: false,
      warungSelected: {}
    });
  };

  handleOnPageChanged = data => {
    const {currentPage, pageLimit} = data;
    const offset = (currentPage - 1) * pageLimit;
    const currentWarung = this.state.warung.slice(offset, offset + pageLimit);
    this.setState({currentWarung, currentPage});
  };

  handleSearch = (e) => {
    this.setState({search: e.target.value})
  };

  handleSelectOrder = (orderDsc) => {
    this.setState({dsc: orderDsc})
  };

  handleSelectFilter = (selected) => {
    this.setState({selectedFilter: selected});
  };

  handleApply = () => {
    this.fetchData();
    this.setState({currentPage: 1})
  };

  handleFilter = () => {
    this.setState({showFilter: !this.state.showFilter})
  };

  handleUpdate = id => {
    this.setState({update: id})
  };

  renderDetailMitra = () => {
    const warung = this.state.warungSelected;
    let reason;
    let status;
    if (warung.state === "registration_done" || warung.state === "registration_accepted" ||
      warung.state === "validation_pending" || warung.state === "validation accepted") {
      status = <span><p>Dalam Proses Verifikasi</p><FontAwesomeIcon icon={faClock}/></span>
    } else if (warung.state === "ready") {
      status = <span><p>Registrasi Berhasil</p><FontAwesomeIcon icon={faCheck}/></span>
    } else if (warung.state === "rejected") {
      status = <span><p>Registrasi Gagal</p><FontAwesomeIcon icon={faTimes}/></span>
    } else {
      status = "ERROR"
    }

    if (warung.state === "rejected") {
      reason =
        <div>
          <hr/>
          <h3>Alasan Penolakan:</h3>
          <p>{warung.reject_reason}</p>
        </div>
    }

    return (
      <div>
        <h4>{this.state.warungSelected.warung_name}</h4>
        <h5>Status: {status}</h5>
        <hr/>
        <div className="detail-warung">
          <p>Nama Pemilik: {this.state.warungSelected.mitra_name}</p>
          <p>Jenis Usaha: {this.state.warungSelected.business_type}</p>
          <p>No. HP: {this.state.warungSelected.phone_number}</p>
          <p>Alamat: {this.state.warungSelected.address}</p>
          <p>Kelurahan: {this.state.warungSelected.village_name}</p>
        </div>
        {reason}
      </div>
    )
  };

  componentDidMount() {
    this.fetchData()
  }

  componentWillUnmount() {
    this.controller.abort()
  }

  fetchData = () => {
    let search = this.state.search;
    if (search !== "") {
      search = "&search=" + search
    }

    let sort;
    if (this.state.selectedFilter.length > 0) {
      sort = "&sort-by=" + this.state.selectedFilter[1]
    }

    let dsc = "?dsc=" + this.state.dsc;

    const URL = "http://localhost:8080";
    const token = "Bearer " + localStorage.getItem("token");
    fetch(URL + "/warung" + dsc + sort + search, {
      signal: this.controller.signal,
      method: "GET",
      headers: {
        'Authorization': token
      }
    }).then(res => res.json()).then(result => {
        this.setState({
          warung: result.Warung,
          currentWarung: result.Warung.slice(0, ITEM_PER_PAGE),
          loading: false
        });
      }
    ).catch((err) => {
      this.setState({warung: [], loading: false})
    });
  };

  render() {
    if (this.state.update !== "") {
      let url = "/warung/update/" + this.state.update;
      return <Redirect to={url}/>
    }
    if (this.state.loading) {
      return (
        <h1>Loading</h1>
      )
    }
    return (
      <React.Fragment>
        <h1>List Calon Mitra</h1>

        <div className="row justify-content-end mr-1 pt-2">
          <OptionButton handleChange={this.handleFilter}/>
          <a className="btn btn-warning ml-1" href="/warung/create" role="button">+</a>
        </div>
        {this.state.showFilter ?
          <div className="mt-2">
            <Filter data={this.state} handleChange={this.handleSelectFilter} handleSort={this.handleSelectOrder}
                    handleSearch={this.handleSearch} handleApply={this.handleApply}/>
            <Status/>
          </div>
          :
          <React.Fragment></React.Fragment>
        }

        <TableWarung warung={this.state.currentWarung} modal={(warungSelected) => {
          this.setState({showModal: true, warungSelected})
        }}/>

        <div className="d-flex flex-row fa-pull-right">
          <CustomPagination
            currentPage={this.state.currentPage}
            totalRecords={this.state.warung.length}
            pageLimit={ITEM_PER_PAGE}
            onPageChanged={this.handleOnPageChanged}
          />
        </div>

        <CustomModal
          show={this.state.showModal}
          title="Detail Mitra"
          onHide={this.closeDetailModal}
          primaryButton="Ok"
          primaryAction={this.closeDetailModal}
          secondaryButton="Update"
          secondaryAction={() => this.handleUpdate(this.state.warungSelected.id.toString())}
        >
          {this.renderDetailMitra()}
        </CustomModal>
      </React.Fragment>
    );
  }
}

export default WarungRegistrasi;