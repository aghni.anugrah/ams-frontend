import React, {Component} from "react";
import {Redirect} from 'react-router-dom'
import LogoutButton from "../../components/LogoutButton";
import LeadSalesReport from "../../components/LeadSalesReport/LeadSalesReport";
import CustomModal from "../../components/Modal/CustomModal";
import DatePicker from 'react-date-picker';
import Button from "react-bootstrap/Button";
import CustomTwoLevelPieChart from "../../components/CustomRecharts/CustomPieChart/CustomTwoLevelPieChart";
import CustomBarChart from "../../components/CustomRecharts/CustomBarChart/CustomBarChart";
import Card from "react-bootstrap/Card";
import PenugasanTimActiveCard from "../../components/Cards/PenugasanTimActiveCard";
import Container from "react-bootstrap/Container";
import Accordion from "react-bootstrap/Accordion";
import PenugasanCompletionCard from "../../components/Cards/PenugasanCompletionCard";
import CustomPagination from "../../components/Pagination/CustomPagination";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import Table from "react-bootstrap/Table";
import OptionButton from "../../components/Filter/OptionButton";
import Filter from "../../components/Filter/Filter";
import './HomeLeadSales.css'

const ITEM_PER_PAGE = 5;

class HomeLeadSales extends Component {
    state = {
        activePenugasan: null,
        overviewPie: null,
        overviewBar: null,
        overviewBestSales: null,
        individual: null,
        isLoadingActivePenugasan: true,
        isLoadingReport: true,
        tanggalAwal: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
        tanggalAkhir: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0),
        customDate: false,
        showIndividualReportModal: false,
        namaSales: "",
        currentIndividualPie: null,
        currentIndividualBar: null,
        currentIndividualCompletionRate: null,
        tabsKey:'overview',
        periode: "",
        currentSalesInPage: [],
        currentPage: 1,
        showFilter: false,
        search: "",
        dsc: false,
        opt: [
            ["Nama Salesperson", "nama-salesperson"]
        ],
        selectedFilter: ["Nama Salesperson", "nama-salesperson"]
    };

    controller = new AbortController();

    componentDidMount() {this.fetchActivePenugasanData();}

    componentWillUnmount() {this.controller.abort()}

    fetchActivePenugasanData(){
        const URL = "http://localhost:8080";
        const token = "Bearer " + localStorage.getItem("token");
        fetch(URL + "/dashboard/active-penugasan", {method: "GET", signal: this.controller.signal, headers: {"Authorization" : token,}
        }).then(response => {return response.json();
        }).then((data) => {this.setState({activePenugasan: data.ActivePenugasan, isLoadingActivePenugasan: false});})
    }

    fetchReportData(){
        let search = this.state.search;
        if (search !== "") {
            search = "&search=" + search
        }
        let sort;
        if (this.state.selectedFilter.length > 0) {
            sort = "&sort-by=" + this.state.selectedFilter[1]
        }
        let dsc = "&dsc=" + this.state.dsc;
        let startDate = this.state.tanggalAwal;
        if (startDate !== new Date(new Date().getFullYear(), new Date().getMonth(), 1)) {
            startDate = "?tanggal-awal=" + startDate.toISOString().split('T')[0]
        }
        let endDate = this.state.tanggalAkhir;
        if (endDate !== new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)) {
            endDate = "&tanggal-akhir=" + endDate.toISOString().split('T')[0]
        }
        const URL = "http://localhost:8080";
        const token = "Bearer " + localStorage.getItem("token");
        fetch(URL + "/dashboard/team-report" + startDate + endDate + dsc + sort + search, {method: "GET", signal: this.controller.signal, headers: {"Authorization" : token,}
        }).then(response => {return response.json();
        }).then((data) => {
            console.log(data.Individual.slice(0, ITEM_PER_PAGE));
            this.setState({
                overviewPie: data.Overview,
                overviewBar: data.Overview.SalespersonComparison,
                overviewBestSales: this.getBestSalesperson(data.Individual),
                individual: data.Individual,
                periode:this.formatPeriodeToString(this.state.tanggalAwal, this.state.tanggalAkhir),
                currentSalesInPage: data.Individual.slice(0, ITEM_PER_PAGE),
                currentPage: 1,
                isLoadingReport: !this.state.isLoadingReport
            });
        })
    }

    dataToOverviewChart = (data) => {
        return (
            {
                outsideLayer: [
                    {name: 'Capaian Visit', value: data.total_targeted_visit-data.total_achieved_visit > 0 ? data.total_achieved_visit : data.total_targeted_visit},
                    {name: 'Selisih Target Visit', value: data.total_targeted_visit-data.total_achieved_visit > 0 ? data.total_targeted_visit-data.total_achieved_visit : 0},
                    {name: 'Capaian Activation', value: data.total_targeted_activation-data.total_achieved_activation > 0 ? data.total_achieved_activation : data.total_targeted_activation},
                    {name: 'Selisih Target Activation', value: data.total_targeted_activation-data.total_achieved_activation > 0 ? data.total_targeted_activation-data.total_achieved_activation : 0},
                    {name: 'Capaian NOO', value: data.total_targeted_noo-data.total_achieved_noo > 0 ? data.total_achieved_noo : data.total_targeted_noo},
                    {name: 'Selisih Target NOO', value: data.total_targeted_noo-data.total_achieved_noo > 0 ? data.total_targeted_noo-data.total_achieved_noo : 0}
                ],
                insideLayer: [
                    { name: 'Targeted Visit', value: data.total_targeted_visit },
                    { name: 'Targeted Activation', value: data.total_targeted_activation },
                    { name: 'Targeted NOO', value: data.total_targeted_noo }
                ]
            }
        )
    };

    completionRatePerMetricsData = (data) => {
        let targetAmount = data.penugasan_amount;
        if (targetAmount <= 0) return null;
        return (
            [[
                {
                     donutData: [
                         {name: 'Capaian Visit', value: data.total_targeted_visit-data.total_achieved_visit > 0 ? data.total_achieved_visit : data.total_targeted_visit},
                         {name: 'Target Visit', value: data.total_targeted_visit-data.total_achieved_visit > 0 ? data.total_targeted_visit-data.total_achieved_visit : 0},
                     ],
                     actualData: [
                         { name: "Capaian Visit", value: data.total_achieved_visit },
                         { name: "Target Visit", value: data.total_targeted_visit },
                         { name: "Target visit terpenuhi", value: data.complete_target_visit },
                         { name: "Selisih Total Penugasan", value: targetAmount}
                     ]
                }
            ],
            [
                {
                    donutData: [
                        {name: 'Capaian Activation', value: data.total_targeted_activation-data.total_achieved_activation > 0 ? data.total_achieved_activation : data.total_targeted_activation},
                        {name: 'Target Activation', value: data.total_targeted_activation-data.total_achieved_activation > 0 ? data.total_targeted_activation-data.total_achieved_activation : 0},
                    ],
                    actualData: [
                        { name: "Capaian Activation", value: data.total_achieved_activation },
                        { name: "Target Activation", value: data.total_targeted_activation },
                        { name: "Target activation terpenuhi", value: data.complete_target_activation },
                        { name: "Selisih Total Penugasan", value: targetAmount}
                    ]
                }
            ],
            [
                {
                    donutData: [
                        {name: 'Capaian NOO', value: data.total_targeted_noo-data.total_achieved_noo > 0 ? data.total_achieved_noo : data.total_targeted_noo},
                        {name: 'Target NOO', value: data.total_targeted_noo-data.total_achieved_noo > 0 ? data.total_targeted_noo-data.total_achieved_noo : 0}
                    ],
                    actualData: [
                        { name: "Capaian NOO", value: data.total_achieved_noo },
                        { name: "Target NOO", value: data.total_targeted_noo },
                        { name: "Target NOO terpenuhi", value: data.complete_target_noo },
                        { name: "Selisih Total Penugasan", value: targetAmount}
                    ]
                }
            ]]
        )
    };

    getBestSalesperson = (data) => {
        if(data.length < 2) return null;
        const bestPerformance = Math.max.apply(Math, data.map(function (o) {
            return o.complete_target_visit + o.complete_target_activation + o.complete_target_noo;
        }));
        return data.find(function (o) {
            return (o.complete_target_visit + o.complete_target_activation + o.complete_target_noo) === bestPerformance;
        });
    };

    formatPeriodeToString(startDate, endDate){
        var tanggalMulai =  new Date(Date.parse(startDate)).toLocaleString('id-ID', {year: 'numeric', month: 'long', day: 'numeric'});
        var tanggalSelesai =  new Date(Date.parse(endDate)).toLocaleString('id-ID', {year: 'numeric', month: 'long', day: 'numeric',});return (tanggalMulai + " - " + tanggalSelesai)
    }

    individualBarXAxis(data){
        if (data.length === 0) return [];
        data.forEach(function (penugasan) {
            let tanggalMulai =  new Date(Date.parse(penugasan.start_date)).toLocaleString('id-ID', {month: 'numeric', day: 'numeric'});
            let tanggalSelesai =  new Date(Date.parse(penugasan.end_date)).toLocaleString('id-ID', {month: 'numeric', day: 'numeric'});
            penugasan.periode = tanggalMulai + "-" + tanggalSelesai
        });
        return data;
    }

    handleSearch = (e) => {this.setState({search: e.target.value})};

    handleSelectOrder = (orderDsc) => {this.setState({dsc: orderDsc})};

    handleSelectFilter = (selected) => {this.setState({selectedFilter: selected});};

    handleFilter = () => {this.setState({showFilter: !this.state.showFilter})};

    onPageChanged = data => {
        const { currentPage, pageLimit } = data;

        const offset = (currentPage - 1) * pageLimit;
        const currentSalesInPage = this.state.individual.slice(offset, offset + pageLimit);

        this.setState({ currentSalesInPage, currentPage });
    };

    showReportModalHandler = (salesperson) => {
        this.setState({
            showIndividualReportModal: true,
            namaSales: salesperson.nama,
            currentIndividualPie: this.dataToOverviewChart(salesperson),
            currentIndividualBar: this.individualBarXAxis(salesperson.Report),
            currentIndividualCompletionRate: this.completionRatePerMetricsData(salesperson)
        })
    };

    handleApplyCustomDate = () => {
        this.setState({isLoadingReport:true});
        this.fetchReportData();
    };

    handleApplyFilter = () => {
        this.setState({isLoadingReport:true, tabsKey:'individual'});
        this.fetchReportData();
    };

    handleAccordion = () => {if(this.state.isLoadingReport) this.fetchReportData()};

    closeReportModal = () => {
        this.setState({
            showIndividualReportModal: false,
            namaSales: "",
            currentIndividualPie: null,
            currentIndividualBar: null,
            currentIndividualCompletionRate: null
        });
    };

    renderCompletionCard(data) {
        if (data !== undefined) {
            return data.map((metrics, index) => {
                    return (<div className="col-sm-4" key={index}><PenugasanCompletionCard data={metrics} type={index}/></div>)
                }
            )
        }
    }

    renderReportModal(){
        return (
            <div className="text-center">
                <p style={{wordWrap:'break-word'}}>{this.state.namaSales}</p>
                <p style={{wordWrap:'break-word'}} className="text-center font-italic">{this.props.periode}</p>
                {this.state.currentIndividualPie !== null && this.state.currentIndividualBar !== null && this.state.currentIndividualCompletionRate !== null ?
                    <div>
                        <br/>
                        <div style={{height:'600px'}}>
                            <CustomTwoLevelPieChart insideLayerData={this.state.currentIndividualPie.insideLayer} outsideLayerData={this.state.currentIndividualPie.outsideLayer}
                                                    title="Kinerja Keseluruhan"
                            />
                        </div>
                        <br/><br/>
                        <div style={{height:`${this.state.currentIndividualBar.length*50}px`, minHeight:'300px'}}>
                            <CustomBarChart layout="vertical" data={this.state.currentIndividualBar} dataTypeX="number" dataKeyY="periode" dataTypeY="category"
                                            dataKeyStackedVisit="Visit" dataKeyStackedActivation="Activation" dataKeyStackedNoo="NOO"
                                            labelX="Jumlah Capaian" labelY="Periode Penugasan" title="Kinerja Per Penugasan"
                            />
                        </div>
                        <div>
                            <h5 className="text-center font-weight-bold">Capaian Per KPI</h5>
                            <div className="row">{this.renderCompletionCard(this.state.currentIndividualCompletionRate)}</div>
                        </div>
                    </div>
                    :
                    <div className="text-center">
                        <div className="row">
                            <img src={require('./empty-penugasan.png')} alt="" className="mx-auto mt-2 center"/>
                        </div>
                        <p className="text-secondary mt-5">{this.state.namaSales} tidak memiliki penugasan dalam periode yang diberikan.</p>
                    </div>
                }
            </div>
        )
    }

    renderSubmitButton(){
        return (
            this.state.customDate ?
                <Button onClick={this.handleApplyCustomDate} style={{ backgroundColor: '#FECF28', borderColor:'#FECF28', color:'#000000' }} type="submit" className="mt-4 mb-2 mh-auto btn btn-primary text-center">
                    APPLY
                </Button>
                :
                <span className="text-center">
                    <button disabled style={{ pointerEvents: 'none', backgroundColor: '#FECF28', borderColor:'#FECF28', color:'#000000' }} className="mt-4 mb-2 mh-auto btn btn-primary text-center">
                        APPLY
                    </button>
                </span>
        )
    }

    handleStartDateChange = date => {
        this.setState({tanggalAwal: date, customDate: true});
    };
    handleEndDateChange = date => {
        this.setState({tanggalAkhir: date, customDate: true});
    };

    renderSalesList() {
        return this.state.currentSalesInPage.map((salesperson, index) => {
                return (
                    <tr key={index} onClick={() => this.showReportModalHandler(salesperson)}>
                        <td>{salesperson.nama}</td>
                    </tr>
                )
            }
        )
    }

    render() {
        if (!this.props.loggedIn) {
            return <Redirect to="/login"/>
        }
        const queryString = require("query-string");
        const parsed = queryString.parse(window.location.search);
        if (Object.keys(parsed).length > 0) {
            return <Redirect to="/"/>
        }
        if (this.state.isLoadingActivePenugasan) {
            return (
                <div style={{
                    position: 'absolute', left: '50%', top: '50%',
                    transform: 'translate(-50%, -50%)'
                }}>
                    <h1 className="font-weight-bolder">Loading ...</h1>
                </div>
            )
        } else {
            return (
                <React.Fragment>
                    {
                        this.state.activePenugasan.team_member_amount > 0 ?
                            <div>
                                <CustomModal
                                    show={this.state.showIndividualReportModal} title="Laporan Kinerja" size="lg"
                                    onHide={this.closeReportModal} primaryButton="Oke" primaryAction={this.closeReportModal}
                                >
                                    {this.renderReportModal()}
                                </CustomModal>

                                <Container fluid style={{marginTop:'-3rem',paddingBottom:'8em'}}>
                                    <Card className="shadow-lg my-lg-5 bg-white rounded">
                                        <Card.Header>
                                            <div className="fa-pull-left my-n3">
                                                <h2 className="font-weight-bold mb-lg-n4">
                                                    Halo, {this.props.namaUser} !
                                                </h2>
                                                <h4 className="font-weight-bold text-secondary mb-xl-5">
                                                    Home
                                                </h4>
                                            </div>
                                            <br/><br/>
                                        </Card.Header>
                                        <Card.Body>
                                            {
                                                this.state.activePenugasan.start_date !== null ?
                                                    <div>
                                                        <p className="font-weight-bold text-center">
                                                            Penugasan aktif yang sedang berjalan:
                                                        </p>
                                                        <PenugasanTimActiveCard data={this.state.activePenugasan}/>
                                                    </div>
                                                    :
                                                    <div className='text-center'>
                                                        <p className="text-secondary mx-5">Tim kamu belum memiliki penugasan aktif yang sedang berjalan.</p>
                                                        <p className="text-secondary mb-3 font-weight-bold">
                                                            <u><a style={{color:'#FECF28'}} href="/target-penugasan/view-all">Buat penugasan</a></u>
                                                        </p>
                                                    </div>
                                            }
                                        </Card.Body>
                                    </Card>
                                    <Accordion>
                                        <Card>
                                            <Card.Header className="text-center">
                                                <Accordion.Toggle as={Button} variant="link" eventKey="0" onClick={this.handleAccordion}>
                                                    Lihat laporan
                                                </Accordion.Toggle>
                                            </Card.Header>
                                            <Accordion.Collapse eventKey="0">
                                                <Card.Body>
                                                    {this.state.isLoadingReport ?
                                                        <div style={{
                                                            position: 'absolute', left: '50%', top: '50%',
                                                            transform: 'translate(-50%, -50%)'
                                                        }}>
                                                            <h1 className="font-weight-bolder m-xl-5">Loading ...</h1>
                                                        </div>
                                                        :
                                                        <div className='mt-5'>
                                                            <h2 className="font-weight-bold mt-xl-1">Rangkuman Kinerja</h2>
                                                            <div className="text-sm-left text-lg-center">
                                                                <h4 className="font-weight-bold mb-3">Periode</h4>
                                                                <DatePicker
                                                                    className="dateWrap" name="tanggalAwal" onChange={this.handleStartDateChange}
                                                                    value={this.state.tanggalAwal} clearIcon={null} maxDate={this.state.tanggalAkhir}
                                                                    locale="id-ID" calendarClassName="calendarWrap"
                                                                />
                                                                <p className="mx-auto">sampai</p>
                                                                <DatePicker
                                                                    className="dateWrap" name="tanggalAkhir" onChange={this.handleEndDateChange}
                                                                    value={this.state.tanggalAkhir} clearIcon={null} minDate={this.state.tanggalAwal}
                                                                    locale="id-ID" calendarClassName="calendarWrap"
                                                                />
                                                                <div className="mb-5">{this.renderSubmitButton()}</div>
                                                            </div>
                                                            <Tabs defaultActiveKey={this.state.tabsKey} style = {{
                                                                display: "flex",
                                                                justifyContent: "center",
                                                                alignItems: "center",
                                                                flexWrap: "wrap",
                                                                padding: "10px"
                                                            }} id='report'>
                                                                <Tab eventKey="overview" title="Overview" className="text-center">
                                                                    {
                                                                        !(this.state.overviewPie.total_targeted_visit === 0 &&
                                                                            this.state.overviewPie.total_targeted_activation === 0 &&
                                                                            this.state.overviewPie.total_targeted_noo === 0) ?
                                                                            <LeadSalesReport
                                                                                overviewPie={this.dataToOverviewChart(this.state.overviewPie)} overviewBar={this.state.overviewBar}
                                                                                overviewBestSales={
                                                                                    this.state.overviewBestSales !== null ?
                                                                                        this.completionRatePerMetricsData(this.state.overviewBestSales) : null
                                                                                }
                                                                                bestSalesName={
                                                                                    this.state.overviewBestSales !== null ?
                                                                                        this.state.overviewBestSales.nama : null
                                                                                }
                                                                                periode={this.state.periode}
                                                                            />
                                                                            :
                                                                            <div className="text-center">
                                                                                <h2 className="font-weight-bold mt-xl-1">Tidak Ada Data</h2>
                                                                                <div className="row">
                                                                                    <img src={require('./empty-period.png')} alt="" className="mx-auto mt-2 center"/>
                                                                                </div>
                                                                                <p className="text-secondary mt-5 font-weight-bold">
                                                                                    <u>Data tidak tersedia.</u>
                                                                                </p>
                                                                            </div>
                                                                    }
                                                                </Tab>
                                                                <Tab eventKey="individual" title="Individual">
                                                                    <OptionButton className="fa-pull-left" handleChange={this.handleFilter}/>
                                                                    {this.state.showFilter ?
                                                                        <Filter data={this.state} handleChange={this.handleSelectFilter} handleSort={this.handleSelectOrder}
                                                                                handleSearch={this.handleSearch} handleApply={this.handleApplyFilter}/>
                                                                        :
                                                                        null
                                                                    }
                                                                    <div className="mt-2 text-center">
                                                                        {
                                                                            !(this.state.overviewPie.total_targeted_visit === 0 &&
                                                                                this.state.overviewPie.total_targeted_activation === 0 &&
                                                                                this.state.overviewPie.total_targeted_noo === 0) ?
                                                                                <div>
                                                                                    <Table hover responsive style={{wordWrap:'break-word'}}>
                                                                                        <thead>
                                                                                        <tr>
                                                                                            <th>Nama Salesperson</th>
                                                                                        </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                        {this.renderSalesList()}
                                                                                        </tbody>
                                                                                    </Table>
                                                                                    <div className="d-flex flex-row py-4 fa-pull-right">
                                                                                        <CustomPagination
                                                                                            currentPage={this.state.currentPage}
                                                                                            totalRecords={this.state.individual.length}
                                                                                            pageLimit={ITEM_PER_PAGE}
                                                                                            onPageChanged={this.onPageChanged}
                                                                                        />
                                                                                    </div>
                                                                                </div>
                                                                                :
                                                                                <div className="text-center">
                                                                                    <h2 className="font-weight-bold mt-xl-1">Tidak Ada Data</h2>
                                                                                    <div className="row">
                                                                                        <img src={require('./empty-period.png')} alt="" className="mx-auto mt-2 center"/>
                                                                                    </div>
                                                                                    <p className="text-secondary mt-5 font-weight-bold">
                                                                                        <u>Data tidak tersedia.</u>
                                                                                    </p>
                                                                                </div>
                                                                        }
                                                                    </div>
                                                                </Tab>
                                                            </Tabs>
                                                        </div>
                                                    }
                                                </Card.Body>
                                            </Accordion.Collapse>
                                        </Card>
                                    </Accordion>
                                </Container>
                            </div>
                            :
                            <div className="text-center">
                                <h2 className="font-weight-bold mt-xl-1">Tim Kosong</h2>
                                <div className="row">
                                    <img src={require('./empty-team.png')} alt="" className="mx-auto mt-2 center"/>
                                </div>
                                <p className="text-secondary mt-5">Kamu tidak memiliki salesperson sebagai anggota tim.</p>
                                <p className="text-secondary mb-3 font-weight-bold">
                                    <u><a style={{color:'#FECF28'}} href="/salesperson-control/">Tambah salesperson ke dalam tim</a></u>
                                </p>
                            </div>

                    }
                    <div style={{textAlign: 'center'}}>
                        <LogoutButton onLogout={this.props.onLogout}/>
                    </div>
                </React.Fragment>
            );
        }
    }
}

export default HomeLeadSales;