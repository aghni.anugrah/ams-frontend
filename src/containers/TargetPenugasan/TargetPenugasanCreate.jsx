import React, { Component } from "react";
import { Redirect } from 'react-router-dom'
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import ModalConfirmationSave from "../../components/ModalTargetPenugasan/ModalConfirmationSave";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Button from "react-bootstrap/Button";
import Tooltip from "react-bootstrap/Tooltip";
import Alert from "react-bootstrap/Alert";

class TargetPenugasanCreate extends Component {
    state = {
        id_wilayah_penugasan: "",
        tanggal_mulai: new Date(),
        tanggal_selesai: new Date(),
        kuota_visit: "",
        kuota_activation: "",
        kuota_noo: "",
        successCreate: false,
        showConfirmationModal: false,
        showCreateAlert: false,
        date: null,
    };

    componentDidMount() {
        this.state.successCreate ?
            window.onbeforeunload = undefined
            :
            window.onbeforeunload = () => true
    }

    componentWillUnmount() {
        window.onbeforeunload = null
    }

    handleNumberChange = e => {
        this.setState({ [e.target.name]: Number(e.target.value) });
    };


    handleDateChangeAwal = e => {
        // this.setState({[e.target.name]: Date(e.target.value)});
        this.setState({
            [e.target.name]: new Date(Date.parse(e.target.value)).toISOString().split('.')[0] + "+07:00"
        });
    };

    handleDateChangeAkhir = e => {
        // this.setState({[e.target.name]: Date(e.target.value)});
        this.setState({
            [e.target.name]: new Date(Date.parse(e.target.value)).toISOString().split('.')[0] + "+07:00"
        });
    };

    formIsComplete() {
        return !(this.state.id_wilayah_penugasan === "" || this.state.kuota_visit === "" ||
            this.state.kuota_activation === "" || this.state.kuota_noo === "")
    }

    showConfirmationFormModal = (e) => {
        e.preventDefault();
        this.setState({
            showConfirmationSubmitModal: true
        });
    }

    closeConfirmationFormModal = () => {
        this.setState({
            showConfirmationSubmitModal: false,
        });
    }

    closeConfirmationSucces = () => {
        this.setState({
            showConfirmationSubmitSuccess: false,
        });
    }

    handleSubmit = (e) => {
        e.preventDefault()
        console.log(this.state)
        const token = "Bearer " + localStorage.getItem("token");
        fetch("http://localhost:8080/target-penugasan/create", {
            method: "POST",
            headers: {
                "Authorization": token
            },
            body: JSON.stringify(this.state)

        }).then(res => {
            console.log(res.status)
            if (res.status === 200) {
                this.setState({
                    successCreate: true,
                    showCreateAlert: true,
                })
            }
        })
    };

    renderConfirmation() {
        return (
            <div className="text-center">
                <p>Apa Anda Yakin ingin menyimpan data ini? </p>
            </div>

        )
    }

    renderSuccessSave(){
        return (
            <div className="text-center">
                <p>Penugasan berhasil disimpan </p>
            </div>

        )
    }

    renderSubmitButton() {
        return (
            this.formIsComplete() ?
                <div className="text-center">
                    <Button onClick={this.showConfirmationFormModal} type="submit" style={{ backgroundColor: '#FECF28', borderColor: '#FECF28', color: '#000000' }}>SIMPAN</Button>
                </div>
                :
                <OverlayTrigger
                    placement="right"
                    delay={{ show: 250, hide: 400 }}
                    overlay={<Tooltip id="tooltip-disabled"> Data belum lengkap!</Tooltip>}
                >
                    <div className="text-center">
                        <Button disabled style={{ pointerEvents: 'none', backgroundColor: '#FECF28', borderColor: '#FECF28', color: '#000000' }}> SIMPAN </Button>
                    </div>
                </OverlayTrigger>
        )
    }



    render() {
        if (!this.props.loggedIn) {
            return <Redirect to="/login" />
        }
        if (this.state.successCreate) {
            return <Redirect to={{
                pathname: "/target-penugasan/view-all",
                state: {
                    successCreate: true,
                }
            }} />

        }

        return (
            <React.Fragment>
                <div className="col-lg-12 col-xl-10 mx-auto">
                    <div className="card card-signin flex-row my-5">
                        <div className="card-body">
                            <ModalConfirmationSave
                                show={this.state.showConfirmationSubmitModal}
                                title="Konfirmasi"
                                onHide={this.closeConfirmationFormModal}
                                primaryAction={this.handleSubmit}
                                secondaryAction={this.closeConfirmationFormModal}
                            >
                                {this.renderConfirmation()}
                            </ModalConfirmationSave>

                            {/*<ModalSuccessSave*/}
                            {/*    show={this.state.showConfirmationSubmitSuccess}*/}
                            {/*    onHide={this.closeConfirmationSucces}*/}
                            {/*    >*/}
                            {/*    {this.renderSuccessSave()}*/}
                            {/*</ModalSuccessSave>*/}


                            <h5 className="card-title text-center"><b>Tambah Target Penugasan</b></h5>
                            <Form onSubmit={this.handleSubmit}>
                                <Form.Group controlId="formWilayah">
                                    <Form.Label>Wilayah</Form.Label>
                                    <Form.Control
                                        as="select"
                                        onChange={this.handleNumberChange}
                                        name="id_wilayah_penugasan">

                                        <option value="none" disabled hidden> Pilih Lokasi</option>
                                        <option value="1">Jakarta Timur</option>
                                        <option value="2">Jakarta Barat</option>
                                        <option value="3">Jakarta Selatan</option>
                                        <option value="4">Jakarta Utara</option>
                                        <option value="5">Jakarta Pusat</option>
                                        <option value="6">Depok</option>
                                        <option value="7">Bekasi</option>
                                    </Form.Control>
                                </Form.Group>

                                <Form.Row>
                                    <Form.Group as={Col} controlId="formPeriodeAwal">
                                        <Form.Label>Periode Awal</Form.Label>
                                        <Form.Control
                                            required
                                            name="tanggal_mulai"
                                            type="date"
                                            onChange={this.handleDateChangeAwal}
                                            // minDate={new Date(new Date().setDate(new Date().getDate() - 1 ))}
                                        />
                                    </Form.Group>

                                    <Form.Group as={Col} controlId="formPeriodeAkhir">
                                        <Form.Label>Periode Akhir</Form.Label>
                                        <Form.Control
                                            required
                                            name="tanggal_selesai"
                                            type="date"
                                            onChange={this.handleDateChangeAkhir}
                                        />
                                    </Form.Group>

                                </Form.Row>
                                <Form.Group controlId="formKuotaVisit">
                                    <Form.Label>Kuota Visit</Form.Label>
                                    <Form.Control
                                        required
                                        type="number"
                                        name="kuota_visit"
                                        onChange={this.handleNumberChange}
                                        min = "0"
                                        max = "100"
                                    />
                                </Form.Group>

                                <Form.Group controlId="formKuotaAktivasi">
                                    <Form.Label>Kuota Aktivasi</Form.Label>
                                    <Form.Control
                                        required
                                        type="number"
                                        name="kuota_activation"
                                        onChange={this.handleNumberChange}
                                        min = "0"
                                        max = "100"
                                    />
                                </Form.Group>

                                <Form.Group controlId="formKuotaNOO">
                                    <Form.Label>Kuota NOO</Form.Label>
                                    <Form.Control
                                        required
                                        type="number"
                                        name="kuota_noo"
                                        onChange={this.handleNumberChange}
                                        min = "0"
                                        max = "100"
                                    />
                                </Form.Group>
                                {this.renderSubmitButton()}
                            </Form>

                        </div>
                    </div>
                </div>
                {this.state.successCreate &&
                    <Alert
                        variant="success"
                        onClose={() =>
                            this.setState({
                                showSuccessAlert: false
                            })}
                        dismissible
                        style={{ marginTop: "10px", margin: "20px" }}
                    >
                        <p>Penugasan berhasil dibuat</p>
                    </Alert>
                }


                

            </React.Fragment>
        );
    }

}
export default TargetPenugasanCreate;
