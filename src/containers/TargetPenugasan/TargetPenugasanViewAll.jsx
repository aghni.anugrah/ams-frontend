import React, { Component } from "react";
import { Redirect } from 'react-router-dom';
import {faPlusSquare} from "@fortawesome/free-solid-svg-icons";
import { Card, Form} from 'react-bootstrap';
import ListPenugasanTable from "../../components/TabelTargetPenugasan/ListPenugasanTable";
import ModalDeletePenugasan from "../../components/ModalTargetPenugasan/ModalDeletePenugasan";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Alert from "react-bootstrap/Alert";
import OptionButton from "../../components/Filter/OptionButton";
import Filter from "../../components/Filter/Filter";
import ModalUpdatePenugasan from "../../components/ModalTargetPenugasan/ModalUpdatePenugasan";
import CustomPagination from "../../components/Pagination/CustomPagination";
import './PenugasanViewAll.css';
import classes from "../SalespersonControl/SalespersonControl.module.css";
import CustomModal from "../../components/Modal/CustomModal";

const ITEM_PER_PAGE = 5;
class TargetPenugasanViewAll extends Component {
    state = {
        targetpenugasan: [],
        currentPenugasan: [],
        currentPage: 1,
        showDetailModal: false,
        showConfirmationModal: false,
        showUpdateModal: false,
        showSuccessAlert: false,
        showUpdateAlert: false,
        successUpdate: false,
        successDelete: false,
        id_wilayah_penugasan: "",
        id_salesperson: "",
        id: "",
        nama: "",
        wilayah: "",
        tanggal_mulai: "",
        tanggal_selesai: "",
        kuota_visit: "",
        kuota_activation: "",
        kuota_noo: "",
        showFilter: false,
        search: "",
        dsc: false,
        opt: [
            ["Nama Sales", "nama-sales"], ["Kuota Visit", "kuota-visit"]
        ],
        selectedFilter: ["Nama Sales", "nama-sales"],
    };

    controller = new AbortController();

    componentWillUnmount() {
        this.controller.abort()
    }

    handleSearch = (e) => {
        this.setState({ search: e.target.value })
    };

    handleSelectOrder = (orderDsc) => {
        this.setState({ dsc: orderDsc })
    };

    handleSelectFilter = (selected) => {
        this.setState({ selectedFilter: selected });
    };

    handleApply = () => {
        this.fetchData();
        this.setState({ currentPage: 1 })
    };

    handleFilter = () => {
        this.setState({ showFilter: !this.state.showFilter })
    };

    componentDidMount() {
        this.fetchData()
    }

    fetchData = () => {
        let search = this.state.search;
        if (search !== "") {
            search = "&search=" + search
        }

        let sort;
        if (this.state.selectedFilter.length > 0) {
            sort = "&sort-by=" + this.state.selectedFilter[1]
        }

        let dsc = "?dsc=" + this.state.dsc;

        const URL = "http://localhost:8080";
        const token = "Bearer " + localStorage.getItem("token");
        fetch(URL + "/target-penugasan/view-all" + dsc + sort + search, {
            method: "GET",
            signal: this.controller.signal,
            headers: {
                "Authorization": token,
            }
        }).then(response => {
            return response.json()
        }).then((data) => {
            const fetchedData = [];
            for (let key in data.target_penugasan_with_name_and_wilayah) {
                fetchedData.push({
                    ...data.target_penugasan_with_name_and_wilayah[key]
                });
            }
            this.setState({
                targetpenugasan: fetchedData,
                currentPenugasan: fetchedData.slice(0, ITEM_PER_PAGE)
            });
            if (this.state.targetpenugasan.length > 0) {
                this.setState({
                    wilayah: this.state.targetpenugasan[0].wilayah
                });
            }
        })
    };

    onPageChanged = data => {
        const { currentPage, pageLimit } = data;

        const offset = (currentPage - 1) * pageLimit;
        const currentPenugasan = this.state.targetpenugasan.slice(offset, offset + pageLimit);

        this.setState({ currentPenugasan, currentPage });
    };


    closeDetailModal = () => {
        this.setState({
            showDetailModal: false,
        });
    };

    closeUpdatelModal = () => {
        this.setState({
            showUpdateModal: false,
        });
    };

    closeDeleteModal = () => {
        if (this.state.showConfirmationModal) {
            this.setState({
                showConfirmationModal: false
            })
        }
    };

    closeConfirmationModal = () => {
        if (this.state.showConfirmationModal) {
            this.setState({
                showConfirmationModal: false
            })
        }
    };

    showConfirmationHandler = (targetpenugasan) => {
        if (!this.state.showConfirmationModal) {
            this.setState({
                id_salesperson: targetpenugasan.id_salesperson,
                showConfirmationModal: true,
            })
        }
    };

    showUpdateHandler = (targetpenugasanUpdate) => {
        const detailTarget = targetpenugasanUpdate.target_penugasan
        this.setState({
            showUpdateModal: true,
            wilayah: targetpenugasanUpdate.wilayah,
            kuota_visit: detailTarget.kuota_visit,
            kuota_activation: detailTarget.kuota_activation,
            kuota_noo: detailTarget.kuota_noo,
            id_salesperson: detailTarget.id_salesperson
        })
    };

    handleNumberChange = e => {
        this.setState({ [e.target.name]: Number(e.target.value) });
    };

    showDetailHandler = (targetpenugasan) => {
        const detailTarget = targetpenugasan.target_penugasan
        this.setState({
            showDetailModal: true,
            wilayah: targetpenugasan.wilayah,
            tanggal_mulai: new Date(Date.parse(detailTarget.tanggal_mulai))
                .toLocaleString('id-ID', {
                    weekday: 'long',
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric'
                }),
            tanggal_selesai: new Date(Date.parse(detailTarget.tanggal_selesai))
                .toLocaleString('id-ID', {
                    weekday: 'long',
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric'
                }),
            kuota_visit: detailTarget.kuota_visit,
            kuota_activation: detailTarget.kuota_activation,
            kuota_noo: detailTarget.kuota_noo,
        })
    };

    showWilayah = (targetpenugasanWilayah) => {
        this.setState({
            wilayah: targetpenugasanWilayah.wilayah,
        })
    };

    deleteToEndpoint = (e) => {
        e.preventDefault();
        const idSalesperson = this.state.id_salesperson;
        console.log(idSalesperson)
        const token = "Bearer " + localStorage.getItem("token");
        fetch("http://localhost:8080/target-penugasan/delete/" + idSalesperson, {
            method: "DELETE",
            headers: {
                "Authorization": token,
            }
        }).then(response => {
            console.log(response.status);
            if (response.status === 200) {
                this.componentDidMount();
                this.setState({
                    successDelete: true,
                    showSuccessAlert: true,
                    showConfirmationModal: false,
                })
            }
        })
    };

    updateToEndPoint = (e) => {
        e.preventDefault();
        const idSalesperson = this.state.id_salesperson;
        console.log(idSalesperson);
        const token = "Bearer " + localStorage.getItem("token");
        fetch("http://localhost:8080/target-penugasan/update/" + idSalesperson, {
            method: "PUT",
            headers: {
                "Authorization": token,
            },
            body: JSON.stringify(this.state)
        }).then(response => {
            console.log(response.status);
            if (response.status === 200) {
                this.componentDidMount();
                this.setState({
                    showUpdateModal: false,
                    successCreate: true,
                    showUpdateAlert: true,
                })
            }
        })
    };

    renderWilayah() {
        return (
            <h3 id="wilayahPenugasan">Wilayah: {this.state.wilayah}</h3>
        )
    }

    renderDetail() {
        return (
            <div>
                <p >Wilayah: {this.state.wilayah}</p>
                <p >Periode Awal: {this.state.tanggal_mulai}</p>
                <p >Periode Akhir: {this.state.tanggal_selesai}</p>
                <p >Kuota Visit: {this.state.kuota_visit}</p>
                <p >Kuota Aktivasi: {this.state.kuota_activation}</p>
                <p >Kuota NOO: {this.state.kuota_noo}</p>
            </div>
        )
    }

    renderDelete() {
        return (
            <div className="pop-up">
                <p>Apa anda yakin ingin menghapus penugasan ini?</p>
            </div>
        )
    }

    countTotalKuota() {
        var total_kuota = 0;
        this.state.targetpenugasan.map((penugasan) => total_kuota = total_kuota + penugasan.target_penugasan.kuota_visit);
        return total_kuota;
    }

    renderUpdate() {
        return (
            <div className="container">
                <h5 className="card-title text-center"><b>Ubah Target Penugasan</b></h5>
                <Form>
                    <Form.Group controlId="formKuotaVisit">
                        <Form.Label>Kuota Visit</Form.Label>
                        <Form.Control
                            required
                            type="number"
                            name="kuota_visit"
                            onChange={this.handleNumberChange}
                            value={this.state.kuota_visit}
                        />
                    </Form.Group>

                    <Form.Group controlId="formKuotaAktivasi">
                        <Form.Label>Kuota Aktivasi</Form.Label>
                        <Form.Control
                            required
                            type="number"
                            name="kuota_activation"
                            onChange={this.handleNumberChange}
                            value={this.state.kuota_activation}
                        />
                    </Form.Group>

                    <Form.Group controlId="formKuotaNOO">
                        <Form.Label>Kuota NOO</Form.Label>
                        <Form.Control
                            required
                            type="number"
                            name="kuota_noo"
                            onChange={this.handleNumberChange}
                            value={this.state.kuota_noo}
                        />
                    </Form.Group>
                </Form>
            </div>
        )
    };

    render() {
        if (!this.props.loggedIn) {
            return <Redirect to="/login" />
        }
        if (this.state.successUpdate) {
            return <Redirect to={{
                pathname: "/target-penugasan/view-all",
                state: {
                    successUpdate: true,
                }
            }} />
        }

        return (
            <React.Fragment>

                {this.state.showDetailModal &&
                <CustomModal
                    show={this.state.showDetailModal}
                    onHide={this.closeDetailModal}
                    title="Detail Penugasan"
                    primaryButton="Oke"
                    primaryAction={this.closeDetailModal}>
                    {this.renderDetail()}
                </CustomModal>
                }

                <ModalDeletePenugasan
                    show={this.state.showConfirmationModal}
                    onHide={this.closeDeleteModal}
                    action={this.deleteToEndpoint}
                >
                    {this.renderDelete()}
                </ModalDeletePenugasan>

                <ModalUpdatePenugasan
                    show={this.state.showUpdateModal}
                    onHide={this.closeUpdatelModal}
                    action={this.updateToEndPoint}
                >
                    {this.renderUpdate()}
                </ModalUpdatePenugasan>

                <div className="text-center">
                    <h2 style={{ fontWeight: "bold"}}>Daftar Penugasan</h2>
                </div>
                <Card>
                    
                    <Card.Header id= "card-header-penugasan" > {this.renderWilayah()}
                        {this.state.targetpenugasan.length === 0 &&
                        <a href="/target-penugasan/create">
                            <FontAwesomeIcon
                                id="create-icon"
                                icon={faPlusSquare}
                                style={{
                                    color:'#000000',
                                }}
                                size="2x"
                                className="fa-pull-right"
                            />
                        </a>
                        }
                    </Card.Header>
                    
                    <div className="text-center">
                    {this.state.showSuccessAlert &&
                        <Alert id="penugasanAlertPopUp"
                            variant="success"
                            onClose={() =>
                                this.setState({
                                    showSuccessAlert: false
                                })}
                            dismissible
                            style={{ marginTop: "10px", margin: "20px" }}
                        >
                            <p id="penugasanAlert">Penugasan berhasil dihapus!</p>
                        </Alert>
                    }


                    {this.state.showUpdateAlert &&
                        <Alert id="penugasanAlertPopUp"
                            variant="success"
                            onClose={() =>
                                this.setState({
                                    showUpdateAlert: false
                                })}
                            dismissible
                            style={{ marginTop: "10px", margin: "20px" }}
                        >

                            <p id="penugasanAlert">Penugasan berhasil diubah!</p>
                        </Alert>
                    }
                    </div>

                    <div className="button-group">
                        <div className={classes.filter}>
                        {this.state.targetpenugasan.length > 0 &&
                        <OptionButton handleChange={this.handleFilter} />
                        }
                        </div>
                        {this.state.showFilter ?
                            <React.Fragment>
                            <Filter data={this.state} handleChange={this.handleSelectFilter} handleSort={this.handleSelectOrder}
                            handleSearch={this.handleSearch} handleApply={this.handleApply} />
                            </React.Fragment>
                            :
                            <React.Fragment></React.Fragment>
                            }
                    </div>

                    <Card.Body>
                        <p>Total Kuota Visit:{this.countTotalKuota()} </p>
                        <div className="text-center">
                            <table className="table table-hover">
                            <thead className="Header">
                            <tr>
                                <th scope="col" className="font-weight-lighter">Nama Sales</th>
                                <th scope="col" className="font-weight-lighter">Kuota Visit</th>
                                <th scope="col"/>
                                <th scope="col"/>
                            </tr>
                            </thead>

                                <ListPenugasanTable
                                    data={this.state.currentPenugasan}
                                    detail={this.showDetailHandler}
                                    delete={this.showConfirmationHandler}
                                    update={this.showUpdateHandler}
                                    wilayahHeader={this.showWilayah} />
                            </table>

                        <div className="d-flex flex-row py-4 fa-pull-right">
                            <CustomPagination
                                currentPage={this.state.currentPage}
                                totalRecords={this.state.targetpenugasan.length}
                                pageLimit={ITEM_PER_PAGE}
                                onPageChanged={this.onPageChanged}
                            />
                        </div>
                        </div>
                    </Card.Body>
                </Card>
            </React.Fragment>
        );
    }
}
export default TargetPenugasanViewAll;