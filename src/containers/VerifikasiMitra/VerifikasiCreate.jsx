import React, {Component} from "react";
import {Redirect} from "react-router-dom";
import Form from "react-bootstrap/Form";
import CustomModal from "../../components/Modal/CustomModal";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Button from "react-bootstrap/Button";
import Tooltip from "react-bootstrap/Tooltip";
import InputGroup from "react-bootstrap/InputGroup";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";
import {faInfoCircle, faCircle, faPlusSquare} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import classes from "./Verifikasi.module.css";
import Expire from "../../components/Expire/Expire";
import Alert from "react-bootstrap/Alert";


class VerifikasiCreate extends Component {
    state = {
        id: "",
        alasan: "",
        successCreate: false,
        showConfirmationSubmitModal: false
    };

    controller = new AbortController();

    componentDidMount() {
        console.log(this.props.id)
        this.state.successCreate ?
            window.onbeforeunload = undefined
            :
            window.onbeforeunload = () => true
    }

    componentWillUnmount() {
        window.onbeforeunload = null
        this.controller.abort()
    }

    handleChange = e => {
        this.setState({[e.target.name]: e.target.value});
    };


    formIsComplete(){
        return !(this.state.alasan === "")
    }

    showConfirmationFormModal = (e) => {
        e.preventDefault();
        this.setState({
            showConfirmationSubmitModal: true
        });
    };

    closeConfirmationFormModal = () => {
        this.setState({
            showConfirmationSubmitModal: false
        });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const idWarung = this.props.id;
        const token = "Bearer " + localStorage.getItem("token");
        fetch("http://localhost:8080/validasi/create/" + idWarung, {
            signal: this.controller.signal,
            method: "POST",
            headers: {
                "Authorization" : token
            },
            body: JSON.stringify(this.state),
        }).then(res => {
            if (res.status === 200) {
                this.setState({successCreate: true});
            } else if (res.status === 400) {
                this.closeConfirmationFormModal();
            }
        })
    };

    renderSubmitButton(){
        return (
            this.formIsComplete() ?
                <div className="mt-4 mb-2 mh-auto text-center">
                    <Button style={{ backgroundColor: '#FFFFFF', borderColor:'#FECF28', color:'#FECF28', margin:'10px'}} className="btn btn-primary">
                        <a href="/validasi/view-all" style={{color:'#FECF28'}}>
                            BATAL
                        </a>
                    </Button>

                    <Button onClick={this.showConfirmationFormModal} style={{ backgroundColor: '#FECF28', borderColor:'#FECF28', color:'#000000' }} type="submit" className="btn btn-primary">
                        SIMPAN
                    </Button>
                </div>
                :
                <div className="mt-4 mb-2 mh-auto text-center">
                    <Button style={{ backgroundColor: '#FFFFFF', borderColor:'#FECF28', color:'#FECF28', margin:'10px'}} className="btn btn-primary">
                        <a href="/validasi/view-all" style={{color:'#FECF28'}}>
                            BATAL
                        </a>
                    </Button>
                    <OverlayTrigger
                        placement="right"
                        delay={{ show: 250, hide: 400 }}
                        overlay={<Tooltip id="tooltip-disabled">Data belum lengkap!</Tooltip>}
                    >
                <span className="text-center">
                <button disabled style={{ pointerEvents: 'none', backgroundColor: '#FECF28', borderColor:'#FECF28', color:'#000000' }} className="btn btn-primary">
                    SIMPAN
                </button>
                </span>
                    </OverlayTrigger>
                </div>
        )
    }

    renderFormSummary() {
        return (
            <div>
                <p className="text-center">Alasan Penolakan: {this.state.alasan}</p>
                <p className="text-center">Yakin ingin menyimpan data?</p>
            </div>
        )
    }

    renderTooltipForm(message){
        return (
            <OverlayTrigger
                placement="right"
                delay={{ show: 250, hide: 400 }}
                overlay={
                    <Tooltip id="tooltip-disabled">
                        {message}
                    </Tooltip>
                }
            >
                <span style={{paddingLeft:5}}>
                    <FontAwesomeIcon
                        icon={faInfoCircle}
                        mask={faCircle}
                        size="lg"
                        transform="shrink-3"
                        style={{ color: '#FF9802' }}
                    />
                </span>
            </OverlayTrigger>
        )
    }

    render() {
        if (!this.props.loggedIn) {
            return <Redirect to="/login"/>
        }
        if (this.state.successCreate) {
            return <Redirect to={{
                pathname: "/validasi/view-all",
                state: {
                    successCreate: true
                }
            }}/>
        }

        return (
            <React.Fragment>
                <Container fluid style={{marginTop:'-3rem',paddingBottom:'8em'}}>
                    <h2 className="text-center font-weight-bold">
                        Alasan Penolakan Data Mitra
                    </h2>
                    <Card className="shadow-lg mb-5 bg-white rounded">
                        <Card.Body>

                            <CustomModal
                                show={this.state.showConfirmationSubmitModal}
                                title="Ringkasan Data"
                                onHide={this.closeConfirmationFormModal}
                                primaryButton="Ya, simpan"
                                primaryAction={this.handleSubmit}
                                secondaryButton="Tidak, kembali"
                                secondaryAction={this.closeConfirmationFormModal}
                                type="confirm"
                            >
                                {this.renderFormSummary()}
                            </CustomModal>
                            <Form onSubmit={this.handleSubmit}>
                                <Form.Group controlId="FGAlasanPenolakan">
                                    <Form.Label>Alasan</Form.Label>
                                    {this.renderTooltipForm("Cantumkan alasan penolakan data mitra secara singkat dan mudah dipahami")}
                                    <Form.Control
                                        as="textarea"
                                        rows="10"
                                        required
                                        type="text"
                                        onChange={this.handleChange}
                                        name="alasan"
                                        placeholder="Alasan penolakan data mitra..."
                                    />
                                </Form.Group>
                                <div className="form-row text-center">
                                    <div className="col-12">
                                        {this.renderSubmitButton()}
                                    </div>
                                </div>
                            </Form>
                        </Card.Body>
                    </Card>
                </Container>
            </React.Fragment>
        );
    }
}

export default VerifikasiCreate;

