import React, {Component} from "react";
import VerifikasiTable from "../../components/Table/VerifikasiTable";
import CustomModal from "../../components/Modal/CustomModal";
import {Redirect, withRouter} from "react-router-dom";
import Card from "react-bootstrap/Card";
import CustomPagination from "../../components/Pagination/CustomPagination";
import Expire from "../../components/Expire/Expire";
import Alert from "react-bootstrap/Alert";
import OptionButton from "../../components/Filter/OptionButton";
import Filter from "../../components/Filter/Filter";
import Container from "react-bootstrap/Container";
import StatusVerifikasi from "../../components/StatusTableWarung/StatusVerifikasi";

const ITEM_PER_PAGE = 5;

class VerifikasiViewAll extends Component {
    state = {
        mitra: [],
        showModal: false,
        showDetailModal: false,
        id: "",
        mitra_name: "",
        warung_name: "",
        ktp_id: "",
        birth_date: "",
        phone_number: "",
        business_type: "",
        village_name: "",
        address: "",
        rt_rw: "",
        point_land: "",
        alasan: "",
        createAlasan: false,
        showAcceptedAlert: false,
        successAccepted: false,
        showRejectedAlert: this.props.location.state !== undefined,
        showFilter: false,
        search: "",
        dsc: true,
        opt: [
            ["Lokasi", "lokasi"],
            ["Nama", "nama"]
        ],
        selectedFilter: ["Nama", "nama"],
        isLoading: true,
        currentVerifikasi: [],
        currentPage: 1
    };

    controller = new AbortController();

    handleSearch = (e) => {
        this.setState({search: e.target.value})
    };

    handleSelectOrder = (orderDsc) => {
        this.setState({dsc: orderDsc})
    };

    handleSelectFilter = (selected) => {
        this.setState({selectedFilter: selected});
    };

    handleApply = () => {
        this.fetchData();
        this.setState({currentPage: 1})
    };

    handleFilter = () => {
        this.setState({showFilter: !this.state.showFilter})
    };


    fetchData=() => {
        let search = this.state.search;
        if (search !== "") {
            search = "&search=" + search
        }

        let sort;
        if (this.state.selectedFilter.length > 0) {
            sort = "&sort-by=" + this.state.selectedFilter[1]
        }

        let dsc = "?dsc=" + this.state.dsc;

        const token = "Bearer " + localStorage.getItem("token");
        fetch("http://localhost:8080/validasi/view-all" + dsc + sort + search   , {
            signal: this.controller.signal,
            method: "GET",
            headers: {
                "Authorization": token,
            }
        }).then(response => {
            return response.json();
        }).then((data) => {
            const fetchedData = [];
            for (let key in data.Warung){
                fetchedData.push({
                    ...data.Warung[key]
                });
            }
            this.setState({
                mitra: fetchedData,
                currentVerifikasi: fetchedData.slice(0, ITEM_PER_PAGE),
                isLoading: false
            });
        })
    }

    fetchAccepted = () => {
        const token = "Bearer " + localStorage.getItem("token");
        fetch("http://localhost:8080/validasi/update/" + this.state.id, {
            signal: this.controller.signal,
            method: "PUT",
            headers: {
                "Authorization" : token,
            }
        }).then(response => {
            if (response.status === 200) {
                this.componentDidMount()
                this.setState({
                    showDetailModal: false,
                    showAcceptedAlert: true
                })

            }
        })
    };

    componentDidMount() {
        this.fetchData();
        window.history.replaceState(undefined, '')
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.state.warung_name !== prevState.warung_name || this.state.currentPage !== prevState.currentPage){
            this.setState({
                showRejectedAlert: false
            });
        }
        window.history.replaceState(undefined, '')
    }

    componentWillUnmount() {
        this.controller.abort()
    }

    closeDetailModal = () => {
        this.setState({
            showDetailModal: false,
            warung_name: "",
            mitra_name: "",
            ktp_id: "",
            birth_date: "",
            phone_number: "",
            business_type: "",
            village_name: "",
            address: "",
            rt_rw: "",
            point_land: ""
        });
    }

    closeAlert = () =>{
        this.setState({
            showAcceptedAlert: false,
            showRejectedAlert: false
        });
    };

    showDetailHandler = (verifikasi) => {
        this.setState({
            showDetailModal: true,
            id: verifikasi.id,
            warung_name: verifikasi.warung_name,
            mitra_name: verifikasi.mitra_name,
            ktp_id: verifikasi.ktp_id,
            birth_date: new Date(Date.parse(verifikasi.birth_date))
                .toLocaleDateString('id-ID'),
            phone_number: verifikasi.phone_number,
            business_type: verifikasi.business_type,
            village_name: verifikasi.village_name,
            address: verifikasi.address,
            rt_rw: verifikasi.rt_rw,
            point_land: verifikasi.point_land,
            alasan: verifikasi.reason
        })
    }

    onPageChanged = data => {
        const { currentPage, pageLimit } = data;

        const offset = (currentPage - 1) * pageLimit;
        const currentVerifikasi = this.state.mitra.slice(offset, offset + pageLimit);

        this.setState({ currentVerifikasi, currentPage });
    };

    renderDetail(){
        return(
            <div>
                <p>Nama Warung : {this.state.warung_name}</p>
                <p>Status : {this.state.alasan} </p>
                <hr/>
                <p>Nama Lengkap Sesuai KTP : {this.state.mitra_name}</p>
                <p>No. KTP : {this.state.ktp_id}</p>
                <p>Tanggal Lahir Sesuai KTP : {this.state.birth_date}</p>
                <p>No. HP : {this.state.phone_number}</p>
                <p>Jenis Usaha : {this.state.business_type}</p>
                <p>Lokasi : {this.state.village_name}</p>
                <p>Alamat : {this.state.address}</p>
                <p>RT/RW : {this.state.rt_rw}</p>
                <p>Patokan : {this.state.point_land}</p>
            </div>
        )
    }


    renderRejectedButton =() => {
        this.setState({
            createAlasan: true
        })

    }

    render() {
        if (!this.props.loggedIn) {
            return <Redirect to="/login"/>
        }

        if (this.state.createAlasan) {
            let url = "/validasi/create/"
            let idUrl = this.state.id
            return <Redirect to={url + idUrl}/>
        }

        if (this.state.successAccepted) {
            return <Redirect to={{
                pathname: "/validasi/view-all",
                state: {
                    successAccepted: true
                }
            }}/>
        }

        if (this.state.isLoading){
            return (
                <div style={{
                    position: 'absolute', left: '50%', top: '50%',
                    transform: 'translate(-50%, -50%)'
                }}>
                    <h1 className="font-weight-bolder">Loading ...</h1>
                </div>
            )
        }

        return (
            <React.Fragment>
                <Container fluid style={{marginTop:'-3rem',paddingBottom:'8em'}}>
                    <h2 className="text-center mb-4 font-weight-bold">
                        List Calon Mitra
                    </h2>
                    <Card className="shadow-lg mb-5 bg-white rounded">
                        <Card.Body>
                            <div className="container">
                                <CustomModal
                                    show={this.state.showDetailModal}
                                    title="Detail Mitra"
                                    onHide={this.closeDetailModal}
                                    primaryButton="Setuju"
                                    primaryAction={this.fetchAccepted}
                                    secondaryButton="Tidak setuju"
                                    secondaryAction={this.renderRejectedButton}
                                >
                                    {this.renderDetail()}
                                </CustomModal>

                                {this.state.showAcceptedAlert &&
                                <Expire delay={4000}>
                                    <Alert
                                        variant="success"
                                        onClose={this.closeAlert}
                                        dismissible>
                                        <p>Mitra berhasil disetujui!</p>
                                    </Alert>
                                </Expire>
                                }

                                {this.state.showRejectedAlert &&
                                <Expire delay={4000}>
                                    <Alert
                                        variant="success"
                                        onClose={this.closeAlert}
                                        dismissible>
                                        <p>Alasan penolakan berhasil disimpan!</p>
                                    </Alert>
                                </Expire>
                                }

                                <OptionButton handleChange={this.handleFilter}/>
                                {this.state.showFilter ?
                                    <React.Fragment>
                                        <Filter data={this.state} handleChange={this.handleSelectFilter} handleSort={this.handleSelectOrder}
                                                handleSearch={this.handleSearch} handleApply={this.handleApply}/>
                                        <StatusVerifikasi/>
                                    </React.Fragment>
                                    :
                                    <React.Fragment></React.Fragment>
                                }

                                {this.state.mitra.length > 0 ?
                                    <VerifikasiTable data={this.state.currentVerifikasi} detail={this.showDetailHandler}/>
                                    :
                                    <p className="text-center m-3">Tidak ada calon mitra</p>
                                }
                                <div className="d-flex flex-row py-4 fa-pull-right">
                                    <CustomPagination
                                        currentPage={this.state.currentPage}
                                        totalRecords={this.state.mitra.length}
                                        pageLimit={ITEM_PER_PAGE}
                                        onPageChanged={this.onPageChanged}
                                    />
                                </div>
                            </div>
                        </Card.Body>
                    </Card>
                </Container>
            </React.Fragment>
        );
    }
}

export default withRouter(VerifikasiViewAll);