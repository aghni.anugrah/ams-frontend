This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

### Pagination

Di container, import Pagination component (sesuai struktur folder): 
```
import CustomPagination from "../../components/Pagination/CustomPagination";
```

Tambah state buat nampung data yang ditampilin di page yang lagi dibuka di state (current page). <br/>
Contoh: Di fetch ngambil semua warung tidak registrasi, terus ditampung di `warungNoreg`. Tambah `currentWarungs` buat nampung data di current page:<br/>
```
state: {
warungNoreg:[],
currentWarungs:[]
}
```

Tambah handler (sesuaikan `currentWarungs` dan `this.state.warungNoreg` sama state tadi: <br/>
```
onPageChanged = data => {
    const { currentPage, pageLimit } = data;

    const offset = (currentPage - 1) * pageLimit;
    const currentWarungs = this.state.warungNoreg.slice(offset, offset + pageLimit);

    this.setState({ currentWarungs });
};
```

Declare table (sesuaikan `currentWarungs` dengan state yang ditambahkan di state tadi):
```
<TableWarungNoreg data={this.state.currentWarungs} detail={this.showDetailHandler}/>
```

Panggil component di bagian render() setelah declare tabel:
```
<div className="d-flex flex-row py-4 fa-pull-right">
    <CustomPagination
        totalRecords={this.state.warungNoreg.length}
        pageLimit={5}
        onPageChanged={this.onPageChanged}
    />
</div>
```
Sesuaikan `this.state.warungNoreg` dengan yang dibuat di state.<br/>
Props: <br/>
`totalRecords`: Jumlah item yang ada di data <br/>
`pageLimit`: Jumlah item yang mau ditampilkan per page <br/>
`onPageChanged`: handler untuk ganti page yang tadi dibuat <br/>

### Navigation Bar

- Cek branch `IMA-List-salesperson-page`, ke folder `components`, copy folder `navigation` ke folder `components` di local masing-masing
- Buka App.js, tambahin `onLogout={this.handleLogout}` pas manggil container
    ```
    <Route path="/salesperson-control/" exact
        render={() =>
            <SalespersonControl
                loggedIn={this.state.loggedIn}
                onLogout={this.handleLogout}
            />}
    />
    ```
- Di container, tambahin `state` baru untuk nampung nama dan role user yang sedang login, kayak gini:
    ```
    state = {
        ...
        namaUser: "",
        roleUser: "",
    }; 
    ```
- Buat function baru untuk ngefetch role dan nama yang sedang login:
    ```
    fetchRoleandName = () => {
        const token = "Bearer " + localStorage.getItem("token");
        fetch("http://localhost:8080/navbar-role", {
            method: "GET",
            headers: {
                "Authorization" : token,
            }
        }).then(response => {
            return response.json();
        }).then((data) => {
            let nameUser;
            let userRole;
            nameUser = data.nama;
            userRole = data.role;
            this.setState({
                namaUser: nameUser,
                roleUser: userRole,
            });
        }).catch(err => {
            throw new Error(err)
        })
    }
    ```
- Panggil function `fetchRoleandName` tadi di `componentDidMount`
    ```
    componentDidMount() {
        ...
        this.fetchRoleandName();
    }  
    ```
- Import navigation barnya
    ```
    import NavigationBar from "../../components/Navigation/NavigationBar";
    ```
- Panggil navigation barnya di posisi paling atas setelah `<React.Fragment>` pas bagian returnnya container, jangan sampai ada di dalam `container`, `body`, atau tag lain
    ```
    <NavigationBar namaUser={this.state.namaUser} roleUser={this.state.roleUser} onLogout={this.props.onLogout}/>
    ```
- Kalau menu di navbar masih ada yang `#`, edit sendiri aja ya soalnya gua gatau kalian mau routingnya kayak gimana he he
- ???
- Profit
